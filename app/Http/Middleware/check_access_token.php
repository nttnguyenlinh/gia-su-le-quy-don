<?php

namespace App\Http\Middleware;

use Closure;
use App\Providers\ImgurService;

class check_access_token
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Lấy nội dung từ file config/.credentials
        $credential = file_get_contents(config_path('.credentials'));
        // Nếu chưa có access_token được lưu trữ trong file thì nhảy tới trang yêu cầu người dùng cấp quyền.
        if(strlen($credential) == 0) {
            return redirect()->away(ImgurService::ImgurAuthUrl());
        }

        return $next($request); // Nếu đã có access_token trong .credentials rồi thì thực hiện như bình thường.

    }
}
