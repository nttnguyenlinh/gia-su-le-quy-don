<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
Use App\gia_su;
use App\lop_hoc;
use App\mon_hoc;
use App\khu_vuc;
use App\phieu_mo_lop;

use Session;
use Hash;

class GiaSuController extends Controller
{
    //Login
    public function login()
    {
        return view('giasu.dangnhap');
    }
    //Login
    public function postlogin(Request $request)
    {
        $giasu = gia_su::where('SoDT', '=', $request->sdt)->first();

        if($giasu)
            if($giasu->Status == 1)
            {
                Session::put('sdt', $giasu->SoDT);
                return redirect('tai-khoan.html')->with(['flag'=>'success','message'=>'Chào mừng, ' . $giasu->HoTen . ' quay lại với trung tâm.']);
            }
            else
                return redirect()->back()->with(['flag'=>'error','message'=>'Tài khoản này đang bị khoá!']);
        else
            return redirect()->back()->with(['flag'=>'error','message'=>'Số ĐT không tồn tại!']);
    }
    //Profile
    public function index()
    {
        if(Session::has('sdt'))
        {
            $getMonHoc = mon_hoc::all();
            $getLopHoc = lop_hoc::all();
            $getKhuVuc = khu_vuc::where('MaTinh', 1)->get();
            $getinfo = gia_su::where('SoDT', 'like', Session::get('sdt'))->first();

            return view('giasu.index', compact('getMonHoc', 'getLopHoc', 'getKhuVuc', 'getinfo'));
        }
        else
            return redirect('/');
    }
    //Get chi tiết lớp sau khi đã đăng ký dạy
    public function getLop(Request $request)
    {
        if(Session::has('sdt'))
        {
            $getChiTiet = phieu_mo_lop::where('slug', 'like', $request->slug)->first();

            if($getChiTiet)
                return view('giasu.lop', compact('getChiTiet'));
            else
                return abort(404);
        }
        return abort(404);
    }
    //Cập nhật Profile
    public function post_update(Request $request)
    {
        if(Session::has('sdt'))
        {
            $this->validate($request,
                [
                    'hoten' => 'required',
                    'ngay' => 'required',
                    'thang' => 'required',
                    'nam' => 'required',
                    'nguyenquan' => 'required',
                    'giongnoi' =>'required',
                    'diachi' => 'required',
                    'cmnd' => 'required|min:9|max:12',
                    'email' => 'required|unique:admins,email',
                    'password' => 'required|regex:/(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/',
                    'truonghoc' => 'required',
                    'chuyennganh' => 'required',
                    'nam_totnghiep' => 'required',
                    'trinhdo' => 'required',
                    'gioitinh' => 'required',
                ],
                [
                    'hoten.required' => 'Vui lòng nhập họ tên!',
                    'ngay.required' => 'Vui lòng chọn ngày sinh!',
                    'thang.required' => 'Vui lòng chọn tháng sinh!',
                    'nam.required' => 'Vui lòng chọn năm sinh!',
                    'nguyenquan.required' => 'Vui lòng chọn nguyên quán!',
                    'giongnoi.required' => 'Vui lòng chọn giọng nói!',
                    'diachi.required' => 'Vui lòng địa chỉ!',
                    'cmnd.required' => 'Vui lòng nhập CMND!',
                    'cmnd.min' => 'CMND không hợp lệ!',
                    'cmnd.max' => 'CMND không hợp lệ!',
                    'email.required' => 'Vui lòng nhập email!',
                    'email.email' => 'Email không hợp lệ!',
                    'email.unique' => 'Email đã có người sử dụng!',
                    'password.required' => 'Nhập mật khẩu!',
                    'password.regex' => 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, có ít nhất 6 kí tự!',
                    'truonghoc.required' => 'Vui lòng nhập trường đào tạo!',
                    'chuyennganh.required' => 'Vui lòng nhập chuyên ngành!',
                    'nam_totnghiep.required' => 'Vui lòng chọn năm tốt nghiệp!',
                    'trinhdo.required' => 'Vui lòng chọn trình độ!',
                    'gioitinh.required' => 'Vui lòng chọn giới tính!',
                ]
            );

            if(empty($request->monday))
                return redirect()->back()->with(['flag'=>'error','message'=>'Vui lòng chọn môn dạy!']);
            if(empty($request->lopday))
                return redirect()->back()->with(['flag'=>'error','message'=>'Vui lòng chọn lớp dạy!']);
            if(empty($request->khuvuc))
                return redirect()->back()->with(['flag'=>'error','message'=>'Vui lòng chọn khu vực!']);

            $email = gia_su::where('Email', $request->email, 'and')->where('MaGiaSu', '!=',  $request->id)->count();
            if($email > 0)
                return redirect()->back()->with(['flag'=>'error','message'=>'Email đã có người sử dụng. Vui lòng chọn email khác!']);
            else
            {
                $giasu = gia_su::where('SoDT', 'like', Session::get('sdt'))->first();
                if(!Hash::check($request->password, $giasu->MatKhau))
                    return redirect()->back()->with(['flag'=>'error','message'=>'Mật khẩu không đúng!']);
                else
                {
                    $giasu->HoTen = $request->hoten;
                    $ngaysinh = $request->ngay . '-' . $request->thang . '-' . $request->nam;
                    $giasu->NgaySinh = $ngaysinh;

                    $giasu->GioiTinh = $request->gioitinh;
                    $giasu->NguyenQuan = $request->nguyenquan;
                    $giasu->GiongNoi = $request->giongnoi;

                    $giasu->DiaChi = $request->diachi;
                    $giasu->CMND = $request->cmnd;
                    $giasu->Email = $request->email;

                    if($request->hasFile('anhthe'))
                    {
                        $file = $request->file('anhthe');
                        $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file
                        $file_size = $file->getClientSize();

                        if($file_extension != 'png' && $file_extension != 'jpg' && $file_extension != 'jpeg')
                            return redirect()->back()->with(['flag'=>'error','message'=>'Vui lòng chọn hình ảnh dạng png, jpg, jpeg.']);

                        if($file_size > 2097152)//2 MB (size is also in bytes)
                            return redirect()->back()->with(['flag' => 'danger', 'message' => 'Hình ảnh quá lớn! Vui lòng chọn hình ảnh < 2MB.']);

                        //$file_name = rand() . '_' . time() . '.' . $file_extension;
                        //$file->move('tt/images/avatars/', $file_name);
                        $image = base64_encode(file_get_contents($file));
                        $giasu->AnhThe = $image;
                    }
                    else
                        $giasu->AnhThe;

                    $giasu->TruongDaoTao = $request->truonghoc;
                    $giasu->NganhHoc = $request->chuyennganh;
                    $giasu->CongTac = $request->congtac;

                    $giasu->NamTN = $request->nam_totnghiep;
                    $giasu->TrinhDo = $request->trinhdo;
                    $giasu->LoaiGiaoVien = $request->loai_gv;

                    $giasu->UuDiem = $request->uudiem;

                    $giasu->MonDay = implode(",",$request->monday);
                    $giasu->LopDay = implode(",",$request->lopday);

                    $giasu->TinhThanh = $request->tinhthanh;
                    $giasu->KhuVuc = implode(",",$request->khuvuc);

                    if($giasu->save())
                        return redirect()->back()->with(['flag'=>'success','message'=>'Cập nhật thông tin thành công!']);
                    else
                        return redirect()->back()->with(['flag'=>'error','message'=>'Xin lỗi! Không thể hoàn tất yêu cầu của bạn.']);
                }
            }
        }
        return redirect('/');
    }

    //Đổi MK
    public function doimk()
    {
        if(Session::has('sdt'))
        {
            $getinfo = gia_su::where('SoDT', 'like', Session::get('sdt'))->first();
            return view('giasu.doimk', compact('getinfo'));
        }
        else
            return redirect('/');
    }

    //Đổi MK
    public function postdoimk(Request $request)
    {
        if(Session::has('sdt'))
        {
            $this->validate($request,
                [
                    'old_password' => 'required',
                    'new_password' => [
                        'required',
                        'regex:/(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/'
                    ]
                ],
                [
                    'old_password.required' => 'Vui lòng nhập mật khẩu hiện tại!',
                    'new_password.required' => 'Vui lòng nhập mật khẩu mới!',
                    'new_password.regex' => 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, có ít nhất 6 kí tự!'
                ]
            );

            $giasu = gia_su::where('SoDT', 'like', Session::get('sdt'))->first();
            $old = $request->old_password;
            $new = $request->new_password;
            if(Hash::check($old, $giasu->MatKhau))
                if($old !== $new)
                {
                    $giasu->MatKhau = Hash::make($new);
                    $giasu->save();
                    return redirect()->back()->with(['flag'=>'success','message'=>'Thay đổi mật khẩu thành công!']);
                }
                else
                    return redirect()->back()->with(['flag'=>'error','message'=>'Mật khẩu mới phải khác mật khẩu hiện tại!']);
            else
                return redirect()->back()->with(['flag'=>'error','message'=>'Mật khẩu hiện tại không chính xác!']);
        }
        return redirect('/');
    }

    //Đăng xuất
    public function thoat()
    {
        if(Session::has('sdt'))
            Session::forget('sdt');
        return redirect('/');
    }
}
