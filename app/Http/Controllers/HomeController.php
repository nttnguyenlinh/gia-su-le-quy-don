<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\phieu_dang_ky;
use App\lop_hoc;
use App\mon_hoc;
Use App\gia_su;
use App\phieu_mo_lop;
use App\phieu_nhan_lop;
use App\TuyenDung;
use App\HocPhi;
use App\khu_vuc;
use Session;
use Mail;
use Hash;
use App\Providers\ImgurService;

class HomeController extends Controller
{
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }

    */

    //Call service to upload image to imgur.com Version GuzzleClient
    public function ImgurImage($image)
    {
        return ImgurService::ImgurImage($image);
    }

    //Call service to upload image to imgur.com Version cURL
    public function upload_Image($image)
    {
        return ImgurService::uploadImage($image);
    }

    public function index()
    {
        $getKhuVuc = khu_vuc::where('MaTinh', 1)->get();

        $getLopHoc = lop_hoc::all();

        $getPhieuMoLop = phieu_mo_lop::where('Status', 0)
            ->orderBy('MaPhieuMo', 'desc')
            ->take(4)
            ->get();
        return view('index', compact('getKhuVuc','getLopHoc', 'getPhieuMoLop'));
    }

    public function getDangKyGiaSu()
    {
        $getLopHoc = lop_hoc::all();
        return view('phieu_dang_ky', compact('getLopHoc'));
    }

    public function postDangKyGiaSu(Request $request)
    {
        $this->validate($request,
            [
                'hoten' => 'required|min:4|max:30',
                'dienthoai' => 'required|min:10|max:10',
                'diachi' => 'required|max:30',
                'email' => 'email',
                'lop' => 'required',
                'monhoc' => 'required',
                'level' => 'required',
                'sobuoi' => 'required',
                'thoigian' => 'required|max:30',
                'yeucau' => 'max:100'
            ],
            [
                'hoten.required' => 'Vui lòng nhập họ tên!',
                'hoten.min' => 'Nhập ít nhất 4 kí tự!',
                'hoten.max' => 'Nhập tối đa 30 kí tự!',
                'dienthoai.required' => 'Vui lòng nhập sô điện thoại!',
                'dienthoai.min' => 'Số điện thoại gồm 10 số!',
                'dienthoai.max' => 'Số điện thoại gồm 10 số!',
                'diachi.required' => 'Vui lòng địa chỉ!',
                'diachi.max' => 'Nhập tối đa 30 kí tự!',
                'email.email' => 'Email không hợp lệ!',
                'lop.required' => 'Vui lòng chọn lớp học!',
                'monhoc.required' => 'Vui lòng nhập môn học!',
                'level.required' => 'Vui lòng chọn người dạy!',
                'sobuoi.required' => 'Vui lòng chọn số buổi học!',
                'thoigian.required' => 'Vui lòng nhập thời gian học!',
                'thoigian.max' => 'Nhập tối đa 30 kí tự!',
                'yeucau.max' => 'Nhập tối đa 100 kí tự!'
            ]
        );

        $phieu = new phieu_dang_ky();

        $phieu->HoTen = $request->hoten;
        $phieu->DiaChi = $request->diachi;
        $phieu->SoDT = $request->dienthoai;
        $phieu->Email = $request->email;
        $phieu->Lop = $request->lop;
        $phieu->Monhoc = $request->monhoc;
        $phieu->SoHocSinh = $request->sohocsinh;
        $phieu->HocLuc = $request->hocluc;
        $phieu->SoBuoiDay = $request->sobuoi;
        $phieu->ThoiGianHoc = $request->thoigian;
        $phieu->YeuCau = $request->level;
        $phieu->YeuCauKhac = $request->yeucau;

        if($phieu->save())
        {
            if(!empty($request->email))
            {
                Mail::send('emails.tim_gia_su', ['hoten'=>$request->hoten, 'trangthai' => 'Trung tâm sẽ liên hệ lại với anh(chị).'], function($message) use ($request)
                {
                    $message->from('giasulequydonhcm@gmail.com', 'Gia sư Lê Quý Đôn')
                        ->subject('Đăng Ký Tìm Gia Sư')
                        ->to($request->email, $request->hoten);
                });

                Mail::send('emails.tim_gia_su_admin', [], function($message)
                {
                    $message->from('giasulequydonhcm@gmail.com', 'Gia sư Lê Quý Đôn')
                        ->subject('Đăng Ký Tìm Gia Sư')
                        ->to('hadung2014@gmail.com', 'Gia sư Lê Quý Đôn');
                });
            }
            return redirect()->back()->with(['flag'=>'success','message'=>'Cảm ơn ' . $request->hoten . '! Đã lựa chọn gia sư Lê Quý Đôn. Chúng tôi sẽ liên hệ với bạn một cách nhanh nhất.']);
        }
        else
            return redirect()->back()->with(['flag'=>'error','message'=>'Có lỗi xảy ra. Vui lòng thử lại!']);
    }

    //Lấy danh sách gia sư
    public function getGiaSuTieuBieu()
    {
        $getLopHoc = lop_hoc::all();
        $getMonHoc = mon_hoc::all();
        $getKhuVuc = khu_vuc::all();
        $getGiaSu = gia_su::orderBy('MaGiaSu', 'desc')->where('Status', 1)->paginate(6);
        return view('giasu', compact('getLopHoc', 'getMonHoc', 'getKhuVuc', 'getGiaSu'));
    }

    //Tìm kiếm danh sách gia sư
    public function searchGiaSu(Request $request)
    {
        $getLopHoc = lop_hoc::all();
        $getMonHoc = mon_hoc::all();
        $getKhuVuc = khu_vuc::all();

        if(!empty($request->timkiem))
        {
            $tukhoa = $request->tukhoa;
            $lopday = $request->lopday;
            $monday = $request->monday;
            $gioitinh = $request->gioitinh;
            $trinhdo = $request->trinhdo;
            $quanhuyen = $request->quanhuyen;

            $getGiaSu = gia_su::where(function($query) use ($tukhoa){
                $query->where('MaGiaSu','like','%'.$tukhoa.'%')
                    ->orWhere('HoTen','like','%'.$tukhoa.'%');
            })
                ->where(function($query2) use ($gioitinh){
                    $query2->orwhere('GioiTinh','like','%'.$gioitinh.'%');
                })

                ->where(function($query3) use ($lopday){
                    $query3->orwhere('LopDay','like','%'.$lopday.'%');
                })

                ->where(function($query4) use ($monday){
                    $query4->orwhere('MonDay','like','%'.$monday.'%');
                })

                ->where(function($query5) use ($trinhdo){
                    $query5->orwhere('TrinhDo','like','%'.$trinhdo.'%');
                })

                ->where(function($query6) use ($quanhuyen){
                    $query6->orwhere('KhuVuc','like','%'.$quanhuyen.'%');
                })

                ->where('Status', 1)
                ->orderBy('MaGiaSu', 'desc')->paginate(6);
        }
        else
            $getGiaSu = gia_su::orderBy('MaGiaSu', 'desc')->where('Status', 1)->paginate(6);

        return view('giasu', compact('getLopHoc', 'getMonHoc', 'getKhuVuc', 'getGiaSu'));

    }

    public function getTuyenDung()
    {
        $tuyendung = TuyenDung::orderBy('MaPhieuTD', 'desc')->get();
        return view('tuyen-dung', compact('tuyendung'));
    }

    public function getHocPhi()
    {
        $hocphi = HocPhi::all();
        return view('hocphi', compact('hocphi'));
    }

    //Get chi tiết lớp đang mở
    public function getChiTietLop(Request $request)
    {
        $getChiTiet = phieu_mo_lop::where('slug', 'like', $request->slug)->first();

        if($getChiTiet)
        {
            if(Session::has('sdt'))
            {
                $giasu = gia_su::where('SoDT', 'like', Session::get('sdt'))->first();
                $info = phieu_nhan_lop::where('MaPhieuMo', $getChiTiet->MaPhieuMo, 'and')->where('MaGiaSu', $giasu->MaGiaSu)->first();

                if($info)
                    return redirect()->route('getLop', ['slug' =>$request->slug]);
            }
            return view('ChiTiet', compact('getChiTiet'));
        }

        else
            return abort(404);
    }

    //Đăng ký nhận lớp dạy (phiếu nhận lớp)
    public function postChiTietLop(Request $request)
    {
        $count = phieu_nhan_lop::where('MaPhieuMo', $request->maphieu)->count();
        if($count >= 10)
            return redirect()->back()->with(['flag'=>'error','message'=>'Xin lỗi Bạn! Lớp này đã đạt đến số lượng đăng ký cho phép. Mời bạn chọn lớp khác.']);
        else
        {
            $giasu = gia_su::where('SoDT', 'like', $request->phone)->first();
            if($giasu)
            {
                if($giasu->Status == 1)
                {
                    $count = phieu_nhan_lop::where('MaPhieuMo', $request->maphieu, '&and')->where('MaGiaSu', '=', $giasu->MaGiaSu)->count();
                    if($count == 0)
                    {
                        $phieunhan = new phieu_nhan_lop();
                        $phieunhan->MaPhieuMo = $request->maphieu;
                        $phieunhan->MaGiaSu = $giasu->MaGiaSu;
                        $phieunhan->HinhThucNhan = $request->hinhthuc;
                        $phieunhan->NgayDay = $request->ngay;
                        $phieunhan->GioDay = $request->gio;
                        $phieunhan->LePhi = $request->lephi;
                        $phieunhan->NoiDungThem = $request->noidung;

                        if($phieunhan->save())
                        {
                            if(!empty($giasu->Email))
                            {
                                Mail::send('emails.dk_gia_su', ['hoten' => $giasu->HoTen, 'trangthai' => 'Thông tin nhận lớp dạy của anh(chị) đã được trung tâm ghi nhận. Bạn sắp xếp đóng lệ phí sớm để đủ điều kiện dạy nhé.'], function($message) use ($giasu)
                                {
                                    $message->from('giasulequydonhcm@gmail.com', 'Gia sư Lê Quý Đôn')
                                        ->subject('Đăng Ký Dạy')
                                        ->to($giasu->Email, $giasu->Hoten);
                                });
                            }

                            Mail::send('emails.dk_gia_su_admin', ['tinnhan' => 'Có đăng ký dạy mới.'], function($message)
                            {
                                $message->from('giasulequydonhcm@gmail.com', 'Gia sư Lê Quý Đôn')
                                    ->subject('Đăng Ký Dạy')
                                    ->to('hadung2014@gmail.com', 'Gia sư Lê Quý Đôn');
                            });

                            return redirect()->back()->with(['flag'=>'success','message'=>'Chúc mừng bạn ' . $giasu->HoTen . '! Thông tin nhận lớp của bạn đã được trung tâm ghi nhận. Bạn sắp xếp đóng lệ phí sớm để đủ điều kiện dạy nhé. Chúc bạn một ngày tốt lành!']);

                        }
                        else
                            return redirect()->back()->with(['flag'=>'error','message'=>'Xin lỗi ' . $giasu->HoTen . '! Trung tâm không thể hoàn tất việc nhận lớp của bạn!']);
                    }
                    else
                        return redirect()->back()->with(['flag'=>'error','message'=>'Bạn đã gửi đăng ký lớp này rồi. Mời bạn chọn lớp khác!']);
                }
                else
                    return redirect()->back()->with(['flag'=>'error','message'=>'Xin lỗi ' . $giasu->HoTen . '! Tài khoản của bạn đang bị khoá nên không thể nhận lớp!']);
            }
            else
                return redirect()->back()->with(['flag'=>'error','message'=>'Không tìm thấy thông tin của bạn. Mời bạn chọn ĐĂNG KÝ LÀM GIA SƯ để đồng hành cùng trung tâm nhé!']);
        }
    }

    //Get danh sách lớp đang mở
    public function getLopDangMo()
    {
        $getKhuVuc = khu_vuc::where('MaTinh', 1)->get();
        $getLopHoc = lop_hoc::all();
        $getPhieuMoLop = phieu_mo_lop::where('Status', 0)->orderBy('MaPhieuMo', 'desc')->paginate(6);
        return view('LopDay', compact('getKhuVuc','getLopHoc', 'getPhieuMoLop'));
    }


    //Search danh sách lớp đang mở
    public function searchLopDangMo(Request $request)
    {
        $getKhuVuc = khu_vuc::where('MaTinh', 1)->get();

        $getLopHoc = lop_hoc::all();

        if(!empty($request->timkiem))
        {
            if($request->khuvuc != "")
            {
                $kv = khu_vuc::where('MaKV', $request->khuvuc)->first();
                $quanhuyen = $kv->TenKV;
            }
            else
                $quanhuyen = "";

            if($request->lopday != "")
            {
                $lop = lop_hoc::where('MaLop', $request->lopday)->first();
                $lopday = $lop->TenLop;
            }
            else
                $lopday = "";

            $tukhoa = $request->tukhoa;

            $getPhieuMoLop = phieu_mo_lop::where(function($query) use ($tukhoa){
                $query->where('MaPhieuMo','like','%'.$tukhoa.'%')
                    ->orWhere('MonDay','like','%'.$tukhoa.'%');
            })

                ->where(function($query2) use ($quanhuyen){
                    $query2->orwhere('DiaChi','like','%'.$quanhuyen.'%');
                })

                ->where(function($query3) use ($lopday){
                    $query3->orwhere('LopDay','like','%'.$lopday.'%');
                })

                ->where('Status', 0)
                ->orderBy('MaPhieuMo', 'desc')->paginate(6);
        }
        else
            $getPhieuMoLop = phieu_mo_lop::where('Status', 0)->orderBy('MaPhieuMo', 'desc')->paginate(6);

        return view('LopDay', compact('getKhuVuc','getLopHoc', 'getPhieuMoLop'));

    }

    //Đăng ký làm gia sư
    public function DkGiaSu()
    {
        $getMonHoc = mon_hoc::all();
        $getLopHoc = lop_hoc::all();
        $getKhuVuc = khu_vuc::where('MaTinh', 1)->get();
        return view('DangKyGiaSu', compact('getMonHoc', 'getLopHoc', 'getKhuVuc'));
    }

    //Đăng ký làm gia sư
    public function postDkGiaSu(Request $request)
    {
        $this->validate($request,
            [
                'hoten' => 'required',
                'ngay' => 'required',
                'thang' => 'required',
                'nam' => 'required',
                'nguyenquan' => 'required',
                'giongnoi' =>'required',
                'diachi' => 'required',
                'cmnd' => 'required|min:9|max:12',
                'email' => 'required|unique:admins,email',
                'password' => 'required|regex:/(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/',
                'password_confirm' => 'required|same:password',
                'phone' => 'required|min:10|max:10',
                'truonghoc' => 'required',
                'chuyennganh' => 'required',
                'nam_totnghiep' => 'required',
                'trinhdo' => 'required',
                'gioitinh' => 'required',
                'anhthe' => 'required',
            ],
            [
                'hoten.required' => 'Vui lòng nhập họ tên!',
                'ngay.required' => 'Vui lòng chọn ngày sinh!',
                'thang.required' => 'Vui lòng chọn tháng sinh!',
                'nam.required' => 'Vui lòng chọn năm sinh!',
                'nguyenquan.required' => 'Vui lòng chọn nguyên quán!',
                'giongnoi.required' => 'Vui lòng chọn giọng nói!',
                'diachi.required' => 'Vui lòng địa chỉ!',
                'cmnd.required' => 'Vui lòng nhập CMND!',
                'cmnd.min' => 'CMND không hợp lệ!',
                'cmnd.max' => 'CMND không hợp lệ!',
                'email.required' => 'Vui lòng nhập email!',
                'email.email' => 'Email không hợp lệ!',
                'email.unique' => 'Email đã có người sử dụng!',
                'password.required' => 'Nhập mật khẩu!',
                'password.regex' => 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, có ít nhất 6 kí tự!',
                'password_confirm.required' => 'Vui lòng nhập lại mật khẩu!',
                'password_confirm.same' => 'Mật khẩu không trùng khớp!',
                'phone.required' => 'Vui lòng nhập số điện thoại!',
                'phone.min' => 'Số điện thoại gồm 10 số!',
                'phone.max' => 'Số điện thoại gồm 10 số!',
                'truonghoc.required' => 'Vui lòng nhập trường đào tạo!',
                'chuyennganh.required' => 'Vui lòng nhập chuyên ngành!',
                'nam_totnghiep.required' => 'Vui lòng chọn năm tốt nghiệp!',
                'trinhdo.required' => 'Vui lòng chọn trình độ!',
                'gioitinh.required' => 'Vui lòng chọn giới tính!',
                'anhthe.required' => 'Vui lòng chọn hình ảnh của bạn!',
            ]
        );

        if(empty($request->monday))
            return redirect()->back()->with(['flag'=>'error','message'=>'Vui lòng chọn môn dạy!']);
        if(empty($request->lopday))
            return redirect()->back()->with(['flag'=>'error','message'=>'Vui lòng chọn lớp dạy!']);
        if(empty($request->khuvuc))
            return redirect()->back()->with(['flag'=>'error','message'=>'Vui lòng chọn khu vực!']);

        $email = gia_su::where('Email', $request->email)->count();

        $phone = gia_su::where('SoDT', $request->phone)->count();

        if($email > 0)
            return redirect()->back()->with(['flag'=>'error', 'message'=>'Email đã có người sử dụng. Vui lòng chọn email khác!']);

        if($phone > 0)
            return redirect()->back()->with(['flag'=>'error', 'message'=>'Số điện thoại đã tồn tại. Nếu bạn quên mật khẩu, vui lòng liên hệ với trung tâm để được hỗ trợ!']);
        else
        {
            $dk = new gia_su();

            $dk->HoTen = $request->hoten;

            $ngaysinh = $request->ngay . '-' . $request->thang . '-' . $request->nam;
            $dk->NgaySinh = $ngaysinh;

            $dk->GioiTinh = $request->gioitinh;
            $dk->NguyenQuan = $request->nguyenquan;
            $dk->GiongNoi = $request->giongnoi;

            $dk->DiaChi = $request->diachi;
            $dk->CMND = $request->cmnd;
            $dk->Email = $request->email;
            $dk->SoDT = $request->phone;

            $dk->MatKhau = Hash::make($request->password);

            if($request->hasFile('anhthe')){
                $file = $request->file('anhthe');
                $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file
                $file_size = $file->getClientSize();

                if($file_extension != 'png' && $file_extension != 'jpg' && $file_extension != 'jpeg')
                    return redirect()->back()->with(['flag'=>'error', 'message'=>'Vui lòng chọn hình ảnh dạng png, jpg, jpeg.']);

                if($file_size > 2097152) //2 MB (size is also in bytes)
                    return redirect()->back()->with(['flag' => 'error', 'message' => 'Hình ảnh quá lớn! Vui lòng chọn hình ảnh < 2MB.']);

                //$file_name = rand() . '_' . time() . '.' . $file_extension;
                //$file->move('tt/images/avatars/', $file_name);
                $image = base64_encode(file_get_contents($file));
                $dk->AnhThe = $this->ImgurImage($image);
            }
            else
                $dk->AnhThe = '';

            $dk->TruongDaoTao = $request->truonghoc;
            $dk->NganhHoc = $request->chuyennganh;
            $dk->CongTac = $request->congtac;

            $dk->NamTN = $request->nam_totnghiep;
            $dk->TrinhDo = $request->trinhdo;
            $dk->LoaiGiaoVien = $request->loai_gv;

            $dk->UuDiem = $request->uudiem;

            $dk->MonDay = implode(",",$request->monday);
            $dk->LopDay = implode(",",$request->lopday);

            $dk->TinhThanh = $request->tinhthanh;
            $dk->KhuVuc = implode(",",$request->khuvuc);


            if($dk->save())
            {
                Mail::send('emails.dk_gia_su', ['hoten' => $request->hoten, 'trangthai' => 'Trung tâm sẽ xem xét và liên hệ lại với anh(chị).'], function($message) use ($request)
                {
                    $message->from('giasulequydonhcm@gmail.com', 'Gia sư Lê Quý Đôn')
                        ->subject('Đăng Ký Làm Gia Sư')
                        ->to($request->email, $request->hoten);
                });

                Mail::send('emails.dk_gia_su_admin', ['tinnhan' => 'Có đăng ký làm gia sư mới.'], function($message)
                {
                    $message->from('giasulequydonhcm@gmail.com', 'Gia sư Lê Quý Đôn')
                        ->subject('Đăng Ký Làm Gia Sư')
                        ->to('hadung2014@gmail.com', 'Gia sư Lê Quý Đôn');
                });
                return redirect()->back()->with(['flag'=>'success','message'=>'Chào mừng ' . $request->hoten . ' đến với gia sư Lê Quý Đôn! Trung tâm sẽ xem xét và kích hoạt thông tin của bạn!']);
            }
            else
                return redirect()->back()->with(['flag'=>'error','message'=>'Xin lỗi! Không thể hoàn tất yêu cầu của bạn.']);
        }
    }
}