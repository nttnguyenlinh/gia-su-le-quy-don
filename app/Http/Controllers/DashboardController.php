<?php
namespace App\Http\Controllers;
use App\gia_su;
use App\HocPhi;
use App\phieu_nhan_lop;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Hash;
use App\Admin;
use App\phieu_dang_ky;
use App\phieu_mo_lop;
use App\lop_hoc;
use App\mon_hoc;
use App\khu_vuc;
use App\TuyenDung;
use Session;
use DB;
use App\Providers\ImgurService;
use Mail;

class DashboardController extends Controller
{
    /*
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    */

    //Call service to upload image to imgur.com Version GuzzleClient
    public function ImgurImage($image)
    {
        return ImgurService::ImgurImage($image);
    }

    //Call service to upload image to imgur.com Version cURL
    public function upload_Image($image)
    {
        return ImgurService::uploadImage($image);
    }


    //Get Access token Imgur
//    public function get_imgur_auth()
//    {
//        return view('imgur.imgur_auth');
//    }

    //Save Access token Imgur
//    public function save_imgur_auth()
//    {
//        $data = request()->all();
//        $credentialFile = fopen(config_path('.credentials'), 'w');
//        //lưu access_token vào file config/.credentials
//        fwrite($credentialFile, $data['access_token']);
//        fclose($credentialFile);
//        return ['status' => 'success'];
//    }

    //Tạo Slug cho chi tiết lớp đang mở
    function to_slug($slug) {
        //Chuyển hết sang chữ thường
        $str = trim(mb_strtolower($slug));

        //Xoá dấu tiếng việt
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    	$str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    	$str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    	$str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);

	    //Xoá ký tự đặc biệt
        $str = preg_replace('/([^a-z0-9-\s])/', '', $str);

	    //Xoá khoảng trắng thay bằng '-'
        $str = preg_replace('/(\s+)/', '-', $str);

	    //Trả về chuỗi đã chuyển đổi
    	return $str;
    }

    public function login()
    {
        if (Auth::check())
            return redirect()->route('trungtam');
        return view('trungtam.login');
    }

    //Đăng nhập
    public function postLogin(Request $request)
    {
        $this->validate($request,
            [
                'username' => 'required',
                'password'=>'required'
            ],
            [
                'username.required'=>'Vui lòng nhập tên tài khoản!',
                'password.required'=>'Vui lòng nhập mật khẩu!'
            ]
        );
        //Thông tin xác thực
        $credentials = array('username'=>$request->username, 'password'=>$request->password);
        $admin = Admin::where('username', '=', $request->username)->where('Status', 1)->first();

        if($admin)
            if(Auth::attempt($credentials))
                return redirect()->route('trungtam');
            else
                return redirect()->back()->with(['flag'=>'danger','message'=>'Mật khẩu sai!']);
        else
            return redirect()->back()->with(['flag'=>'danger','message'=>'Không thể đăng nhập do tài khoản bị khoá hoặc không tồn tại!']);
    }

    //Đăng xuất
    public function logout()
    {
        if( Auth::check())
            Auth::logout();
        return redirect()->route('trungtam.login');
    }

    //Dashboard
    public function index()
    {
        if(Auth::check())
            return view('trungtam.index');
        return redirect()->route('trungtam.login');
    }


    /************************NGƯỜI DÙNG************************/

    //Lấy danh sách người dùng
    public function users()
    {
        if(Auth::check())
        {
            $admin = Admin::orderBy('id', 'desc')->get();
            return view('trungtam.users', compact('admin'));
        }
        return redirect()->route('trungtam.login');
    }

    //Thêm người dùng
    function users_add(Request $request)
    {
        if(Auth::check())
        {
            $this->validate($request,
                [
                    'name' => 'required|min:4|max:30',
                    'username' => 'required|unique:admins,username|min:4|max:30',
                    'email' => 'required|unique:admins,email',
                    'password' => [
                        'required',
                        'regex:/(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/'
                    ],
                ],
                [
                    'name.required' => 'Vui lòng nhập tên bạn!',
                    'name.min' => 'Tên bạn ít nhất 4 kí tự!',
                    'name.max' => 'Tên bạn tối đa 30 kí tự!',

                    'username.required' => 'Vui lòng nhập tài khoản!',
                    'username.min' => 'Tài khoản ít nhất 4 kí tự!',
                    'username.max' => 'Tài khoản tối đa 30 kí tự!',
                    'username.unique' => 'Tài khoản đã tồn tại!',

                    'email.required' => 'Vui lòng nhập email!',
                    'email.email' => 'Email không hợp lệ!',
                    'email.unique' => 'Email đã tồn tại!',

                    'password.required' => 'Vui lòng nhập mật khẩu!',
                    'password.regex' => 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, có ít nhất 6 kí tự!'
                ]
            );

            $admin = new Admin();

            $admin->name = $request->name;
            $admin->username = $request->username;
            $admin->email = $request->email;
            $admin->password = Hash::make($request->password);
            $admin->Status = 1;

            if($admin->save())
                return redirect()->back()->with(['flag' => 'success', 'message' => 'Người dùng ' . $request->username . ' được tạo thành công!']);
            else
                return redirect()->back()->with(['flag'=>'danger','message'=>'Không thể tạo tài khoản cho người dùng này!']);
            // Session::put("register_success", );
            // return redirect('trungtam/nguoi-dung.html');
        }
        return redirect()->route('trungtam.login');
    }
    //Xoá người dùng
    public function users_delete(Request $request)
    {
        if(Auth::check())
        {
            $return[] = '';
            if($request->ajax()){
                $admin = Admin::find($request->input('id'));
                if($admin->delete())
                    $return = ['flag' => 'success', 'message' => 'Thực hiện thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể xoá!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];

            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }
    //Thay đổi Status User
    public function users_status(Request $request)
    {
        if(Auth::check())
        {
            if($request->ajax())
            {
                $return[] = '';
                $admin = Admin::find($request->input('id'));
                if ($request->status == "enable")
                    $admin->update(['Status' => 0]);
                else
                    $admin->update(['Status' => 1]);

                if ($admin->save())
                    $return = ['flag' => 'success', 'message' => 'Thay đổi thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể thay đổi!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];

            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }
    //Reset MK người dùng
    public function users_password(Request $request)
    {
        if(Auth::check())
        {
            if($request->ajax()){
                $admin = Admin::find($request->input('id'));

                $password = 'matkhau'.rand();
                $password_hash = Hash::make($password);

                $admin->password = $password_hash;
                if($admin->save())
                    return $password;
                else
                    return 'Lỗi truyền dữ liệu!';
            }
            else return 'Có lỗi xảy ra!';
        }
        return redirect()->route('trungtam.login');
    }
    /************************NGƯỜI DÙNG************************/


    /************************CÁ NHÂN************************/
    //Lấy thông tin profile
    public function profile()
    {
        if(Auth::check())
        {
            $info = Admin::find(Auth::user()->id);
            return view('trungtam.profile', compact('info'));
        }
        return redirect()->route('trungtam.login');
    }

    //edit profile
    public function profile_edit(Request $request)
    {
        if(Auth::check())
        {
            $this->validate($request,
                [
                    'name' => 'required|min:4|max:30',
                    'email' => 'required|unique:admins,email,' .$request->id
                ],
                [
                    'name.required' => 'Vui lòng nhập tên bạn!',
                    'name.min' => 'Tên bạn ít nhất 4 kí tự!',
                    'name.max' => 'Tên bạn tối đa 30 kí tự!',

                    'email.required' => 'Vui lòng nhập email!',
                    'email.email' => 'Email không hợp lệ!',
                    'email.unique' => 'Email đã có người sử dụng!'
                ]
            );

            $info = Admin::find($request->id);
            $info->name = $request->name;
            $info->email = $request->email;

            if($request->hasFile('avatar'))
            {
                $file = $request->file('avatar');
                $file_extension = $file->getClientOriginalExtension(); // Lấy đuôi của file
                $file_size = $file->getClientSize();

                if($file_extension != 'png' && $file_extension != 'jpg' && $file_extension != 'jpeg')
                    return redirect()->back()->with(['flag' => 'danger', 'message' => 'Vui lòng chọn hình ảnh dạng png, jpg, jpeg.']);

                if($file_size > 2097152) { //2 MB (size is also in bytes)
                    return redirect()->back()->with(['flag' => 'danger', 'message' => 'Hình ảnh quá lớn! Vui lòng chọn hình ảnh < 2MB.']);
                }
                //$file_name = rand() . '_' . time() . '.' . $file_extension;
                //$file->move('tt/images/avatars/', $file_name);
                $image = base64_encode(file_get_contents($file));

                $info->avatar = $this->ImgurImage($image);
            }
            else
                $info->avatar;

            if($info->save())
                return redirect()->back()->with(['flag' => 'success', 'message' => 'Cập nhật thông tin thành công!']);
            else
                return redirect()->back()->with(['flag' => 'danger', 'message' => 'Không thể cập nhật thông tin!']);
        }
        return redirect()->route('trungtam.login');
    }

    //Thay đổi mật khẩu
    public function profile_password(Request $request)
    {
        if(Auth::check())
        {
            $this->validate($request,
                [
                    'current_password' => 'required',
                    'new_password' => [
                        'required',
                        'regex:/(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/'
                    ],
                    'confirm_password' => 'required|same:new_password'
                ],
                [
                    'current_password.required' => 'Vui lòng nhập mật khẩu hiện tại.',
                    'new_password.required' => 'Vui lòng nhập mật khẩu mới.',
                    'new_password.regex' => 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, có ít nhất 6 kí tự!',
                    'confirm_password.required' => 'Vui lòng nhập lại mật khẩu mới.',
                    'confirm_password.same' => 'Mật khẩu mới không trùng khớp.'
                ]
            );

            $user = Admin::find(Auth::user()->id);

            $current = $request->current_password;

            $new = $request->new_password;

            if(Hash::check($current, $user->password))
            {
                if($current !== $new)
                {
                    $user->password = Hash::make($new);
                    if($user->save())
                        return redirect()->back()->with(['flag' => 'success', 'message' => 'Mật khẩu đã được thay đổi!']);
                    else
                        return redirect()->back()->with(['flag' => 'danger', 'message' => 'Không thể thay đổi mật khẩu!']);
                }
                else
                    return redirect()->back()->with(['flag' => 'danger', 'message' => 'Mật khẩu mới phải khác mật khẩu hiện tại!']);
            }
            else
                return redirect()->back()->with(['flag' => 'danger', 'message' => 'Mật khẩu hiện tại không chính xác!']);
        }
        return redirect()->route('trungtam.login');
    }
    /************************CÁ NHÂN************************/


    /************************GIA SƯ************************/
    //Lấy danh sách gia sư
    public function giasu()
    {
        if(Auth::check())
        {
            $getMonHoc = mon_hoc::all();
            $getLopHoc = lop_hoc::all();
            $getKhuVuc = khu_vuc::all();
            $giasu = gia_su::orderBy('MaGiaSu', 'desc')->get();
            return view('trungtam.gia-su', compact('getMonHoc', 'getLopHoc', 'getKhuVuc', 'giasu'));
        }
        return redirect()->route('trungtam.login');
    }

    //Xoá gia sư
    public function giasu_delete(Request $request)
    {
        if(Auth::check())
        {
            $return[] = '';
            if($request->ajax()){
                $giasu= gia_su::find($request->input('id'));
                if($giasu->delete())
                    $return = ['flag' => 'success', 'message' => 'Thực hiện thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể xoá!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];

            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }

    //Thay đổi Status Gia sư
    public function giasu_status(Request $request)
    {
        if(Auth::check())
        {
            if($request->ajax())
            {
                $return[] = '';
                $giasu = gia_su::find($request->input('id'));
                if ($request->status == "enable")
                    $giasu->update(['Status' => 0]);
                else
                    $giasu->update(['Status' => 1]);

                if ($giasu->save())
                {
                    if(!empty($giasu->Email))
                    {
                        Mail::send('emails.dk_gia_su', ['hoten' => $giasu->HoTen, 'trangthai' => 'Thông tin đăng ký của anh(chị) đã được xét đuyệt thành công, anh(chị) vui lòng đăng nhập và tìm lớp để dạy nhé.'], function($message) use ($giasu)
                        {
                            $message->from('giasulequydonhcm@gmail.com', 'Gia sư Lê Quý Đôn')
                                ->subject('Kích Hoạt Tài Khoản')
                                ->to($giasu->Email, $giasu->Hoten);
                        });
                    }

                    $return = ['flag' => 'success', 'message' => 'Thay đổi thành công!'];
                }
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể thay đổi!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];

            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }

    //Thay đổi số đt, reset password
    public function giasu_sdt_password()
    {
        if(Auth::check())
        {
            $giasu = gia_su::all();
            return view('trungtam.sdt-password', compact('giasu'));
        }
        return redirect()->route('trungtam.login');
    }

    //Thay đổi số đt, reset password
    public function giasu_sdt_password_change(Request $request)
    {
        if(Auth::check())
        {
            if(!empty($request->thaydoi))
            {
                $count = gia_su::where('SoDT', $request->phone)->where('MaGiaSu', '!=', $request->id)->count();
                if($count != 0)
                    return redirect()->back()->with(['flag'=>'danger','message'=>'Số điện thoại đã được sử dụng!']);
                else
                {
                    $giasu = gia_su::find($request->input('id'));
                    $giasu->SoDT =  $request->phone;

                    if($giasu->save())
                        return redirect()->back()->with(['flag'=>'success','message'=>'Số ' .  $request->phone . ' đã được thiết lập cho gia sư '. $giasu->HoTen]);
                    else
                        return redirect()->back()->with(['flag'=>'danger','message'=>'Không thể thiết lập số điện thoại!']);
                }
            }

            if(!empty($request->pass))
            {
                $giasu = gia_su::find($request->input('id'));
                $password = 'Matkhau'.rand();
                $giasu->MatKhau = Hash::make($password);

                if($giasu->save())
                    return redirect()->back()->with(['flag'=>'success','message'=>'Mật khẩu mới cho gia sư '. $giasu->HoTen . ' là: ' . $password]);
                else
                    return redirect()->back()->with(['flag'=>'danger','message'=>'Không thể khôi phục mật khẩu!']);
            }
        }
        return redirect()->route('trungtam.login');
    }
    /************************GIA SƯ************************/

    /************************PHIẾU ĐĂNG KÝ************************/
    //Lấy danh sách phiếu đăng ký
    public function phieudangky()
    {
        if(Auth::check())
        {
            $phieudk = phieu_dang_ky::orderBy('MaPhieuDK', 'desc')->get();
            $getLopHoc = lop_hoc::all();
            return view('trungtam.phieu_dang_ky', compact('phieudk', 'getLopHoc'));
        }
        return redirect()->route('trungtam.login');
    }

    //Lấy danh sách phiếu đăng ký theo ID
    public function phieudangky_id(Request $request)
    {
        if(Auth::check())
        {
            if($request->ajax())
            {
                $id = $request->id;
                $phieudk = phieu_dang_ky::find($id);

                $result = array(
                    'MaPhieuDK' => $phieudk->MaPhieuDK,
                    'HoTen' => $phieudk->HoTen,
                    'SoDT' => $phieudk->SoDT,
                    'Email' => $phieudk->Email,
                    'DiaChi' => $phieudk->DiaChi,
                    'Lop' => $phieudk->Lop,
                    'MonHoc' => $phieudk->MonHoc,
                    'SoBuoiDay' => $phieudk->SoBuoiDay,
                    'ThoiGianHoc' => $phieudk->ThoiGianHoc,
                    'SoHocSinh' => $phieudk->SoHocSinh,
                    'HocLuc' => $phieudk->HocLuc,
                    'YeuCau' => $phieudk->YeuCau,
                    'YeuCauKhac' => $phieudk->YeuCauKhac
                );
                return json_encode($result);
            }
        }
        return redirect()->route('trungtam.login');
    }

    //Thêm phiếu đăng ký mới
    public function phieudangky_add(Request $request)
    {
        if(Auth::check())
        {
            if(!empty($request->themdk))
            {
                if($request->themdk == "Add")
                {
                    $phieu = new phieu_dang_ky();

                    $phieu->HoTen = $request->hoten;
                    $phieu->DiaChi = $request->diachi;
                    $phieu->SoDT = $request->dienthoai;
                    $phieu->Email = $request->email;

                    $phieu->Lop = $request->lop;
                    $phieu->Monhoc = $request->monhoc;

                    $phieu->SoHocSinh = $request->sohs;
                    $phieu->HocLuc = $request->hocluc;

                    $phieu->SoBuoiDay = $request->sobuoi;
                    $phieu->ThoiGianHoc = $request->thoigian;
                    $phieu->YeuCau = $request->yeucau;
                    $phieu->YeuCauKhac = $request->yeucaukhac;

                    if ($phieu->save())
                        return redirect()->back()->with(['flag' => 'success', 'message' => 'Lớp học được tạo thành công! Vui lòng chọn tiếp chức năng xét duyệt để hoàn tất việc mở lớp.']);
                    else
                        return redirect()->back()->with(['flag' => 'danger', 'message' => 'Xin lỗi! Có lỗi xảy ra. Vui lòng thử lại!']);
                }

                //Duyệt
                else if($request->themdk == "Accept")
                {
                    $phieu = new phieu_mo_lop();
                    $phieu->MaPhieuDK = $request->maphieudk;
                    $phieu->LopDay = $request->lop;
                    $phieu->MonDay = $request->monhoc;
                    $phieu->DiaChi = $request->diachitt;
                    $phieu->Luong = $request->luong;
                    $phieu->LePhi = $request->mucphi;
                    $phieu->SoBuoiDay = $request->sobuoi;
                    $phieu->ThoiGian = $request->thoigian;
                    $phieu->ThongTin = $request->thongtinhs;
                    $phieu->YeuCau = $request->yeucautt;

                    //Tạo slug
                    if (empty($request->lop))
                        $slug = $this->to_slug($request->monhoc) . '-' . rand();
                    else
                        $slug = $this->to_slug($request->monhoc) . '-' . $this->to_slug($request->lop) . '-' . rand();

                    $phieu->slug = $slug;

                    if ($phieu->save())
                    {
                        $phieudk = phieu_dang_ky::find($request->maphieudk);

                        $phieudk->update([
                            'HoTen' => $request->hoten,
                            'DiaChi' => $request->diachi,
                            'SoDT' => $request->dienthoai,
                            'Email' => $request->email,
                            'Lop' => $request->lop,
                            'Monhoc' => $request->monhoc,
                            'SoHocSinh' => $request->sohs,
                            'HocLuc' => $request->hocluc,
                            'SoBuoiDay' => $request->sobuoi,
                            'ThoiGianHoc' => $request->thoigian,
                            'YeuCau' => $request->yeucau,
                            'YeuCauKhac' => $request->yeucaukhac,
                            'Status' => 1
                        ]);

                        $phieudk->save();

                        if(!empty($request->email))
                        {
                            Mail::send('emails.tim_gia_su', ['hoten' => $request->hoten, 'trangthai' => 'Thông tin đăng ký của anh(chị) đã được xét duyệt thành công, anh(chị) vui lòng chờ gia sư nhận dạy.'], function($message) use ($request)
                            {
                                $message->from('giasulequydonhcm@gmail.com', 'Gia sư Lê Quý Đôn')
                                    ->subject('Phản Hồi Đăng Ký Tìm Gia Sư')
                                    ->to($request->email, $request->hoten);
                            });
                        }

                        return redirect()->back()->with(['flag' => 'success', 'message' => 'Lớp đã được thêm!']);
                    }
                    else
                        return redirect()->back()->with(['flag'=>'danger','message'=>'Không thể thêm lớp!']);
                }
            }
        }
        return redirect()->route('trungtam.login');
    }

    //Xoá đăng ký lớp
    public function phieudangky_delete(Request $request)
    {
        if(Auth::check())
        {
            $return[] = '';
            if($request->ajax()){
                $phieu= phieu_dang_ky::find($request->input('id'));
                if($phieu->delete())
                    $return = ['flag' => 'success', 'message' => 'Thực hiện thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể xoá!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];

            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }

    //Thay đổi Status
    public function phieudangky_status(Request $request)
    {
        if(Auth::check())
        {
            if($request->ajax())
            {
                $return[] = '';
                $phieu = phieu_dang_ky::find($request->input('id'));
                if ($request->status == "enable")
                    $phieu->update(['Status' => 0]);
                else
                    $phieu->update(['Status' => 1]);

                if ($phieu->save())
                    $return = ['flag' => 'success', 'message' => 'Thay đổi thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể thay đổi!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];

            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }

    /************************PHIẾU ĐĂNG KÝ************************/


    /************************MỞ LỚP************************/
    //Lấy danh sách lớp đang mở
    public function phieumolop()
    {
        if(Auth::check())
        {
            $dslop = phieu_mo_lop::orderBy('MaPhieuMo', 'desc')->get();
            $getLopHoc = lop_hoc::all();

            return view('trungtam.phieu_mo_lop', compact('dslop', 'getLopHoc'));
        }

        return redirect()->route('trungtam.login');
    }

    //Chỉnh sửa lớp
    public function phieumolop_edit(Request $request)
    {
        if(Auth::check())
        {
            $phieu = phieu_mo_lop::find($request->id);

            $phieu->LopDay = $request->lop;
            $phieu->MonDay = $request->mon;
            $phieu->DiaChi = $request->diachi;
            $phieu->Luong = $request->luong;
            $phieu->LePhi = $request->mucphi;
            $phieu->SoBuoiDay = $request->sobuoi;
            $phieu->ThoiGian = $request->thoigian;
            $phieu->ThongTin = $request->thongtin;
            $phieu->YeuCau = $request->yeucau;

            //Tạo slug
            if (empty($request->lop))
                $slug = $this->to_slug($request->mon) . '-' . rand();
            else
                $slug = $this->to_slug($request->mon) . '-' . $this->to_slug($request->lop) . '-' . rand();

            if($phieu->slug != $slug)
                $phieu->slug = $slug;
            else
                $phieu->slug;

            if($phieu->save())
                return redirect()->back()->with(['flag' => 'success', 'message' => 'Cập nhật thông tin thành công!']);
            else
                return redirect()->back()->with(['flag'=>'danger','message'=>'Không thể cập nhật thông tin!']);
        }
        return redirect()->route('trungtam.login');
    }

    //Thay đổi Status
    public function phieumolop_status(Request $request)
    {
        if(Auth::check())
        {
            if($request->ajax())
            {
                $return[] = '';
                $lop = phieu_mo_lop::find($request->input('id'));
                if ($request->status == "enable")
                    $lop->update(['Status' => 0]);
                else
                    $lop->update(['Status' => 1]);

                if ($lop->save())
                    $return = ['flag' => 'success', 'message' => 'Thay đổi thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể thay đổi!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];

            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }

    //Xoá lớp đang mở
    public function phieumolop_delete(Request $request)
    {
        if(Auth::check())
        {
            $return[] = '';
            if($request->ajax()){
                $lop = phieu_mo_lop::find($request->input('id'));
                if($lop->delete())
                    $return = ['flag' => 'success', 'message' => 'Thực hiện thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể xoá!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];

            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }

    /************************MỞ LỚP************************/


    /************************HỌC PHÍ************************/
    //Học phí tham khảo
    public function hocphi()
    {
        if(Auth::check())
        {
            $hocphi = HocPhi::all();
            return view('trungtam.hocphi', compact('hocphi'));
        }
        return redirect()->route('trungtam.login');
    }

    //Chỉnh sửa học phí
    function hocphi_edit(Request $request)
    {
        if(Auth::check())
        {
            $hp1 = HocPhi::where('MaHP', 1)->get()->first();
            $hp1->SinhVien = $request->sv1;
            $hp1->GiaoVien = $request->gv1;
            $hp1->save();

            $hp2 = HocPhi::where('MaHP', 2)->get()->first();
            $hp2->SinhVien = $request->sv2;
            $hp2->GiaoVien = $request->gv2;
            $hp2->save();

            $hp3 = HocPhi::where('MaHP', 3)->get()->first();
            $hp3->SinhVien = $request->sv3;
            $hp3->GiaoVien = $request->gv3;
            $hp3->save();

            $hp4 = HocPhi::where('MaHP', 4)->get()->first();
            $hp4->SinhVien = $request->sv4;
            $hp4->GiaoVien = $request->gv4;
            $hp4->save();

            $hp5 = HocPhi::where('MaHP', 5)->get()->first();
            $hp5->SinhVien = $request->sv5;
            $hp5->GiaoVien = $request->gv5;
            $hp5->save();

            $hp6 = HocPhi::where('MaHP', 6)->get()->first();
            $hp6->SinhVien = $request->sv6;
            $hp6->GiaoVien = $request->gv6;
            $hp6->save();

            $hp7 = HocPhi::where('MaHP', 7)->get()->first();
            $hp7->SinhVien = $request->sv7;
            $hp7->GiaoVien = $request->gv7;
            $hp7->save();

            $hp8 = HocPhi::where('MaHP', 8)->get()->first();
            $hp8->SinhVien = $request->sv8;
            $hp8->GiaoVien = $request->gv8;
            $hp8->save();

            $hp9 = HocPhi::where('MaHP', 9)->get()->first();
            $hp9->SinhVien = $request->sv9;
            $hp9->GiaoVien = $request->gv9;
            $hp9->save();

            $hp10 = HocPhi::where('MaHP', 10)->get()->first();
            $hp10->SinhVien = $request->sv10;
            $hp10->GiaoVien = $request->gv10;
            $hp10->save();

            $hp11 = HocPhi::where('MaHP', 11)->get()->first();
            $hp11->SinhVien = $request->sv11;
            $hp11->GiaoVien = $request->gv11;
            $hp11->save();

            $hp12 = HocPhi::where('MaHP', 12)->get()->first();
            $hp12->SinhVien = $request->sv12;
            $hp12->GiaoVien = $request->gv12;
            $hp12->save();

            $hp13 = HocPhi::where('MaHP', 13)->get()->first();
            $hp13->SinhVien = $request->sv13;
            $hp13->GiaoVien = $request->gv13;
            $hp13->save();

            $hp14 = HocPhi::where('MaHP', 14)->get()->first();
            $hp14->SinhVien = $request->sv14;
            $hp14->GiaoVien = $request->gv14;
            $hp14->save();

            $hp15 = HocPhi::where('MaHP', 15)->get()->first();
            $hp15->SinhVien = $request->sv15;
            $hp15->GiaoVien = $request->gv15;
            $hp15->save();

            $hp16 = HocPhi::where('MaHP', 16)->get()->first();
            $hp16->SinhVien = $request->sv16;
            $hp16->GiaoVien = $request->gv16;
            $hp16->save();

            return redirect()->back()->with(['flag'=>'success','message'=>'Học phí đã được thay đổi!']);
        }
        return redirect()->route('trungtam.login');
    }

    /************************HỌC PHÍ************************/

    /************************TUYỂN DỤNG************************/

    //Lấy danh sách tuyển dụng
    public function tuyendung()
    {
        if(Auth::check())
        {
            $tuyendung = TuyenDung::orderBy('MaPhieuTD', 'desc')->get();

            return view('trungtam.tuyen_dung', compact('tuyendung'));
        }
        return redirect()->route('trungtam.login');
    }

    //Lấy danh sách phiếu tuyển dung theo ID
    public function tuyendung_id(Request $request)
    {
        if(Auth::check())
        {
            if($request->ajax())
            {
                $id = $request->id;
                $tuyendung = TuyenDung::find($id);

                $result = array(
                    'MaPhieuTD' => $tuyendung->MaPhieuTD,
                    'TenPhieuTD' => $tuyendung->TenPhieuTD,
                    'NoiLam' => $tuyendung->NoiLam,
                    'SoLuong' => $tuyendung->SoLuong,
                    'HoSo' => $tuyendung->HoSo,
                    'YeuCau' => $tuyendung->YeuCau,
                    'ThoiGian' => $tuyendung->ThoiGian,
                    'Luong' => $tuyendung->Luong,
                    'MotaCV' => $tuyendung->MotaCV,
                    'QuyenLoi' => $tuyendung->QuyenLoi
                );
                return json_encode($result);
            }
        }
        return redirect()->route('trungtam.login');
    }

    //Thêm tuyển dụng
    public function tuyendung_add(Request $request)
    {
        if(Auth::check())
        {
            $return[] = '';
            if ($request->ajax())
            {
                $phieu = new TuyenDung();
                $phieu->TenPhieuTD = $request->tenphieu;
                $phieu->NoiLam = $request->noilam;
                $phieu->SoLuong = $request->soluong;
                $phieu->HoSo = $request->hoso;
                $phieu->YeuCau = $request->yeucau;
                $phieu->ThoiGian = $request->thoigian;
                $phieu->Luong = $request->luong;
                $phieu->MotaCV = $request->mota;
                $phieu->QuyenLoi = $request->quyenloi;

                if ($phieu->save())
                    $return = ['flag' => 'success', 'message' => 'Thêm thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể thêm!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];
            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }

    //Chỉnh sửa tuyển dụng
    public function tuyendung_edit(Request $request)
    {
        if(Auth::check())
        {
            $return[] = '';
            if ($request->ajax())
            {
                $phieu = TuyenDung::find($request->maphieu);
                $phieu->TenPhieuTD = $request->tenphieu;
                $phieu->NoiLam = $request->noilam;
                $phieu->SoLuong = $request->soluong;
                $phieu->HoSo = $request->hoso;
                $phieu->YeuCau = $request->yeucau;
                $phieu->ThoiGian = $request->thoigian;
                $phieu->Luong = $request->luong;
                $phieu->MotaCV = $request->mota;
                $phieu->QuyenLoi = $request->quyenloi;

                if ($phieu->save())
                    $return = ['flag' => 'success', 'message' => 'Chỉnh sửa thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể chỉnh sửa!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];
            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }

    //Xoá tuyển dụng
    public function tuyendung_delete(Request $request)
    {
        if(Auth::check())
        {
            $return[] = '';
            if($request->ajax()){
                $tuyendung = TuyenDung::find($request->input('id'));
                if($tuyendung->delete())
                    $return = ['flag' => 'success', 'message' => 'Thực hiện thành công!'];
                else
                    $return = ['flag' => 'danger', 'message' => 'Không thể xoá!'];
            }
            else
                $return = ['flag' => 'danger', 'message' => 'Lỗi truyền dữ liệu!'];
            return json_encode($return);
        }
        return redirect()->route('trungtam.login');
    }

    /************************TUYỂN DỤNG************************/


    /************************NHẬN DẠY************************/

    //Lấy danh sách đăng ký dạy
    public function nhanlop()
    {
        if(Auth::check())
        {
            $nhanlop = DB::table('phieu_nhan_lop as n')
                ->leftJoin('phieu_mo_lop as m','m.MaPhieuMo','=','n.MaPhieuMo')
                ->leftJoin('gia_su as g','g.MaGiaSu','=','n.MaGiaSu')
                ->select('n.MaPhieuNhan','n.MaPhieuMo','n.MaGiaSu', 'g.HoTen', 'g.TrinhDo', 'm.LopDay', 'm.MonDay', 'n.HinhThucNhan', 'm.LePhi', 'm.Luong',
                    'n.NgayDay', 'n.GioDay', 'n.NoiDungThem', 'n.Status', 'n.created_at')
                ->orderBy('MaPhieuNhan', 'desc')
                ->get();

            return view('trungtam.phieu_nhan_lop', compact('nhanlop'));
        }

        return redirect()->route('trungtam.login');
    }

    //Xét duyệt cho danh sách đăng ký dạy
    public function nhanlop_status(Request $request)
    {
        if(Auth::check())
        {
            $status = $request->status;
            $phieunhan = phieu_nhan_lop::find($request->input('maphieunhan'));
            $maphieumo = $request->maphieumo;
            $giasu = gia_su::find($phieunhan->MaGiaSu);
            $validCommands = [0, 1, 2, 3, 4];

            $tatus_str = '';
            switch($status)
            {
                case 0: $tatus_str = 'CHỜ DUYỆT'; break;
                case 1: $tatus_str = 'XEM XÉT'; break;
                case 2: $tatus_str = 'ĐỦ ĐIỀU KIỆN'; break;
                case 3: $tatus_str = 'ĐÃ NHẬN'; break;
                case 4: $tatus_str = 'KHÔNG ĐẠT';
            }

            if (in_array($status, $validCommands))
            {
                $phieunhan->Status = $status;

                if($phieunhan->save())
                {
                    if(!empty($giasu->Email))
                    {
                        Mail::send('emails.dk_gia_su', ['hoten' => $giasu->HoTen, 'trangthai' => 'Trạng thái thông tin nhận dạy của anh(chị) hiện tại là ' . $tatus_str .', anh(chị) vui lòng đăng nhập và kiểm tra lại trong danh sách nhé.'], function($message) use ($giasu)
                        {
                            $message->from('giasulequydonhcm@gmail.com', 'Gia sư Lê Quý Đôn')
                                ->subject('Trạng Thái Nhận Dạy')
                                ->to($giasu->Email, $giasu->Hoten);
                        });
                    }

                    //Đã nhận thành công
                    if($status == 3)
                    {
                        $phieumo = phieu_mo_lop::find($maphieumo);
                        $phieumo->update(['Status'=>'1']);
                        $phieumo->save();
                    }

                    return redirect()->back()->with(['flag'=>'success','message'=>'Thay đổi thành công!']);
                }
                else
                    return redirect()->back()->with(['flag'=>'danger','message'=>'Thay đổi không thành công!']);
            }
            else
                return redirect()->back()->with(['flag'=>'danger','message'=>'Xin lỗi giá trị không hợp lệ!']);
        }

        return redirect()->route('trungtam.login');
    }

    /************************NHẬN DẠY************************/
}
