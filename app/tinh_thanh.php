<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tinh_thanh extends Model
{
    protected $table = 'tinh_thanh';
    protected $primaryKey = 'MaTinh';
    public $timestamps = false;

    public function tinhthanh()
    {
        return $this->hasMany('App\khu_vuc', 'MaTinh', 'MaTinh');
    }


    protected $fillable = [
        'TenTinh',
    ];
}
