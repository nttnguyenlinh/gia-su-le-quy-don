<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HocPhi extends Model
{
    protected $table = 'hoc_phi';
    protected $primaryKey = 'MaHP';
    public $timestamps = false;
}
