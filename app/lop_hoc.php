<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class lop_hoc extends Model
{
    protected $table = 'lop_hoc';
    protected $primaryKey = 'MaLop';
    public $timestamps = false;

    protected $fillable = [
        'TenLop',
    ];
}
