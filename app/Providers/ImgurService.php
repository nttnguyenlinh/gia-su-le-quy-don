<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client as GuzzleClient;

class ImgurService extends ServiceProvider
{
    const END_POINT = "https://api.imgur.com/3/image";
    const CLIENT_ID = "6e8e6778a035a5b";

    //Version cURL
    public static function uploadImage($image)
    {
        //Imgur Upload
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, ImgurService::END_POINT);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60000);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . ImgurService::CLIENT_ID));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array('image' => $image));

        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response, true);
        return $data['data']['link'];
        //End Imgur Upload
    }


    //Version GuzzleClient
    public static function ImgurImage($image)
    {
        $client = new GuzzleClient();
        $request = $client->request(
            'POST',
            ImgurService::END_POINT,
            [
                'headers' => ['Authorization' => "Client-ID ". ImgurService::CLIENT_ID],
                'form_params' => ['image' => $image]
            ]
        );
        $response = (string) $request->getBody();
        $jsonResponse = json_decode($response);
        return $jsonResponse->data->link; // return url of image
    }

    public static function ImgurAuthUrl()
    {
        return 'https://api.imgur.com/oauth2/authorize'.'?client_id='.ImgurService::CLIENT_ID.'&response_type=token';
    }
}
