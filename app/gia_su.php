<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gia_su extends Model
{
    protected $table = 'gia_su';
    protected $primaryKey = 'MaGiaSu';

    protected $fillable = [
        'HoTen', 'NgaySinh', 'GioiTinh', 'NguyenQuan', 'GiongNoi', 'DiaChi', 'CMND', 'Email',
        'SoDT', 'MatKhau', 'AnhThe', 'TruongDaoTao', 'NganhHoc', 'CongTac', 'NamTN', 'TrinhDo', 'LoaiGiaoVien',
        'UuDiem', 'MonDay', 'LopDay', 'TinhThanh', 'KhuVuc', 'Status'
    ];
}
