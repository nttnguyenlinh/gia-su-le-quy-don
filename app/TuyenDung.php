<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TuyenDung extends Model
{
    protected $table = 'phieu_tuyen_dung';
    protected $primaryKey = 'MaPhieuTD';
    public $timestamps = false;

    protected $fillable = [
        'TenPhieuTD', 'NoiLam', 'SoLuong', 'HoSo', 'YeuCau', 'ThoiGian', 'Luong', 'MotaCV', 'QuyenLoi'
    ];
}
