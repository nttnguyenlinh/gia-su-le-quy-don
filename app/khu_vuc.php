<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class khu_vuc extends Model
{
    protected $table = 'khu_vuc';
    protected $primaryKey = 'MaKV';
    public $timestamps = false;

    public function khuvuc()
    {
        return $this->belongsTo('App\tinh_thanh', 'MaTinh', 'MaTinh');
    }

    protected $fillable = [
        'TenKV',
    ];
}
