<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class phieu_nhan_lop extends Model
{
    protected $table = 'phieu_nhan_lop';
    protected $primaryKey = 'MaPhieuNhan';

    protected $fillable = [
        'MaPhieuMo', 'MaGiaSu', 'HinhThucNhan', 'NgayDay', 'GioDay', 'LePhi', 'NoiDungThem', 'Status'
    ];

    public function phieu_mo_lop()
    {
        return $this->belongsTo('App\phieu_mo_lop', 'MaPhieuMo', 'MaPhieuMo');
    }

    public function gia_su()
    {
        return $this->belongsTo('App\gia_su', 'MaGiaSu', 'MaGiaSu');
    }
}
