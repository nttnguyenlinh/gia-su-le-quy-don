<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class phieu_dang_ky extends Model
{
    protected $table = 'phieu_dang_ky';
    protected $primaryKey = 'MaPhieuDK';
    protected $fillable = [
        'HoTen', 'DiaChi', 'SoDT', 'Email', 'Lop', 'Monhoc', 'GiaSu', 'SoHocSinh', 'HocLuc',
        'SoBuoiDay', 'ThoiGianHoc', 'YeuCau', 'YeuCauKhac',  'Status'
    ];
}
