<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class phieu_mo_lop extends Model
{
    protected $table = 'phieu_mo_lop';
    protected $primaryKey = 'MaPhieuMo';

    protected $fillable = [
        'MaPhieuDK', 'LopDay', 'MonDay', 'DiaChi', 'Luong', 'LePhi', 'SoBuoiDay', 'ThoiGian', 'ThongTin', 'YeuCau', 'slug', 'Status'
    ];
}
