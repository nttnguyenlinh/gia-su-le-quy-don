<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mon_hoc extends Model
{
    protected $table = 'mon_hoc';
    protected $primaryKey = 'MaMon';
    public $timestamps = false;

    protected $fillable = [
        'TenMon',
    ];
}
