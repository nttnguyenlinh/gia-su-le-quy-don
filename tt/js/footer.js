//Đóng thông báo
$(".alert").fadeTo(3500, 600).slideUp(400, function () {
    $(".alert").slideUp(600);
});

//Kiểm tra password
$.validator.addMethod("pass_regex", function (value) {
    return /(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/.test(value);
});

$("#form-login").validate({
    rules: {
        username: {required: true},
        password:{required: true}
    },

    messages:{
        username: {
            required: 'Vui lòng nhập tên tài khoản!'
        },

        password: {
            required: 'Vui lòng nhập mật khẩu!'
        }
    }
});

// var numberMask = new IMask(document.getElementById('luong'), {
//     mask: Number,
//     scale: 0,
//     thousandsSeparator: '.',
//     normalizeZeros: true,
//     min: 0,
//     max: 10000000
// });

$('#luong').mask("#.##0", {reverse: true});

/*****CUSTOM CHOSEN SELECT********/
$('.chosen-select').chosen({allow_single_deselect:true});
//resize the chosen on window resize

$(window).off('resize.chosen').on('resize.chosen', function() {
    $('.chosen-select').each(function() {
        $(this).next().css({'width': $(this).parent().width()});
    })
}).trigger('resize.chosen');
//resize chosen on sidebar collapse/expand

$(document).on('settings.ace.chosen', function(event_name) {
    if(event_name != 'sidebar_collapsed') return;

    $('.chosen-select').each(function() {
        $(this).next().css({'width': $(this).parent().width()});
    })
});
/*****CUSTOM CHOSEN SELECT********/

/************************CÁ NHÂN************************/
//Cập nhật thông tin
$("#form_profile_update").validate({
    rules: {
        name: {
            required: true,
            minlength: 4,
            maxlength: 30
        },

        email: {
            required: true,
            email: true
        }
    },

    messages: {
        name: {
            required: 'Vui lòng nhập tên bạn!',
            minlength: 'Tên bạn ít nhất 4 kí tự!',
            maxlength: 'Tên bạn tối đa 30 kí tự!'
        },

        email: {
            required: 'Vui lòng nhập email!',
            email: 'Email không hợp lệ!'
        }
    }
});

//Form thay đổi mật khẩu
$("#form_profile_change").validate({
    rules: {
        current_password: {
            required: true
        },
        new_password: {
            required: true,
            pass_regex: true
        },
        confirm_password: {
            required: true,
            equalTo: '#new_password'
        },
    },

    messages: {
        current_password: {
            required: 'Vui lòng nhập mật khẩu hiện tại!'
        },

        new_password: {
            required: 'Vui lòng nhập mật khẩu mới!',
            pass_regex: 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, tối thiểu 6 kí tự!'
        },
        confirm_password: {
            required: 'Vui lòng nhập lại mật khẩu mới!',
            equalTo: 'Mật khẩu không trùng khớp!'
        },
    }
});
/************************CÁ NHÂN************************/


/************************NGƯỜI DÙNG************************/
//Hiển thị thông tin chi tiết
$('.show-details-btn').on('click', function(e) {
    e.preventDefault();
    $(this).closest('tr').next().toggleClass('open');
    //$(this).find(ace.vars['.icon']).toggleClass('fal fa-chevron-double-down').toggleClass('fal fa-chevron-double-up');
});

//Thêm người dùng
$("#form_users_add").validate({
    rules: {
        name: {
            required: true,
            minlength: 4,
            maxlength: 30
        },

        username: {
            required: true,
            minlength: 4,
            maxlength: 30
        },

        password: {
            required: true,
            pass_regex: true
        },

        email: {
            required: true,
            email: true
        }
    },

    messages: {
        name: {
            required: 'Vui lòng nhập họ tên!',
            minlength: 'Họ tên ít nhất 4 kí tự!',
            maxlength: 'Họ tên tối đa 30 kí tự!'
        },

        username: {
            required: 'Vui lòng nhập tài khoản!',
            minlength: 'Tài khoản ít nhất 4 kí tự!',
            maxlength: 'Tài khoản tối đa 30 kí tự!'
        },

        password: {
            required: 'Vui lòng nhập mật khẩu!',
            pass_regex: 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, có ít nhất 6 kí tự!'
        },

        email: {
            required: 'Vui lòng nhập email!',
            email: 'Email không hợp lệ!'
        }
    }
});

//Xoá người dùng
$(document).on('click', '.users_delete', function () {
    var id = $(this).attr('id');
    if (confirm("Bạn muốn xoá người dùng (ID: " + id + ") này?"))
        $.ajax({
            url: "xoa-nguoi-dung",
            type: "get",
            data: {id: id},
            success: function (data) {
                var retData = JSON.parse(data);
                toastr[retData.flag](retData.message);

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                setTimeout(function(){
                    location.reload();
                }, 1000);
            }
        });
    else
        return false;
});

//Reset mật khẩu
$(document).on('click', '.users_password', function () {
    var id = $(this).attr('id');
    if (confirm("Bạn muốn tạo mật khẩu mới cho người dùng (ID: " + id + ") này?"))
        $.ajax({
            url: "khoi-phuc-mk",
            type: "get",
            data: {id: id},
            success: function (data) {
                window.prompt("Bấm: Ctrl+C, Enter để copy mật khẩu!" , data);
                $("#table_users").load("nguoi-dung.html #table_users");
            }
        });
    else
        return false;
});

//Thay đổi trạng thái người dùng
$(document).on('click', '.users-status', function () {
    var id = $(this).attr('id');
    var status = $(this).attr('data-status');
    $.ajax({
        url: "trang-thai-nguoi-dung",
        type: "get",
        data: {id: id, status: status},
        success: function(data) {
            var retData = JSON.parse(data);
            toastr[retData.flag](retData.message);

            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            setTimeout(function(){
                location.reload();
            }, 1000);
        }
    });
});

/************************NGƯỜI DÙNG************************/


/************************GIA SƯ************************/
//Bảng gia sư
$('#table_giasu').DataTable( {
    bAutoWidth: true,
    "aoColumns": [
        null, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }
    ],
    "aaSorting": [],

    select: {
        style: 'single'
    }
});

//Xoá gia sư
$(document).on('click', '.giasu_delete', function () {
    var id = $(this).attr('id');
    if (confirm("Bạn muốn xoá gia sư (ID:" + id + ") này?"))
        $.ajax({
            url: "xoa-gia-su",
            type: "get",
            data: {id: id},
            success: function (data) {
                var retData = JSON.parse(data);
                toastr[retData.flag](retData.message);

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                $("#table_giasu").load("gia-su.html #table_giasu");
            }
        });
    else
        return false;
});

//Thay đổi trạng thái gia sư
$(document).on('click', '.giasu_status', function () {
    var id = $(this).attr('id');
    var status = $(this).attr('data-status');
    $.ajax({
        url: "trang-thai-gia-su",
        type: "get",
        data: {id: id, status: status},
        success: function (data) {
            var retData = JSON.parse(data);
            toastr[retData.flag](retData.message);

            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            setTimeout(function(){
                location.reload();
            }, 1000);
        }
    });
});

/************************GIA SƯ************************/


/************************PHIẾU ĐĂNG KÝ************************/

//Bảng phiếu đăng ký
$('#table_phieudangky').DataTable({
    bAutoWidth: false,
    "aoColumns": [
        null, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }
    ],
    "aaSorting": [],
    select: {style: 'single' }
});

//Xoá phiếu đăng ký
$(document).on('click', '.phieudangky_delete', function () {
    var id = $(this).attr('id');
    if (confirm("Bạn muốn xoá phiếu đăng ký (ID:" + id + ") này?"))
        $.ajax({
            url: "xoa-dang-ky",
            type: "get",
            data: {id: id},
            success: function (data) {
                var retData = JSON.parse(data);
                toastr[retData.flag](retData.message);

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                $("#table_phieudangky").load("danh-sach-dang-ky.html #table_phieudangky");
            }
        });
    else
        return false;
});

//Thêm mới và xét duyệt
var phieudangky_dialog_add;

phieudangky_dialog_add = $("#phieudangky_dialog_add").dialog({
    autoOpen: false,
    resizable: false,
    dialogClass: 'no-close no-titlebar .no-position'
});

//Bật form thêm mới
$(".phieudangky_add").button().on("click", function() {

    document.getElementById('widget-title-dangky').innerHTML = "Thêm lớp mới";
    $('#themdk').val("Add");
    $('#form_phieudangky_add')[0].reset();

    document.getElementById('hoten').readOnly = false;
    document.getElementById('diachi').readOnly = false;
    document.getElementById('dienthoai').readOnly = false;

    $('#yeucau').prop('disabled', false);

    document.getElementById('email').readOnly = false;
    document.getElementById('sohs').readOnly = false;
    document.getElementById('hocluc').readOnly = false;
    document.getElementById('yeucaukhac').readOnly = false;

    document.getElementById('diachitt').readOnly = true;
    document.getElementById('luong').readOnly = true;
    document.getElementById('mucphi').readOnly = true;
    document.getElementById('yeucautt').readOnly = true;
    document.getElementById('thongtinhs').readOnly = true;

    phieudangky_dialog_add.dialog("open");
});

//Nút đóng dialog
$(".close-dangky").button().on("click", function() {
    phieudangky_dialog_add.dialog("close");
});

//Bật form duyệt
$(document).on('click', '.phieudangky_apply', function() {

    document.getElementById('widget-title-dangky').innerHTML = "Xét duyệt lớp";
    $('#form_phieudangky_add')[0].reset();
    document.getElementById('diachitt').readOnly = false;
    document.getElementById('luong').readOnly = false;
    document.getElementById('mucphi').readOnly = false;
    document.getElementById('yeucautt').readOnly = false;
    document.getElementById('thongtinhs').readOnly = false;

    var id = $(this).attr("id");

    //Lấy dữ liệu
    $.ajax({
        url:"lay-thong-tin-dang-ky",
        method:"GET",
        data:{id:id},
        dataType:"json",
        success:function(data){
            $('#maphieudk').val(data.MaPhieuDK);

            $('#hoten').val(data.HoTen);
            $('#diachi').val(data.DiaChi);
            $('#dienthoai').val(data.SoDT);

            $('#email').val(data.Email);


            $('#lop').val(data.Lop);
            $('#monhoc').val(data.MonHoc);
            $('#sobuoi').val(data.SoBuoiDay);
            $('#thoigian').val(data.ThoiGianHoc);

            $('#sohs').val(data.SoHocSinh);
            $('#hocluc').val(data.HocLuc);
            $('#thongtinhs').val(data.SoHocSinh + ' - ' + data.HocLuc);

            $('#yeucau').val(data.YeuCau);
            $('#yeucaukhac').val(data.YeuCauKhac);
            $('#yeucautt').val(data.YeuCau + ' - ' + data.YeuCauKhac);

            $('#themdk').val("Accept");
            phieudangky_dialog_add.dialog("open");
        }
    });
});


/************************PHIẾU ĐĂNG KÝ************************/


/************************TUYỂN DỤNG************************/

$('#table-tuyendung')
    .DataTable( {
        bAutoWidth: false,
        "aoColumns": [
            null, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }
        ],
        "aaSorting": [],

        select: {
            style: 'single'
        }
    });

//Thêm mới và chỉnh sửa
var tuyendung_dialog_add;

tuyendung_dialog_add = $("#tuyendung_dialog_add").dialog({
    autoOpen: false,
    resizable: false,
    dialogClass: 'no-close no-titlebar .no-position'
});

//Nút đóng dialog
$(".close_add_tuyendung").button().on("click", function() {
    tuyendung_dialog_add.dialog("close");
});

//Bật form thêm mới
$(".tuyendung_add").button().on("click", function() {

    document.getElementById('widget-title-tuyendung').innerHTML = "Đăng tuyển dụng";
    $('#action').val("Add");
    $('#form_tuyendung_add')[0].reset();

    tuyendung_dialog_add.dialog("open");
});

//Bật form chỉnh sửa
$(document).on('click', '.tuyendung_edit', function() {

    document.getElementById('widget-title-tuyendung').innerHTML = "Chỉnh sửa tuyển dụng";
    $('#form_tuyendung_add')[0].reset();

    var id = $(this).attr("id");

    //Lấy dữ liệu
    $.ajax({
        url:"lay-thong-tin-tuyen-dung",
        method:"GET",
        data:{id:id},
        dataType:"json",
        success:function(data){
            $('#maphieu').val(data.MaPhieuTD);
            $('#tenphieu').val(data.TenPhieuTD);
            $('#noilam').val(data.NoiLam);
            $('#soluong').val(data.SoLuong);
            $('#hoso').val(data.HoSo);
            $('#yeucau').val(data.YeuCau);
            $('#mota').val(data.MotaCV);
            $('#thoigian').val(data.ThoiGian);
            $('#luong').val(data.Luong);
            $('#quyenloi').val(data.QuyenLoi);

            $('#action').val("Edit");
            tuyendung_dialog_add.dialog("open");
        }
    });
});

//Kiểm soát hành động Add & Edit
$('#form_tuyendung_add').on('submit', function(event){
    event.preventDefault();

    var action = document.getElementById("action").value;
    var form_data = $(this).serialize();

    if(action == "Add") {
        $.ajax({
            url: "them-tuyen-dung",
            type: "post",
            data: form_data,
            success: function (data) {
                var retData = JSON.parse(data);
                toastr[retData.flag](retData.message);

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                setTimeout(function(){
                    location.reload();
                }, 1000);
                tuyendung_dialog_add.dialog("close");
            }
        });
    }

    else if(action == "Edit") {
        $.ajax({
            url: "sua-tuyen-dung",
            type: "post",
            data: form_data,
            success: function (data) {
                var retData = JSON.parse(data);
                toastr[retData.flag](retData.message);

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                setTimeout(function(){
                    location.reload();
                }, 1000);
                tuyendung_dialog_add.dialog("close");
            }
        });
    }
});

//Xoá phiếu tuyển dụng
$(document).on('click', '.tuyendung_delete', function () {
    var id = $(this).attr('id');
    if (confirm("Bạn muốn xoá (ID:" + id + ") này?"))
        $.ajax({
            url: "xoa-tuyen-dung",
            type: "get",
            data: {id: id},
            success: function (data) {
                var retData = JSON.parse(data);
                toastr[retData.flag](retData.message);

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                setTimeout(function(){
                    location.reload();
                }, 1000);
            }
        });
    else
        return false;
});

/************************TUYỂN DỤNG************************/


/************************MỞ LỚP************************/

//Bảng phiếu mở lớp
$('#table-phieumolop').DataTable({
    bAutoWidth: false,
    "aoColumns": [
        null, null, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }
    ],
    "aaSorting": [],
    select: {style: 'single' }
});


//Xoá thông tin mở lớp
$(document).on('click', '.phieumolop_delete', function () {
    var id = $(this).attr('id');
    if (confirm("Bạn muốn xoá (ID:" + id + ") này?"))
        $.ajax({
            url: "xoa-lop",
            type: "get",
            data: {id: id},
            success: function (data) {
                var retData = JSON.parse(data);
                toastr[retData.flag](retData.message);

                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                setTimeout(function(){
                    location.reload();
                }, 1000);
            }
        });
    else
        return false;
});


//Chi tiết đăng ký
var dangky_dialog;

dangky_dialog = $("#dangky_dialog").dialog({
    autoOpen: false,
    resizable: false,
    dialogClass: 'no-close no-titlebar .no-position'
});

//Bật form chi tiết đăng ký
$(".phieumolop-dangky-detail").button().on("click", function() {
    var id = $(this).attr("id");

    //Lấy dữ liệu
    $.ajax({
        url:"lay-thong-tin-dang-ky",
        method:"GET",
        data:{id:id},
        dataType:"json",
        success:function(data){
            document.getElementById('maphieudk').innerHTML = data.MaPhieuDK;
            document.getElementById('hoten').innerHTML = data.HoTen;
            $('#sodt').append("<a href='tel:"+data.SoDT+"'>" + data.SoDT + "</a>");
            $('#email').append("<a href='mailto:"+data.Email+"'>" + data.Email + "</a>");
            document.getElementById('diachi').innerHTML = data.DiaChi;

            dangky_dialog.dialog("open");
        }
    });
});

//Nút đóng dialog
$(".close_dangky").button().on("click", function() {
    document.getElementById('maphieudk').innerHTML = null;
    document.getElementById('hoten').innerHTML = null;
    document.getElementById('sodt').innerHTML = null;
    document.getElementById('email').innerHTML = null;
    document.getElementById('diachi').innerHTML = null;

    dangky_dialog.dialog("close");
});

/************************MỞ LỚP************************/


/************************NHẬN DẠY************************/
$('#table_nhanday').DataTable( {
    bAutoWidth: true,
    "aoColumns": [
        null, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }, { "bSortable": false }
    ],
    "aaSorting": [],

    select: {
        style: 'single'
    }
});

/************************NHẬN DẠY************************/