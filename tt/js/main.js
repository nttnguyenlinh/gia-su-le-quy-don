var $has_post = false;
function showAlert(msg, callback) {
    $('#page-loader').fadeOut();
    swal({
        title: "",
        text: msg,
        type: "error",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: 'Đóng',
        html: true
    },
    function(){
        if (callback) callback();
    });
}

function showSuccess(msg, callback) {
    $('#page-loader').fadeOut();
    swal({
        title: "",
        text: msg,
        type: "success",
        showCancelButton: false,
        confirmButtonText: "OK",
        html: true
    },
    function(){
        if (callback) callback();
    });
}

function showComfirm(msg, callback) {
    swal({
        title: "",
        text: msg,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Hủy',
        confirmButtonText: 'Xác nhận',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        html: true
    },
    function(){
        console.log(callback);
        if (callback) {
            window[callback]();
        }
    });
}

function fb_share(title, link, img, description) {
    FB.ui( {
        method: 'feed',
        name: title,
        link: link,
        picture: img,
        hashtag: '#LangMai',
        // description: description,
        caption: '#LangMai'
    }, function(response){
        console.log(response);
        if (response) {

        }
    });
}
function goToByScroll(id){
      // Remove "link" from the ID
    id = id.replace("link", "");
      // Scroll
    $('html,body').animate({scrollTop: $("#"+id).offset().top}, 'slow');
}

function chonGiaSu(id) {
    if ( ! $has_post ){
        $has_post = 1;
        $('#page-loader').fadeIn();
        $.post(root + 'chon-gia-su.html', {giasu_id: id}, function(res){
            $has_post = 0;
            if (res && res.st == '1') {
                $('#page-loader').fadeOut();
                window.location.href = root + 'gia-su-da-chon.html';
                return true;
            }
            if (res && res.st == '0'){
                $('#page-loader').fadeOut();
                if (typeof res.errors === 'string' ) {
                    showAlert(res.errors);
                } else {
                    var msg = $.map(res.errors, function(text, index){
                        return '<li>' + text + '</li>';
                    });
                    showAlert(msg.join(''));
                }
                $has_post = false;

                if (res.redirect) {
                    setTimeout(function(){
                        window.location.href = res.redirect ;
                    }, 1500);
                }
                return false;
            }
        },'json');
    }
}
function boChonGiaSu(id, element) {
    var $this = $(element); 
    if ( ! $has_post ){
        $has_post = 1;
        $('#page-loader').fadeIn();
        $.post(root + 'bo-chon-gia-su.html', {giasu_id: id}, function(res){
            $has_post = 0;
            if (res && res.st == '1') {
                $('#page-loader').fadeOut();
                // window.location.href = root + 'gia-su-da-chon.html';
                $this.parent().parent().remove();
                return true;
            }
            if (res && res.st == '0'){
                $('#page-loader').fadeOut();
                if (typeof res.errors === 'string' ) {
                    showAlert(res.errors);
                } else {
                    var msg = $.map(res.errors, function(text, index){
                        return '<li>' + text + '</li>';
                    });
                    showAlert(msg.join(''));
                }
                $has_post = false;

                if (res.redirect) {
                    setTimeout(function(){
                        window.location.href = res.redirect ;
                    }, 1500);
                }
                return false;
            }
        },'json');
    }
}
$(document).ready(function(){
    $(document).on('click', '.js-scroll-to', function(){
        var div = $(this).attr('href');
        div = div.replace('#', '');
        goToByScroll(div);
    });

	if ($('#tinhthanh').length > 0) {
		$('#tinhthanh').on('change', function(){
			var province_id = $(this).val();
			var district_id = $('#quanhuyen').data('current');
			if (province_id != '' && province_id != 0) {
				$.post(root + 'home/change_province', {province_id: province_id, district_id: district_id}, function(res){
					$('#quanhuyen').html(res);
				});
			}
		});

		$('#tinhthanh').trigger('change');
	}

    if ($('#form__login').length > 0){
        $("#login__phone").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $('#form__login').on('submit', function(){
            if ( ! $has_post ){
                $has_post = 1;
                $('#page-loader').fadeIn();
                $.post(root + 'dang-nhap.html', $('#form__login').serialize(), function(res){
                    if (res && res.st == '1') {
                        $('#page-loader').fadeOut();
                        window.location.href = root + 'tai-khoan.html';
                        return true;
                    }
                    if (res && res.st == '0'){
                        $('#page-loader').fadeOut();
                        if (typeof res.errors === 'string' ) {
                            showAlert(res.errors);
                        } else {
                            var msg = $.map(res.errors, function(text, index){
                                return '<li>' + text + '</li>';
                            });
                            showAlert(msg.join(''));
                        }
                        $has_post = false;

                        if (res.redirect) {
                            setTimeout(function(){
                                window.location.href = res.redirect ;
                            },1500);
                        }
                        return false;
                    }
                },'json');
            }
            return false;
        });
    }
	
	if ($('.captcha-image').length > 0){
		$(".captcha-image").load(root + 'captcha/create');
		$("a.refresh").click(function() {
	        jQuery.ajax({
	            type: "POST",
	            url: root + 'captcha/create',
	            success: function(res) {
	            	if (res)
	                {
	                    $(".captcha-image").html(res);
	                }
	            }
	        });
	    });
	}

    if ($('#form__dk_tim_giasu').length > 0){
        $('#form__dk_tim_giasu').on('submit', function(){
            if ( ! $has_post ){
                $has_post = 1;
                $('#page-loader').fadeIn();
                $.post($('#form__dk_tim_giasu').attr('action'),$('#form__dk_tim_giasu').serialize(), function(res){
                    $has_post = 0;
                    if (res && res.st == '1') {
                        $('#page-loader').fadeOut();
                        showSuccess('Đăng ký thành công. Trung tâm Gia sư Đất Việt sẽ sớm gọi điện cho bạn khi được thông tin. Cám ơn!');
                        $("a.refresh").trigger('click');
                        return true;
                    }
                    if (res && res.st == '0'){
                        $('#page-loader').fadeOut();
                        // goToByScroll('form__dk_tim_giasu');
                        if (typeof res.errors === 'string' ) {
                            showAlert(res.errors);
                        } else {
                            var msg = $.map(res.errors, function(text, index){
                                return '<li>' + text + '</li>';
                            });
                            showAlert(msg.join(''));
                        }
                        $("a.refresh").trigger('click');
                        return false;
                    }
                },'json');
            }
            return false;
        });
    }

	if ($('#form__dk_lam_giasu').length > 0){
        $('#province_id').on('change', function(){
            var province_id = $(this).val();
            var district_id = $('#khuvuc_wrap').data('current');
            if (province_id != '' && province_id != 0) {
                $.post(root + 'home/change_user_province', {province_id: province_id, district_id: district_id}, function(res){
                    $('#khuvuc_wrap').html(res);
                });
            }

        });
		$('#form__dk_lam_giasu').on('submit', function(){
            formData = new FormData(document.getElementById('form__dk_lam_giasu'));
            formData.append('attachment', $('#attachment')[0].files[0]);
            formData.append('file_bangcap', $('#file_bangcap')[0].files[0]);
            if ( ! $has_post ){
                $has_post = 1;
                $('#page-loader').fadeIn();
                $.ajax({
                    method: 'POST',
                    url: $('#form__dk_lam_giasu').attr('action'),
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(res){
                        $has_post = false;
                        if (res && res.st == '1') {
                            $('#page-loader').fadeOut();
                            showSuccess('Bạn đã đăng ký gia sư thành công vui lòng chọn lớp để dạy.');
                            $("a.refresh").trigger('click');
                            
                            setTimeout(function(){
                                window.location.href = root + 'lop-day-can-gia-su.html';
                            }, 1500);

                            return true;
                        }
                        if (res && res.st == '0') {
                            $('#page-loader').fadeOut();
                            var message = '';
                            if (typeof res.errors === 'string') {
                                // showAlert(res.errors);
                                message = res.errors;
                            } else {
                                var msg = $.map(res.errors, function(text, index){
                                    return '<li>' + text + '</li>';
                                });
                                // showAlert(msg.join(''));
                                message = msg.join('');
                            }
                            $("a.refresh").trigger('click');
                            // goToByScroll('form__dk_lam_giasu');

                            if (res.redirect) {
                                swal({
                                  title: "",
                                  text: message,
                                  type: "warning",
                                  html: true,
                                  showCancelButton: true,
                                  confirmButtonClass: "btn-danger",
                                  cancelButtonText: "OK",
                                  closeOnCancel: true
                                },
                                function(isConfirm) {
                                  if (isConfirm) {

                                  } else {
                                    window.location.href = res.redirect;
                                  }
                                });
                            } else {
                                showAlert(message);
                            }
                            return false;
                        }
                    }
                });
            }
            return false;
		});
	}

    if ($('#form__update_profile').length > 0) {
        $('#province_id').on('change', function(){
            var province_id = $(this).val();
            var district_id = $('#khuvuc_wrap').data('current');
            if (province_id != '' && province_id != 0) {
                $.post(root + 'home/change_user_province', {province_id: province_id, district_id: district_id}, function(res){
                    $('#khuvuc_wrap').html(res);
                });
            }

        });
        
        $('#form__update_profile').on('submit', function(){
            var formData = new FormData(document.getElementById('form__update_profile'));
            formData.append('attachment', $('#attachment')[0].files[0]);
            formData.append('file_bangcap', $('#file_bangcap')[0].files[0]);
            // console.log(formData); return;
            if ( ! $has_post ){
                $has_post = 1;
                $('#page-loader').fadeIn();
                $.ajax({
                    method: 'POST',
                    url: $('#form__update_profile').attr('action'),
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(res){
                        $has_post = false;
                        if (res && res.st == '1') {
                            $('#page-loader').fadeOut();
                            showSuccess('Cập nhật thông tin thành công!');
                            // $("a.refresh").trigger('click');
                            return true;
                        }
                        if (res && res.st == '0'){
                            $('#page-loader').fadeOut();
                            
                            if (typeof res.errors === 'string' ) {
                                showAlert(res.errors);
                            } else {
                                var msg = $.map(res.errors, function(text, index){
                                    return '<li>' + text + '</li>';
                                });
                                showAlert(msg.join(''));
                            }
                            $("a.refresh").trigger('click');
                            // goToByScroll('form__dk_lam_giasu');
                            return false;
                        }
                    }
                });
            }
            return false;
        });
    }

    if ( $('#form__changepassword').length > 0) {
        $('#form__changepassword').on('submit', function(){
            formData = new FormData(document.getElementById('form__changepassword'));
            if ( ! $has_post ){
                $has_post = 1;
                $('#page-loader').fadeIn();
                $.ajax({
                    method: 'POST',
                    url: $('#form__changepassword').attr('action'),
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(res){
                        $has_post = false;
                        if (res && res.st == '1') {
                            $('#page-loader').fadeOut();
                            showSuccess('Đổi mật khẩu thành công!');
                            // $("a.refresh").trigger('click');
                            setTimeout(function(){ window.location.href = root + 'tai-khoan.html'; }, 1500);
                            return true;
                        }
                        if (res && res.st == '0'){
                            $('#page-loader').fadeOut();
                            
                            if (typeof res.errors === 'string' ) {
                                showAlert(res.errors);
                            } else {
                                var msg = $.map(res.errors, function(text, index){
                                    return '<li>' + text + '</li>';
                                });
                                showAlert(msg.join(''));
                            }
                            return false;
                        }
                    }
                });
            }
            return false;
        });
    }

    if ($('#form__dk__nhanlop').length > 0){
        $('#form__dk__nhanlop').on('submit', function(){
            if ( ! $has_post ){
                $has_post = 1;
                $('#page-loader').fadeIn();
                $.post($('#form__dk__nhanlop').attr('action'),$('#form__dk__nhanlop').serialize(), function(res){
                    $has_post = false;
                    if (res && res.st == '1') {
                        $('#page-loader').fadeOut();
                        showSuccess(res.msg);
                        $("a.refresh").trigger('click');

                        setTimeout(function(){ 
                            window.location.href = root + 'tai-khoan.html';
                        }, 1500);
                        
                        return true;
                    }
                    if (res && res.st == '0'){
                        $('#page-loader').fadeOut();
                        if (typeof res.errors === 'string' ) {
                            showAlert(res.errors);
                        } else {
                            var msg = $.map(res.errors, function(text, index){
                                return '<li>' + text + '</li>';
                            });
                            showAlert(msg.join(''));
                        }

                        if (res && res.redirect) {
                            setTimeout(function(){ window.location.href = res.redirect; }, 1500);
                        }
                        return false;
                    }
                },'json');
            }
            return false;
        });
    }

    if ($('#suco__hinhthuc').length > 0) {
        $(document).on('change', '#suco__hinhthuc', function(){
            var val = $(this).val();
            if (val == 1) {
                $('#form__noidung__wrap').fadeOut();
            } else {
                $('#form__noidung__wrap').fadeIn();
            }
        });
    }

    if ($('#form__thongbao__suco').length > 0){
        $('#form__thongbao__suco').on('submit', function(){
            if ( ! $has_post ){
                $has_post = 1;
                $('#page-loader').fadeIn();
                $.post($('#form__thongbao__suco').attr('action'),$('#form__thongbao__suco').serialize(), function(res){
                    $has_post = false;
                    if (res && res.st == '1') {
                        $('#page-loader').fadeOut();
                        showSuccess(res.msg);
                        setTimeout(function(){ window.location.reload(); }, 1000);
                        return true;
                    }
                    if (res && res.st == '0'){
                        $('#page-loader').fadeOut();
                        if (typeof res.errors === 'string' ) {
                            showAlert(res.errors);
                        } else {
                            var msg = $.map(res.errors, function(text, index){
                                return '<li>' + text + '</li>';
                            });
                            showAlert(msg.join(''));
                        }

                        if (res && res.redirect) {
                            setTimeout(function(){ window.location.href = res.redirect; }, 1500);
                        }
                        return false;
                    }
                },'json');
            }
            return false;
        });
    }
})