<?php
    if(isset($_SERVER['HTTPS']))
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    else
        $protocol = 'http';

    $geturl = $protocol . "://" . $_SERVER['HTTP_HOST'];
?>

<!DOCTYPE html>
    <html lang="vi">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Trung Tâm Gia Sư Lê Quý Đôn Dạy Kèm Uy Tín Chất Lượng TPHCM</title>
        <meta name="abstract" content="Trung Tâm Gia Sư Lê Quý Đôn Dạy Kèm Uy Tín Chất Lượng TPHCM">
        <meta name="description" content="Trung Tâm Gia Sư Lê Quý Đôn Dạy Kèm Uy Tín Chất Lượng TPHCM"/>
        <meta name="keywords" content="gia sư, gia sư lê quý đôn, gia sư tphcm, gia su le quy don, gia su tphcm"/>

        <meta name="author" content="Nguyễn Linh, ntt.nguyenlinh@gmail.com">
        <meta name="copyright" content="Copyright 2018 - Gia Sư Lê Quý Đôn">

        <link rel="icon" href="{{URL::asset('favicon.ico')}}" type="image/x-icon">
        <link rel="canonical" href=""/>
</head>

<body>
    <img src="<?php echo $geturl; ?>/images/dribbble-403.png" width="100%" height="100%">
</body>
</html>