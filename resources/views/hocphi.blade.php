@extends('master')
@section('title', 'Học Phí Tham Khảo - ')
@section('canonical', Route('hocphithamkhao'))
@section('content')
    <div class="col_c">
        <div class="newClass">
            <div class="content_choose_class">
                <h1 class="class_ttl" style="font-size: 14pt; font-weight: bold;">HỌC PHÍ THAM KHẢO</h1><br>
                <div class="content-detail" style="margin-top: 10px;">
                    <h1 style="font-size: 13pt; font-weight: bold; color: #0000ff; text-align: center; margin-bottom: 20px;">
                        CAM KẾT GIA SƯ DẠY TẬN TÂM
                    </h1>
                    <p style="font-size: 12pt;">
                        Gia sư sẽ dạy kèm <span style="font-weight: bold; color: #ff0000;">2 BUỔI MIỄN PHÍ</span> để quý phụ huynh và học viên đánh giá được chất lượng của gia sư.
                    </p>

                    <table class='table1'>
                        <tbody>
                            <tr>
                                <th class='col1' rowspan='2'>KHỐI LỚP</th>
                                <th colspan='2'>2 buổi/tuần</th>
                            </tr>
                            <tr>
                                <th>Sinh viên</th>
                                <th>Giáo viên</th>
                            </tr>
                            @foreach($hocphi as $row)
                                @if($row['SoBuoi'] == 2)
                                    <tr>
                                        <td class='col1'>
                                            {{$row->KhoiLop}}
                                        </td>
                                        <td>
                                            {{$row->SinhVien}}
                                        </td>
                                        <td>
                                            {{$row->GiaoVien}}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>

                    <table class='table1'>
                        <tbody>
                        <tr>
                            <th class='col1' rowspan='2'>KHỐI LỚP</th>
                            <th colspan='2'>3 buổi/tuần</th>
                        </tr>
                        <tr>
                            <th>Sinh viên</th>
                            <th>Giáo viên</th>
                        </tr>
                        @foreach($hocphi as $row)
                            @if($row['SoBuoi'] == 3)
                                <tr>
                                    <td class='col1'>
                                        {{$row->KhoiLop}}
                                    </td>
                                    <td>
                                        {{$row->SinhVien}}
                                    </td>
                                    <td>
                                        {{$row->GiaoVien}}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>

                    <table class='table2'>
                        <tbody>
                        <tr>
                            <th class='col1' rowspan='2'>KHỐI LỚP</th>
                            <th colspan='2'>4 buổi/tuần</th>
                        </tr>
                        <tr>
                            <th>Sinh viên</th>
                            <th>Giáo viên</th>
                        </tr>
                        @foreach($hocphi as $row)
                            @if($row['SoBuoi'] == 4)
                                <tr>
                                    <td class='col1'>
                                        {{$row->KhoiLop}}
                                    </td>
                                    <td>
                                        {{$row->SinhVien}}
                                    </td>
                                    <td>
                                        {{$row->GiaoVien}}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>

                    <table class='table2'>
                        <tbody>
                        <tr>
                            <th class='col1' rowspan='2'>KHỐI LỚP</th>
                            <th colspan='2'>5 buổi/tuần</th>
                        </tr>
                        <tr>
                            <th>Sinh viên</th>
                            <th>Giáo viên</th>
                        </tr>
                        @foreach($hocphi as $row)
                            @if($row['SoBuoi'] == 5)
                                <tr>
                                    <td class='col1'>
                                        {{$row->KhoiLop}}
                                    </td>
                                    <td>
                                        {{$row->SinhVien}}
                                    </td>
                                    <td>
                                        {{$row->GiaoVien}}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>

                    <p style="color: #ff0000; font-weight: bold; margin-top: 20px; font-size: 12pt; font-style: italic;">Lưu ý:</p>
                    <ul class="tgs_l">
                        <li style="margin-left: 20px;">Thời gian dạy của sinh viên 1 buổi là 120 phút</li>
                        <li style="margin-left: 20px;">Thời gian dạy của giáo viên 1 buổi là 90 phút</li>
                        <li style="margin-left: 20px;">Học phí trên áp dụng cho sinh viên và giáo viên có bằng cử nhân hoặc giáo viên có bằng sư phạm.</li>
                        <li style="margin-left: 20px;">Đối với thạc sỹ, giáo viên thâm niên, giáo viên đang dạy tại trường có kinh nghiệm hoặc có kinh nghiệm dạy kèm thì học phí sẽ được tính theo tiết (1 tiết = 45 phút) như sau:</li>
                    </ul>
                    <ul class="tgs_l">
                        <li style="margin-left: 50px; color:purple;">Cấp 1: 60.000đ - 80.000đ / 1 tiết = 120.000đ - 160.000đ / 1 buổi dạy</li>
                        <li style="margin-left: 50px; color:purple;">Cấp 2: 80.000đ - 100.000đ / 1 tiết = 160.000đ - 200.000đ / 1 buổi dạy</li>
                        <li style="margin-left: 50px; color:purple;">Cấp 3: 100.000đ- 120.000đ / 1 tiết = 200.000đ - 240.000đ / 1 buổi dạy</li>
                    </ul>

                    <p style="margin-top: 10px; font-size: 10pt; font-style: italic;">Mức học phí trên là tham khảo, mức học phí sẽ tăng hoặc giảm theo từng yêu cầu: Số môn học, số người học và các yêu cầu thêm khác, mời quý phụ huynh và học viên vui lòng liên hệ để biết mức học phí và được tư vấn tìm gia sư giỏi và chất lượng.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
