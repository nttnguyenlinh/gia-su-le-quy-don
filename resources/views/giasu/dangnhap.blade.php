<div class="box">
    @if(Session::has('sdt'))
        <?php $getinfo = \App\gia_su::where('SoDT', 'like', Session::get('sdt'))->first(); ?>
        <h3 class="bh_g text-uppercase">{{$getinfo->HoTen}}</h3>
            <a href="{{Route('giasu.index')}}">
                @if(!empty($getinfo->AnhThe))
                    <img src="{{$getinfo->AnhThe}}?v=4.0.1" alt="{{$getinfo->HoTen}}" width="150px" height="200px"/>
                @else
                    <img src="https://i.imgur.com/zP22ia8.png?v=4.0.1" alt="{{$getinfo->HoTen}}" width="150px" height="200px"/>
                @endif

            </a>
            <p class="more_g2"><a href="{{Route('giasu.thoat')}}" style="background: #8CC141; color:white; text-decoration: none;">THOÁT</a></p>
    @else
        <h3 class="bh_g">GIA SƯ ĐĂNG NHẬP</h3>
        <ul class="l_list1">
            <li class="login clearfix">
                <form action="{{Route('giasu.postlogin')}}" method="post">
                    {{csrf_field()}}
                    <input type="text" id="sdt" name="sdt" value="" placeholder="Nhập số điện thoại" maxlength="10" onkeypress="return event.charCode = 13 || event.charCode >= 48 && event.charCode <= 57" required/>
                    <input type="submit" value="ĐĂNG NHẬP" class="btn_s3" style="width: 100px !important; background:#8CC141!important; color: white; margin: 15px 20px;" />
                </form>
            </li>
        </ul>
    @endif
</div>