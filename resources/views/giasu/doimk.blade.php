@extends('master')
@section('title', 'Đổi Mật Khẩu - ')
@section('canonical', Route('giasu.doimk'))
@section('content')
    <div class="col_c">
        <h1 class="class_ttl" style="font-size: 14pt; font-weight: bold;">ĐỔI MẬT KHẨU</h1>
        @if(count($errors)>0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $err)
                    <li>{{$err}}</li>
                @endforeach
            </div>
        @endif

        <form action="{{Route('giasu.postdoimk')}}" id="form_doi_mk" method="post">
            {{csrf_field()}}
            <div class="formOut clearfix fm NiceIt">
                <div class="formOut_line clearfix">
                    <input type="hidden" name="id" class="form-control input-sm" value="{{$getinfo->MaGiaSu}}">
                    <p class="formOut_lab">Mật khẩu hiện tại (<span>*</span>)</p>
                    <div class="formOut_field clearfix">
                        <span class="w250">
                            <input value="" type="password" id="old_password" name="old_password" class="in_405" required>
                        </span>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Mật khẩu mới (<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="password" id="new_password" name="new_password" value="" class="in_405" required/>
                    </div>
                </div>
                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Xác nhận (<span>*</span>)</p>
                    <div class="formOut_field">
                        <span>
                            <input type="password" id="password_confirm" name="password_confirm" value="" class="in_405" required/>
                        </span>
                    </div>
                </div>

                <p class="dangky_btn clearfix">
                    <input type="submit" value="Cập nhật" class="btn_s3">
                </p>
            </div>
        </form>
    </div>

    <script src="{{URL::asset('js/jquery.validate.min.js?v=4.0.1')}}"></script>
    <script>
        $("#form_doi_mk").validate({
            rules: {
                old_password: {
                    required: true
                },
                new_password: {
                    required: true,
                    pass_regex: true
                },
                password_confirm: {
                    required: true,
                    equalTo: "#new_password"
                },
            },

            messages: {
                old_password: {
                    required: 'Vui lòng nhập mật khẩu hiện tại!'
                },

                new_password: {
                    required: 'Vui lòng nhập mật khẩu mới!',
                    pass_regex: 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, tối thiểu 6 kí tự!'
                },
                password_confirm: {
                    required: 'Vui lòng nhập lại mật khẩu mới!',
                    equalTo: 'Mật khẩu không trùng khớp!'
                },
            }
        });

        //Kiểm tra mật khẩu
        $.validator.addMethod("pass_regex", function (value) {
            return /(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/.test(value);
        });
    </script>
@endsection