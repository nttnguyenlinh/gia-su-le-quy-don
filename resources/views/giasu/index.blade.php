@extends('master')
@section('title', "Gia Sư $getinfo->HoTen - ")
@section('canonical', Route('giasu.index'))
@section('content')
    <div class="col_c">
        @if(count($errors)>0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $err)
                    <li>{{$err}}</li>
                @endforeach
            </div>
        @endif
        <div class="dsgsdk" style="margin-bottom: 10px;">
            <h1>DANH SÁCH LỚP NHẬN DẠY</h1>
        </div>
        <table class="dsgsdk_table" style="margin-bottom: 10px;">
            <tbody>
                <tr>
                    <th>MS.Lớp</th>
                    <th>Lớp/Môn</th>
                    <th>Lương</th>
                    <th>Mức phí</th>
                    <th>Trạng thái</th>
                </tr>

                <?php
                    $getPhieuNhan = \App\phieu_nhan_lop::where('MaGiaSu', $getinfo->MaGiaSu)->orderBy('created_at', 'desc')->get();
                    $getPhieuMo = \App\phieu_mo_lop::all();
                    $num = count($getPhieuNhan);
                ?>

                @foreach($getPhieuNhan as $row)
                    @foreach($getPhieuMo as $phieu)
                        @if($row->MaPhieuMo == $phieu->MaPhieuMo)
                            <?php
                                $status = $row->Status;
                                $class = $label = '';
                                switch ($status)
                                {
                                    case  1:   $class='color3'; $label = 'Xem xét'; break;
                                    case  2:   $class='color2'; $label = 'Đủ điều kiện'; break;
                                    case  3:   $class='color1'; $label = 'Đã nhận'; break;
                                    case  4:   $class='color4'; $label = 'Không đạt'; break;
                                    default:   $class='color5'; $label = 'Chờ duyệt';
                                }
                            ?>
                            <tr class="{{$class}}">
                                <td>
                                    <a target="_blank" href="{{Route('getLop', ['slug' =>$phieu->slug])}}" title="Xem chi tiết lớp" style="font-weight:bold; color: #679312; text-decoration: none; background:white; border:#679312 solid 2px; padding: 2px;">{{$row->MaPhieuMo}}</a>
                                </td>
                                <td class="col_2">
                                    {{$phieu->MonDay}}
                                    <span>Lớp: {{$phieu->LopDay}}</span>
                                </td>

                                <td>{{$phieu->Luong}}<sup>đ</sup></td>

                                <td>
                                    {{$phieu->LePhi}}% = {{(float)$phieu->LePhi * (float)$phieu->Luong / 100}} <sup>triệu</sup>
                                </td>

                                <td>{{$label}}</td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
            </tbody>
        </table>

        <div class="dsgsdk">
            <h4>THÔNG TIN CÁ NHÂN</h4>
        </div>

        <form action="{{Route('giasu.post_update')}}" enctype="multipart/form-data" id="form_update_profile" method="post">
            {{csrf_field()}}
            <div class="formOut clearfix fm NiceIt">
                <input type="hidden" name="id" value="{{$getinfo->MaGiaSu}}"/>
                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Tỉnh/Thành dạy (<span>*</span>)</p>
                    <div class="formOut_field w100">
                        <select name="tinhthanh" id="tinhthanh" required>
                            <option value="Hồ Chí Minh">Hồ Chí Minh</option>
                        </select>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Họ tên (<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="hoten" name="hoten" value="{{$getinfo->HoTen}}"  placeholder="Họ tên của bạn" class="in_405" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Ngày sinh (<span>*</span>)</p>
                    <div class="formOut_field clearfix">
                        <select name='ngay' id='ngay' style="margin-right: 5px;" required>
                            <option value=''>Ngày</option>
                            <?php
                                for($i = 1; $i <= 31; $i++)
                                    if(substr($getinfo->NgaySinh, 0, 2) == $i)
                                        if($i < 10)
                                            echo "<option value='0$i' selected='selected'>0$i</option>";
                                        else
                                            echo "<option value='$i' selected='selected'>$i</option>";
                                    else
                                        if($i < 10)
                                            echo "<option value='0$i'>0$i</option>";
                                        else
                                            echo "<option value='$i'>$i</option>";
                            ?>
                        </select>

                        <select name='thang' id='thang' style="margin-right: 5px;" required>
                            <option value=''>Tháng</option>
                            <?php
                                for($i = 1; $i <= 12; $i++)
                                    if(substr($getinfo->NgaySinh, 3, 2) == $i)
                                        if($i < 10)
                                            echo "<option value='0$i' selected='selected'>0$i</option>";
                                        else
                                            echo "<option value='$i' selected='selected'>$i</option>";
                                    else
                                        if($i < 10)
                                            echo "<option value='0$i'>0$i</option>";
                                        else
                                            echo "<option value='$i'>$i</option>";
                            ?>
                        </select>

                        <select name='nam' id='nam' style="margin-right: 5px;" required>
                            <option value=''>Năm</option>
                            <?php
                                for($i = (date('Y') - 18); $i >= 1950; $i--)
                                    if(substr($getinfo->NgaySinh, 6, 4) == $i)
                                        echo "<option value='$i' selected='selected'>$i</option>";
                                    else
                                        echo "<option value='$i'>$i</option>";
                            ?>
                        </select>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Nguyên quán (<span>*</span>)</p>
                    <div class="formOut_field w100">
                        <select name='nguyenquan' id='nguyenquan' style="margin-right: 5px;" required>
                            <option value=''>Tỉnh/thành trên CMND</option>
                            <?php
                                $tinhthanh = array("Hồ Chí Minh", "Hà Nội", "An Giang", "Bà Rịa - Vũng Tàu", "Bắc Cạn",
                                    "Bắc Giang", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước",
                                    "Bình Thuận", "Cà Mau", "Cần Thơ", "Cao Bằng", "Đà Nẵng", "Đắc Lắk", "Đắk Nông", "Điện Biên",
                                    "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương", "Hải Phòng",
                                    "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng",
                                    "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Phú Yên",
                                    "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình",
                                    "Thái Nguyên", "Thanh Hóa", "Thừa Thiên - Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc",
                                    "Yên Bái");
                                foreach($tinhthanh as $row)
                                    if($getinfo->NguyenQuan == $row)
                                        echo "<option value='$row' selected='selected'>$row</option>";
                                    else
                                        echo "<option value='$row'>$row</option>";
                            ?>
                        </select>

                        <select name='giongnoi' id='giongnoi' style="margin-right: 5px;" required>
                            <option value=''>Chọn giọng nói</option>
                            <?php
                                $giongnoi = array("Miền Bắc", "Miền Trung", "Miền Nam");
                                foreach($giongnoi as $row)
                                    if($getinfo->GiongNoi == $row)
                                        echo"<option value='$row' selected='selected'>$row</option>";
                                    else
                                        echo"<option value='$row'>$row</option>";
                            ?>
                        </select>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Địa chỉ hiện tại (<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="diachi" name="diachi" value="{{$getinfo->DiaChi}}" class="in_405" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">CMND (<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="cmnd" name="cmnd" value="{{$getinfo->CMND}}" class="in_405" maxlength="12" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Email (<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="email" id="email" name="email" value="{{$getinfo->Email}}" class="in_405" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Ảnh của bạn</p>
                    <div class="formOut_field">
                        <input type="file" name="anhthe" id="anhthe" onchange="validateFileType()" accept="gif|png|jpg|jpeg"/>
                        <br><br>
                        @if($getinfo->AnhThe != null)
                            <img src="{{$getinfo->AnhThe}}?v=4.0.1" alt="{{$getinfo->HoTen}}" width="132" height="180" title="{{$getinfo->HoTen}}" style="padding-left: 5px;"/>
                        @else
                            <img src="https://i.imgur.com/zP22ia8.png?v=4.0.1" alt="{{$getinfo->HoTen}}" width="132" height="180" title="{{$getinfo->HoTen}}" style="padding-left: 5px;"/>
                        @endif
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Trường đào tạo (<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="truonghoc" name="truonghoc" value="{{$getinfo->TruongDaoTao}}" placeholder="ví dụ: ĐH sư phạm" class="in_405" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Ngành học (<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="chuyennganh" name="chuyennganh" value="{{$getinfo->NganhHoc}}" placeholder="ví dụ: sư phạm vật lý" class="in_405" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Đơn vị công tác</p>
                    <div class="formOut_field">
                        <input type="text" id="congtac" name="congtac" value="{{$getinfo->CongTac}}" placeholder="Dành cho giáo viên, ví dụ: trường tiểu học Đinh Tiên Hoàng" class="in_405"/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Năm tốt nghiệp (<span>*</span>)</p>
                    <div class="formOut_field clearfix">
                    <span class="w120">
                        <select name="nam_totnghiep" id="nam_totnghiep" required>
                            <option value="">Năm tốt nghiệp</option>
                            <?php
                                for($i = (date('Y') + 20); $i >= 1960; $i--)
                                    if($getinfo->NamTN == $i)
                                        echo "<option value='$i' selected='selected'>$i</option>";
                                    else
                                        echo "<option value='$i'>$i</option>";
                            ?>
                        </select>
                    </span>
                        <br><br>

                        <p><i style="color: red; background: yellow;">Ví dụ: Niên khóa ĐH/CĐ của bạn là 2015 - 2019. Bạn chọn năm tốt nghiệp là 2019.</i></p>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Hiện là (<span>*</span>)</p>
                    <div class="formOut_field clearfix">
                        <select name="trinhdo" id="trinhdo" style="margin-right: 5px;" required>
                            <option value="">Trình độ</option>
                            <?php
                            $trinhdo = array("Giáo viên", "Sinh Viên", "Cử Nhân", "Kỹ Sư", "Thạc Sỹ", "Tiến Sỹ",
                                "Giảng Viên", "Bằng Cấp Khác", "Sinh Viên Sư Phạm", "Cử Nhân Sư Phạm");
                            foreach($trinhdo as $row)
                                if($getinfo->TrinhDo == $row)
                                    echo "<option value='$row' selected='selected'>$row</option>";
                                else
                                    echo "<option value='$row'>$row</option>";
                            ?>
                        </select>

                        <select name="gioitinh" id="gioitinh" style="margin-right: 5px;" required>
                            <option value="">Giới tính</option>
                            <?php
                            $gioitinh = array("Nam", "Nữ");
                            foreach($gioitinh as $row)
                                if($getinfo->GioiTinh == $row)
                                    echo "<option value='$row' selected='selected'>$row</option>";
                                else
                                    echo "<option value='$row'>$row</option>";
                            ?>
                        </select>

                        <select name="loai_gv" id="loai_gv" style="margin-right: 5px;">
                            <option value="">Loại giáo viên</option>
                            <?php
                                $loai_gv = array("GV dạy tự do", "GV dạy tại trường", "GV dạy trung tâm");
                                foreach($loai_gv as $row)
                                    if($getinfo->LoaiGiaoVien == $row)
                                        echo "<option value='$row' selected='selected'>$row</option>";
                                    else
                                        echo "<option value='$row'>$row</option>";
                            ?>
                        </select>

                        <br><br>
                        <p style="color: red; background: yellow; font-style: italic;">
                            Lưu ý: <b>Chọn loại giáo viên</b> chỉ dành riêng cho giáo viên, giảng viên, nếu bạn không phải thì không cần chọn phần này.
                        </p>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Ưu điểm</p>
                    <div class="formOut_field">
                        <textarea id="uudiem" name="uudiem" placeholder="Nói thêm về bạn cũng như kinh nghiệm bạn tích luỹ được" class="area_100" style="resize: vertical; height: 70px;">{{$getinfo->UuDiem}}</textarea>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Môn dạy (<span>*</span>)</p>
                    <div class="formOut_field">
                        <ul class="check_l1 clearfix">
                            <?php
                                $monday_array = explode(",",$getinfo->MonDay);
                                foreach($getMonHoc as $mon)
                                {
                                    foreach($monday_array as $row)
                                        if($row == $mon->MaMon)
                                        {
                                            echo "<li><input type='checkbox' id='monday' name='monday[]' value='$mon->MaMon' checked='checked'/><label>$mon->TenMon</label></li>";
                                            continue 2;
                                        }
                                    echo "<li><input type='checkbox' id='monday' name='monday[]' value='$mon->MaMon'/><label>$mon->TenMon</label></li>";
                                }
                            ?>
                        </ul>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Lớp dạy (<span>*</span>)</p>
                    <div class="formOut_field">
                        <ul class="check_l2 clearfix">
                            <?php
                            $lopday_array = explode(",",$getinfo->LopDay);
                            foreach($getLopHoc as $lop)
                            {
                                foreach($lopday_array as $row)
                                    if($row == $lop->MaLop)
                                    {
                                        echo "<li><input type='checkbox' id='lopday' name='lopday[]' value='$lop->MaLop' checked='checked'/><label>$lop->TenLop</label></li>";
                                        continue 2;
                                    }
                                echo "<li><input type='checkbox' id='lopday' name='lopday[]' value='$lop->MaLop'/><label>$lop->TenLop</label></li>";
                            }
                            ?>
                        </ul>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Khu vực dạy (<span>*</span>)</p>
                    <div class="formOut_field">
                        <ul class="check_l3 clearfix">
                            <?php
                            $khuvuc_array = explode(",",$getinfo->KhuVuc);
                            foreach($getKhuVuc as $khuvuc)
                            {
                                foreach($khuvuc_array as $row)
                                    if($row == $khuvuc->MaKV)
                                    {
                                        echo "<li><input type='checkbox' id='khuvuc'  name='khuvuc[]' value='$khuvuc->MaKV' checked='checked'/><label>$khuvuc->TenKV</label></li>";
                                        continue 2;
                                    }
                                echo "<li><input type='checkbox' id='khuvuc' name='khuvuc[]' value='$khuvuc->MaKV'/><label>$khuvuc->TenKV</label></li>";
                            }
                            ?>
                        </ul>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab"></p>
                    <div class="formOut_field">
                        <p class="bg_txt bg_txt3">Bấm vào đây để <a target="_blank" href="{{Route('giasu.doimk')}}" title="Đổi mật khẩu">Đổi mật khẩu</a></p>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Nhập Mật khẩu (<span>*</span>)</p>
                    <div class="formOut_field clearfix">
                        <span class="w250">
                            <input value="" type="password" id="password" name="password" class="in_405" placeholder="Nhập mật khẩu hiện tại" required>
                        </span>
                        <br><br>
                        <p><i style="color: red; background: yellow;">Lưu ý: Nếu gia sư muốn thay đổi số điện thoại, hoặc quên mật khẩu cần cấp lại mật khẩu mới thì vui lòng liên hệ với trung tâm để được hỗ trợ.</i></p>
                    </div>
                </div>

                <p class="dangky_btn clearfix">
                    <input type="submit" id="update" name="update" value="Cập nhật" class="btn_s2" style="float: none;">
                </p>
                <p><i style="color: red; background: yellow;">Lưu ý: Điền đầy đủ thông tin đánh dấu (*) để được xét duyệt. Xin cảm ơn!</i></p>
            </div>
        </form>
    </div>
@endsection