@extends('master')
@section('title', "Gia Sư $getChiTiet->MonDay $getChiTiet->LopDay $getChiTiet->DiaChi - ")
@section('canonical', Route('getChiTietLop', ['slug' =>$getChiTiet->slug]))
@section('content')
    <div class="col_c">
        <div class="newClass">
            <h1 class="class_ttl" style="font-size: 14pt; font-weight: bold;">{{mb_strtoupper($getChiTiet->MonDay)}} {{mb_strtoupper($getChiTiet->LopDay)}}</h1>
            <div class="ldcgs_info clearfix">
                <div class="ldcgs_info_l">
                    <div class="clearfix">
                        <p style="margin-bottom: 10px; margin-top: 10px;">
                            Mã lớp: <span style="color: red; font-weight: bold;">{{$getChiTiet->MaPhieuMo}}</span>
                            @if($getChiTiet->Status == 1)
                                <span style="color: #fff; font-weight: bold;" class="label label-success">ĐÃ GIAO</span>
                            @else
                                <span style="color: #fff; font-weight: bold;" class="label label-danger">CHƯA GIAO</span>
                            @endif
                        </p>

                        <p style="margin-bottom: 10px;">Môn: <span style="color: green; font-weight: bold;">{{$getChiTiet->MonDay}}</span></p>
                        <p style="margin-bottom: 10px;">Lớp: <span style="color: green; font-weight: bold;">{{$getChiTiet->LopDay}}</span></p>
                        <p style="margin-bottom: 10px;">Địa chỉ:
                            <button style="font-weight:bold; border: #00BE67 solid 1px; padding: 2px; color: green;" data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!">
                                <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("$getChiTiet->DiaChi")}}','width=600,height=600'); return false;">
                                    {{$getChiTiet->DiaChi}}
                                </a>
                            </button>
                        </p>
                        <p style="margin-bottom: 10px;">Dạy: <span style="color: green;">{{$getChiTiet->SoBuoiDay}} buổi/tuần</span></p>
                        <p style="margin-bottom: 10px;">Giờ học: <span style="color: green;">{{$getChiTiet->ThoiGian}}</span></p>
                        <p style="margin-bottom: 10px;">Lương: <span style="color: green;">{{$getChiTiet->Luong}} <sup>đ</sup>/tháng</span></p>
                        <p style="margin-bottom: 10px;">Thông tin: <span style="color: green;">{{$getChiTiet->ThongTin}}</span></p>
                        <p style="margin-bottom: 10px;">Yêu cầu: <span style="color: green;">{{$getChiTiet->YeuCau}}</span></p>
                        <p style="margin-bottom: 10px;">Liên hệ: <span><a href="tel:0903651882" style="text-decoration: none; color: green; font-weight: bold;">0903.651.882</a></span></p>
                    </div>
                </div>
            </div>

            <div class="dsgsdk"> <h4>DANH SÁCH GIA SƯ NHẬN DẠY</h4></div>
            <table class="dsgsdk_table">
                <tr>
                    <th class="col_1" style="font-weight: bold;">STT</th>
                    <th class="col_2" style="font-weight: bold;">Gia sư</th>
                    <th class="col_3" style="font-weight: bold;">Ngày đăng ký</th>
                    <th class="col_5" style="font-weight: bold;">Trạng thái</th>
                </tr>

                <?php
                    $getPhieuNhan = \App\phieu_nhan_lop::where('MaPhieuMo', 'like', $getChiTiet->MaPhieuMo)->orderBy('created_at', 'desc')->get();
                    $num = count($getPhieuNhan);
                    $getGiaSu = \App\gia_su::all();
                    $getPhieuMo = \App\phieu_mo_lop::all();
                    $i = 1;

                    foreach($getPhieuNhan as $row)
                    {
                        while($num)
                        {
                            foreach($getGiaSu as $giasu)
                            {
                                if($row->MaGiaSu == $giasu->MaGiaSu)
                                {
                                    foreach($getPhieuMo as $phieumo)
                                    {
                                        if($row->MaPhieuMo == $phieumo->MaPhieuMo)
                                        {
                                            $status = $row->Status;
                                            $class = $label = '';
                                            switch ($status)
                                            {
                                                case  1:   $class='color3'; $label = 'Xem xét'; break;
                                                case  2:   $class='color2'; $label = 'Đủ điều kiện'; break;
                                                case  3:   $class='color1'; $label = 'Đã nhận'; break;
                                                case  4:   $class='color4'; $label = 'Không đạt'; break;
                                                default:   $class='color5'; $label = 'Chờ duyệt';
                                            }

                                            echo "<tr class='$class'>";
                                            echo "<td><span style='font-weight:bold; color: #679312; text-decoration: none; background:white; border:#679312 solid 1px; padding: 2px;'>$i</span></td>";
                                            echo "<td>$giasu->HoTen <span style='color: #679312; text-decoration: none; background:white; border:#679312 solid 1px; padding: 2px;'>$giasu->TrinhDo</span></td>";
                                            echo "<td>" .date_format(date_create($row->created_at), 'd-m-Y') . "</td>";
                                            echo "<td>$label</td>";
                                            echo "</tr>";
                                        }
                                    }
                                }
                            }
                            $i++;
                            continue 2;
                        }
                    }
                ?>
            </table>
            <p class="bg_txt">Cho phép tối đa 10 người đăng ký. Chỉ giao lớp cho 1 người đủ điều kiện nhận lớp và đóng lệ phí trước.</p>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $("#ngay").datepicker({
                dateFormat : "dd-mm-yy",
                minDate : 0
            });
        });
    </script>
@endsection