@extends('master')
@section('title', '')
@section('canonical', Route('home'))

@section('content')
    <div class="col_c">
        <div class="gioithieu clearfix">
            <div class="gt_l">
                <div class="owl-carousel2">
                    <div class="item"><img src="{{URL::asset('/images/img3.png')}}" alt="Gia Sư Lê Quý Đôn" title=""/></div>
                </div>
            </div>
            <div class="gt_r">
                <p><img src="{{URL::asset('/images/img_footer.png')}}" alt="Gia Sư Lê Quý Đôn" title="Gia Sư Lê Quý Đôn" width="100%"/></p>
                <p>Hãy đến với gia sư Lê Quý Đôn, để có được sự lựa chọn đúng đắn.
                    Với sứ mệnh mang lại niềm hạnh phúc cho quý phụ huynh trước sự tiến bộ của con em trong học tập.</p>
            </div>
        </div>
        <br>
        <div class="newClass">
            <h1 class="class_ttl" style="font-size: 14pt; font-weight: bold;">DANH SÁCH LỚP MỚI - CẬP NHẬT NGÀY {{date("d-m-Y")}}</h1>
            <div class="search clearfix">
                <form method="post" action="{{Route('searchLopDangMo')}}">
                    {{csrf_field()}}
                    <input type="text" name="tukhoa" value="" placeholder="Nhập mã lớp hoặc tên môn dạy" class="in_search" style="color:orange;">
                    <select name="tinhthanh" style="margin-right:4px; color:orange; border: orange 1px solid;">
                        <option value="1">Hồ Chí Minh</option>
                    </select>

                    <select name="khuvuc" style="margin-right:4px; color:orange;">
                        <option value="" disabled selected>Quận/huyện</option>
                        @foreach($getKhuVuc as $row)
                            <option value="{{$row->MaKV}}">{{$row->TenKV}}</option>
                        @endforeach
                    </select>

                    <select style="width:100%; color:orange; border: orange 1px solid;" name="lopday">
                        <option value="" disabled selected>Lớp dạy</option>
                        @foreach($getLopHoc as $row)
                            <option value="{{$row->MaLop}}">{{$row->TenLop}}</option>
                        @endforeach
                    </select>

                    <input type="submit" name="timkiem" value="Tìm kiếm" class="btn_s2" style="background:orange; border: orange 1px solid;"/>
                </form>
            </div>

            <div class="class_list clearfix">
                @foreach($getPhieuMoLop as $row)
                    <div class="item_c">
                        <div class="c_ttl">LỚP CHƯA GIAO
                            <p class="c_ms">MS<br>{{$row->MaPhieuMo}}</p>
                        </div>
                        <div class="c_content" style="height: 250px;">
                            <p>Môn: <span style="color: green; font-weight: bold;">{{$row->MonDay}}</span></p>
                            <p>Lớp: <span style="color: green; font-weight: bold;">{{$row->LopDay}}</span></p>
                            <p>Địa chỉ:
                                <button style="font-weight:bold; border: #00BE67 solid 1px; padding: 2px; color: green;" data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!">
                                    <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("$row->DiaChi")}}','width=600,height=600'); return false;">
                                        {{$row->DiaChi}}
                                    </a>
                                </button>
                            </p>
                            <p>Dạy: <span style="color: green;">{{$row->SoBuoiDay}} buổi/tuần</span></p>
                            <p>Giờ học: <span style="color: green;">{{$row->ThoiGian}}</span></p>
                            <p>Lương: <span style="color: green;">{{$row->Luong}} <sup>đ</sup>/tháng</span></p>
                            <p>Thông tin: <span style="color: green;">{{$row->ThongTin}}</span></p>
                            <p>Yêu cầu: <span style="color: green;">{{$row->YeuCau}}</span></p>
                            <p>Liên hệ: <span><a href="tel:0903651882" style="text-decoration: none; color: green; font-weight: bold;">0903.651.882</a></span></p>
                        </div>
                        <div class="social clearfix">
                            <p class="more_y3"><a href="{{Route('getChiTietLop', ['slug' =>$row->slug])}}" title="Đăng ký dạy">ĐĂNG KÝ DẠY</a></p>
                        </div>
                    </div>
                @endforeach
            </div>
            <p class="more_g2"><a href="{{Route('getLopDangMo')}}" title="Danh sách lớp">XEM THÊM LỚP</a></p>
        </div>
    </div>
@endsection