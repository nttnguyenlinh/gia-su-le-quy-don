@extends('master')
@section('title', 'Đăng Ký Tìm Gia Sư - ')
@section('canonical', Route('getDangKyGiaSu'))
@section('content')
    <style>
        select:required:invalid {
            color: gray;
        }
        option[value=""][disabled] {
            display: none;
        }
    </style>
    <div class="col_c">
        <h1 class="class_ttl" style="font-size: 16pt; font-weight: bold;">ĐĂNG KÝ TÌM GIA SƯ</h1>
        @if(count($errors)>0)
            <div class="alert alert-danger" style="display: none;">
                @foreach($errors->all() as $err)
                    {{$err}}
                @endforeach
            </div>
        @endif
        <p style="font-weight:bolder; text-align:center; color: blue; margin-top: 10px;">Anh chị vui lòng sử dụng số điện thoại chuẩn 10 số để đăng ký!</p>
        <div class="formOut clearfix">
			<form method="post" action="{{Route('postDangKyGiaSu')}}" id="form_tim_gia_su">
            {{csrf_field()}}
            <div class="formOut_line clearfix">
                <p class="formOut_lab">Họ tên(<span>*</span>)</p>
                <div class="formOut_field">
                    <input type="text" id="hoten" name="hoten" class="in_405" maxlength="30" required/>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Địa chỉ<span>*</span>)</p>
                <div class="formOut_field">
                    <input type="text" id="diachi" name="diachi" class="in_405" maxlength="30" required/>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Điện thoại(<span>*</span>)</p>
                <div class="formOut_field">
                    <input type="text" id="dienthoai" name="dienthoai" placeholder="090xxxxxxx" class="in_405" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Email</p>
                <div class="formOut_field">
                    <input type="email" id="email" name="email" class="in_405" maxlength="30"/>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Chọn lớp(<span>*</span>)</p>
                <div class="formOut_field clearfix">
                    <span class="w160">
                    <select name="lop" id="lop" required>
                        <option value="" disabled selected>Chọn lớp</option>
                        @foreach($getLopHoc as $row)
                            <option value="{{$row->TenLop}}">{{$row->TenLop}}</option>
                        @endforeach
                    </select>
                    </span>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Môn học(<span>*</span>)</p>
                <div class="formOut_field">
                    <input type="text" id="monhoc" name="monhoc" placeholder="Ví dụ: Toán, lý, hóa" class="in_405" maxlength="30" required/>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Số lượng học sinh</p>
                <div class="formOut_field">
                    <input type="text" id="sohocsinh" name="sohocsinh" class="in_405" placeholder="Ví dụ: 1 học sinh" maxlength="30"/>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Học lực hiện tại</p>
                <div class="formOut_field">
                    <input type="text" id="hocluc" name="hocluc" class="in_405" placeholder="Ví dụ: trung bình" maxlength="30"/>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Số buổi học(<span>*</span>)</p>
                <div class="formOut_field clearfix">
                    <span class="w160">
                        <select name="sobuoi" id="sobuoi" required>
                            <option value="" disabled selected>Chọn số buổi/tuần</option>
                            <option value="2">2 buổi/tuần</option>
                            <option value="3">3 buổi/tuần</option>
                            <option value="4">4 buổi/tuần</option>
                            <option value="5">5 buổi/tuần</option>
                        </select>
                    </span>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Thời gian học(<span>*</span>)</p>
                <div class="formOut_field">
                    <input type="text" id="thoigian" name="thoigian" placeholder="Ví dụ: Thứ 2 - thứ 4; 17h - 19h" class="in_405" maxlength="30" required/>
                </div>
            </div>

            <div class="formOut_line clearfix">
                <p class="formOut_lab">Yêu cầu(<span>*</span>)</p>
                <div class="formOut_field clearfix">
                    <span class="w160">
                    <select name="level" id="level" required>
                        <option value="" disabled selected>Chọn yêu cầu</option>
                        <option value="Giáo viên">Giáo viên</option>
                        <option value="Sinh Viên">Sinh Viên</option>
                        <option value="Cử Nhân">Cử Nhân</option>
                        <option value="Kỹ Sư">Kỹ Sư</option>
                        <option value="Thạc Sỹ">Thạc Sỹ</option>
                        <option value="Tiến Sỹ">Tiến Sỹ</option>
                        <option value="Giảng Viên">Giảng Viên</option>
                        <option value="Bằng Cấp Khác">Bằng Cấp Khác</option>
                        <option value="Sinh Viên Sư Phạm">Sinh Viên Sư Phạm</option>
                        <option value="Cử Nhân Sư Phạm">Cử Nhân Sư Phạm</option>
                    </select>
                    </span>
                </div>
            </div>

            <!--
            <div class="formOut_line clearfix">
                <p class="formOut_lab"></p>
                <div class="formOut_field">
                    <p class="bg_txt bg_txt3">Chọn gia sư tại đây <a target="_blank" title="Chọn gia sư" href="gia-su-tieu-bieu.html">Chọn gia sư</a></p>
                </div>
            </div>
            -->
            <div class="formOut_line clearfix">
                <p class="formOut_lab">Yêu cầu khác</p>
                <div class="formOut_field">
                    <textarea id="yeucau" name="yeucau" class="area_100"  style="resize: vertical; height: auto;" maxlength="100"></textarea>
                </div>
            </div>

            <p class="dangky_btn clearfix">
                <button type="submit" class="btn_s2" style="float: none;">ĐĂNG KÝ</button>
            </p>
            <p>
                <i style="color: red; background: yellow;">
                    Lưu ý: Điền đầy đủ thông tin để được xét duyệt. Xin cảm ơn!
                </i>
            </p>
        </form>
		</div>
    </div>
@endsection