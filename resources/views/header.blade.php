<!DOCTYPE html>
<html lang="vi">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-language" content="vi" />

    <title>@yield('title')Gia Sư Lê Quý Đôn</title>
    <meta name="abstract" content="Gia Sư Lê Quý Đôn">

    <meta name="description" content="Gia sư Lê Quý Đôn với đội ngũ gia sư giỏi, giảng dạy tận tâm là sự lựa chọn đúng đắn của quý phụ huynh"/>
    <meta name="keywords" content="gia sư lê quý đôn, gia sư, gia su le quy don"/>

    <meta name="author" content="Nguyễn Linh, ntt.nguyenlinh@gmail.com">
    <meta name="copyright" content="Copyright 2018 - Gia Sư Lê Quý Đôn">
    <meta name="google-site-verification" content="Usst-0ctVdYySuNeMmzfGQk5drq81ojL3PyatNZTU_o" />

    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon image_src" href="/favicon.ico">
    <link rel="canonical" href="@yield('canonical')"/>

    <link rel="stylesheet" href="{{URL::asset('css/chuoi.css?v=4.0.1')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/style.css?v=4.0.1')}}" type="text/css" media="screen,print">
    <link rel="stylesheet" href="{{URL::asset('css/custom.css?v=4.0.1')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/jquery-ui-1.12.1.min.css?v=4.0.1')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('tt/sweetalert2_7.32.1/sweetalert2.min.css?v=4.0.1')}}" type="text/css">
    <script src="{{URL::asset('tt/sweetalert2_7.32.1/sweetalert2.all.min.js?v=4.0.1')}}"></script>

    <script src="{{URL::asset('js/jquery-1.12.4.min.js?v=4.0.1')}}"></script>

    <script src="{{URL::asset('js/jquery-ui-1.12.1.min.js?v=4.0.1')}}"></script>

    <style>

        select:invalid, select:required:invalid {
            color: gray;
        }

        option[value=""][disabled] {
            display: none;
        }
    </style>

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5XZ75SP" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=install_email
     page_id="1704488249663430"
     theme_color="#0084ff"
     logged_in_greeting="Xin chào! Gia sư Lê Quý Đôn giúp gì được cho anh(chị)?"
     logged_out_greeting="Xin chào! Gia sư Lê Quý Đôn giúp gì được cho anh(chị)?">
</div>

@if(Session::has('flag'))
    <script>
        swal({
            type: "{{Session::get('flag')}}",
            title: "",
            text: "{{Session::get('message')}}",
        })
    </script>
@endif

<div id="background"></div>
<div class="wrapper">
    <div class="header">
        <div class="header">
            <img class="logo" src="{{URL::asset('images/logo.png?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
            <ul class="menu clearfix">
                <li class="m1">
                    <a href="/" title="Gia Sư Lê Quý Đôn">TRANG CHỦ</a>
                </li>
                <li class="m2">
                    <a>PHỤ HUYNH</a>
                    <ul class="sub_menu">
                        <li><a href="{{Route('getDangKyGiaSu')}}" title="Đăng ký tìm gia sư">Đăng ký tìm gia sư</a></li>
                        <li><a href="{{Route('hocphithamkhao')}}" title="Học phí tham khảo">Học phí tham khảo</a></li>
                        <li><a href="{{Route('phuhuynhcanbiet')}}" title="Phụ huynh cần biết">Phụ huynh cần biết</a></li>
                        <li><a href="{{Route('getGiaSuTieuBieu')}}" title="Danh sách gia sư">Danh sách gia sư</a></li>
                    </ul>
                </li>
                <li class="m3">
                    <a>GIA SƯ</a>
                    <ul class="sub_menu">
                        <li><a href="{{Route('DkGiaSu')}}" title="Đăng ký làm gia sư">Đăng ký làm gia sư</a></li>
                        <li><a href="{{Route('getLopDangMo')}}" title="Danh sách lớp">Danh sách lớp</a></li>
                        <li><a href="{{Route('nganhang')}}" title="Tài khoản ngân hàng">Tài khoản ngân hàng</a></li>
                    </ul>
                </li>

                <li class="m4">
                    <a href="{{Route('getLopDangMo')}}" title="Danh sách lớp">Lớp mới</a>
                </li>

                <li class="m5">
                    <a href="{{Route('tuyendung')}}" title="Tuyển dụng">Tuyển dụng</a>
                </li>

                <li class="m6"><a href="{{Route('lienhe')}}" title="Liên hệ">Liên hệ</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="slideOut">
        <div id="slider" class="swipe">
            <div class="swipe-wrap">
                <div>
                    <img src="{{URL::asset('images/trung-tam-gia-su.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
                </div>
                <div>
                    <img src="{{URL::asset('images/trung-tam-gia-su-tphcm.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
                </div>
                <div>
                    <img src="{{URL::asset('images/trung-tam-gia-su-1.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
                </div>
                <div>
                    <img src="{{URL::asset('images/trung-tam-gia-su-2.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
                </div>

            </div>
        </div>
        <p class="slide_top">
            <img src="{{URL::asset('images/bg_slide.png?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
        </p>

        <div class="slide_left">
            <p style="font-size: 12pt; text-align: center;">Phụ huynh - học sinh<span style="text-align: center; font-size: 12pt;">Tìm gia sư</span></p>
            <p style="font-size: 14pt; margin-top: 5pt; color:red;">0975.034.745 (Cô Tâm)</p>
        </div>
        <div class="slide_right">
            <p style="font-size: 12pt; text-align: center;">Giáo viên - sinh viên<span style="text-align: center; font-size: 12pt;">Tìm lớp dạy</span></p>
        </div>
        <p class="r_phone" style="font-size: 14pt;">0903.651.882 (Cô Hà)</p>
    </div>

    <div class="pageBody clearfix">
        <div class="pageBodyInner clearfix">
            <div class="col_l">
                <div class="box">
                    <h3 class="bh_b">PHỤ HUYNH</h3>
                    <ul class="l_list1">
                        <li><a href="{{Route('getDangKyGiaSu')}}" title="Đăng ký tìm gia sư">Đăng ký tìm gia sư</a></li>
                        <li><a href="{{Route('hocphithamkhao')}}" title="Học phí tham khảo">Học phí tham khảo</a></li>
                        <li><a href="{{Route('phuhuynhcanbiet')}}" title="Phụ huynh cần biết">Phụ huynh cần biết</a></li>
                        <li><a href="{{Route('getGiaSuTieuBieu')}}" title="Danh sách gia sư">Danh sách gia sư</a></li>
                    </ul>
                </div>

                <div class="box">
                    <h3 class="bh_b">GIA SƯ</h3>
                    <ul class="l_list1">
                        <li><a href="{{Route('DkGiaSu')}}" title="Đăng ký làm gia sư">Đăng ký làm gia sư</a></li>
                        <li><a href="{{Route('getLopDangMo')}}" title="Danh sách lớp">Danh sách lớp</a></li>
                        <li><a href="{{Route('nganhang')}}" title="Tài khoản ngân hàng">Tài khoản ngân hàng</a></li>
                    </ul>
                </div>

                <div class="box">
                    <h3 class="bh_b">HỖ TRỢ </h3>
                    <ul class="l_list3">
                        <li>Phụ Huynh - Học Sinh</li>
                        <li>
                            0975.034.745<br><span>(Cô Tâm)</span>
                        </li>
                        <li>Giáo Viên - Sinh Viên</li>
                        <li>
                            0903.651.882<br><span>(Cô Hà)</span>
                        </li>
                    </ul>
                </div>


                <div class="box clearfix">
                    <h3 class="bh_b">BẢN ĐỒ</h3>
                    <div class="map">
                        <a data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!" target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("189/1/10C Dương Đình Hội, Phước Long B, Quận 9")}}','width=600,height=600'); return false;">
                            <img width="140px" src="{{URL::asset('images/bando.jpg?v=4.0.1')}}" alt="Gia sư Lê Quý Đôn">
                        </a>
                    </div>
                </div>

                <div class="box clearfix">
                    <h3 class="bh_b">FANPAGE</h3>
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgiasulequydon.net%2F&tabs&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1835120729871081" width="150" height="230" style="border:none;overflow:hidden" scrolling="true" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                </div>
            </div>
