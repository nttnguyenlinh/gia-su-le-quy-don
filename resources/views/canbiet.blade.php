@extends('master')
@section('title', 'Phụ Huynh Cần Biết - ')
@section('canonical', Route('phuhuynhcanbiet'))

@section('content')
    <div class="col_c">
        <div class="newClass">
            <div class="content_choose_class">
                <h1 class="class_ttl" style="font-size: 14pt; font-weight: bold;">PHỤ HUYNH CẦN BIẾT</h1><br>
                <p class="content-detail" style="margin-top: 10px;">
                    <h2 style="text-align: left; padding-left: 5px;">Kính Gửi Quý Phụ Huynh!</h2>

                    <ul class="tgs_l" style="font-size: 12pt;">
                        <li style="margin-left: 20px;">Làm thế nào để con mình học tập hiệu quả, đạt kết quả cao?</li>
                        <li style="margin-left: 20px;">Cho con học gia sư sao cho hiệu quả?</li>
                        <li style="margin-left: 20px;">Làm thế nào để tìm được gia sư giỏi và tận tâm?</li>
                    </ul>

                    <p style="font-size:12pt; padding-left: 30px; margin-top: 30px; text-indent: 30px; text-align: justify;">
                        Đó luôn luôn là những trăn trở, lo lắng của các bậc phụ huynh về tình hình học tập của con mình.
                    </p>

                    <p style="font-size:12pt; padding-left: 30px; margin-top: 10px; text-indent: 30px; text-align: justify;">
                        Công việc bận rộn, chương trình giáo dục đổi mới khiến các bậc phụ huynh gặp không ít khó khăn trong việc kèm cặp.
                    </p>

                    <br>
                    <h2 style="background: none; color:mediumseagreen; font-size: 13pt;">NGOÀI RA</h2>
                    <br>
                    <p style="font-size:12pt; padding-left: 30px; margin-top: 10px; text-indent: 30px; text-align: justify;">
                        Muốn bồi dưỡng nâng cao kiến thức cho con thi vào các trường chuyên, trường đại học.
                    </p>

                    <p style="font-size:12pt; padding-left: 30px; margin-top: 10px; text-indent: 30px; text-align: justify;">
                        Muốn nâng cao kiến thức ngoại ngữ, tin học cho bản thân để lấy chứng chỉ hoặc hỗ trợ cho công việc.
                    </p>
                    <br>
                    <h2 style="background: none; color:mediumseagreen; font-size: 13pt;">CHÚC MỪNG QUÝ PHỤ HUYNH</h2>
                   <br>

                    <p style="font-size:12pt; font-weight: bold; text-align: center; line-height: 1.5cm;">
                        TRUNG TÂM GIA SƯ LÊ QUÝ ĐÔN
                    </p>
                    <p style="font-size:12pt; font-weight: bold; text-align: center; line-height: 0.5cm; margin-bottom: 15px;">
                        UY TÍN – CHẤT LƯỢNG – TẬN TÂM – HIỆU QUẢ
                    </p>

                    <p style="font-size:12pt; padding-left: 30px; text-indent: 30px; text-align: justify; line-height: 1cm;">
                        Gia sư Lê Qúy Đôn là một trong những trung tâm uy tín về chất lượng ở TP.HCM. Trung tâm là nơi quy tụ các giáo viên có kinh nghiệm đang trực tiếp đứng lớp tại các trường tiểu học, các trường THCS và trường THPT trên địa bàn thành phố.
                    </p>

                    <p style="font-size:12pt; padding-left: 30px; text-indent: 30px; text-align: justify; line-height: 1cm;">
                        Ngoài ra trung tâm gia sư của chúng tôi còn cộng tác với đội ngũ sinh viên từ các trường đại học uy tín.
                    </p>

                    <p style="font-size:12pt; padding-left: 30px; text-indent: 30px; text-align: justify; line-height: 1cm;">
                        Chúng tôi là đội ngũ gia sư có chuyên môn vững vàng, nghiệp vụ sư phạm tốt, nhiệt tình và có trách nhiệm cao sẽ đáp ứng mọi yêu cầu học tập và rèn luyện cho tất cả học sinh ở các cấp học và mọi trình độ.
                    </p>
                    <br>
                    <h2 style="background: none; color:mediumseagreen; font-size: 13pt;">LĨNH VỰC DẠY KÈM</h2>
                    <ul class="tgs_l" style="font-size: 12pt;">
                        <li style="margin-left: 20px;">Mẫu giáo, tiểu học: Giáo viên tiểu học đứng lớp, đảm nhận rèn chữ đẹp – toán- tiếng việt – chính tả - kèm theo sổ báo bài trên lớp.</li>
                        <li style="margin-left: 20px;">Các lớp THCS – THPT: Dạy kèm các môn Toán – Lý – Hóa – Văn – Sinh – Ngoại ngữ...</li>
                        <li style="margin-left: 20px;">Ngoại ngữ: Giáo viên chuyên ngoại ngữ dạy Anh – Pháp – Hoa – Nhật – Hàn.</li>
                        <li style="margin-left: 20px;">Dạy các môn năng khiếu organ, piano, guitar, vẽ...</li>
                        <li style="margin-left: 20px;">Dạy tin học văn phòng, chứng chỉ A, B…</li>
                        <li style="margin-left: 20px;">Đặc biệt các lớp luyện thi tuyển sinh 10 và luyện thi đại học.</li>
                    </ul>
                    <br>
                    <h2 style="background: none; color:mediumseagreen; font-size: 13pt;">PHƯƠNG CHÂM GIẢNG DẠY</h2>

                    <ul class="tgs_l" style="font-size: 12pt;">
                        <li style="margin-left: 20px;">Lấy lại kiến thức cơ bản cho học sinh yếu.</li>
                        <li style="margin-left: 20px;">Dạy sát chương trình, dạy sâu kiến thức, dạy kỹ chuyên môn.</li>
                        <li style="margin-left: 20px;">Nâng cao kiến thức cho học sinh khá, giỏi.</li>
                        <li style="margin-left: 20px;">Thường xuyên báo cáo kết quả học tập đến quý phụ huynh.</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>
@endsection