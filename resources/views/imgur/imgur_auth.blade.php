<script defer>
    document.addEventListener("DOMContentLoaded", function(event) {
        fragments = window.location.href.split('#')[1] // fragments = 'access_token=abcde&refresh_token=xyzw&expires_in=315360000'
        $.post(window.location.origin + '/saveAuth', fragments)
            .done(function(data){
                console.log(data)
                // redirect về khi lưu thành công
                window.location.href = window.location.origin
            })
    });
</script>