<!DOCTYPE html>
<html lang="vi">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Danh Sách Gia Sư - Gia Sư Lê Quý Đôn</title>
    <meta name="abstract" content="Gia Sư Lê Quý Đôn">

    <meta http-equiv="content-language" content="vi" />
    
    <meta name="description" content="Gia sư Lê Quý Đôn với đội ngũ gia sư giỏi, giảng dạy tận tâm là sự lựa chọn đúng đắn của quý phụ huynh"/>
    <meta name="keywords" content="gia sư lê quý đôn, gia sư, gia su le quy don"/>

    <meta name="author" content="Nguyễn Linh, ntt.nguyenlinh@gmail.com">
    <meta name="copyright" content="Copyright 2018 - Gia Sư Lê Quý Đôn">
    <meta name="google-site-verification" content="Usst-0ctVdYySuNeMmzfGQk5drq81ojL3PyatNZTU_o" />
    
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon image_src" href="/favicon.ico">
    <link rel="canonical" href="{{Route('getGiaSuTieuBieu')}}"/>

    <link rel="stylesheet" href="{{URL::asset('css/chuoi.css?v=4.0.1')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/style.css?v=4.0.1')}}" type="text/css" media="screen,print">
    <link rel="stylesheet" href="{{URL::asset('css/custom.css?v=4.0.1')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('css/jquery-ui-1.12.1.min.css?v=4.0.1')}}" type="text/css">
    <link rel="stylesheet" href="{{URL::asset('tt/sweetalert2_7.32.1/sweetalert2.min.css?v=4.0.1')}}" type="text/css">
    <script src="{{URL::asset('tt/sweetalert2_7.32.1/sweetalert2.all.min.js?v=4.0.1')}}"></script>

    <script src="{{URL::asset('js/jquery-1.12.4.min.js?v=4.0.1')}}"></script>

    <script src="{{URL::asset('js/jquery-ui-1.12.1.min.js?v=4.0.1')}}"></script>

    <style>
        option[value=""][disabled] {
            display: none;
        }
    </style>

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5XZ75SP" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=install_email
     page_id="1704488249663430"
     theme_color="#0084ff"
     logged_in_greeting="Xin chào! Gia sư Lê Quý Đôn giúp gì được cho anh(chị)?"
     logged_out_greeting="Xin chào! Gia sư Lê Quý Đôn giúp gì được cho anh(chị)?">
</div>

    @if(Session::has('flag'))
        <script>
            swal({
                type: "{{Session::get('flag')}}",
                title: "",
                text: "{{Session::get('message')}}",
            })
        </script>
    @endif

    <?php
    /*
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        $geturl = $protocol . "://" . $_SERVER['HTTP_HOST'];
    */
    ?>
<div id="background"></div>
<div class="wrapper">
    <div class="header">
        <div class="header">
            <img class="logo" src="{{URL::asset('images/logo.png?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
            <ul class="menu clearfix">
                <li class="m1">
                    <a href="/" title="Gia Sư Lê Quý Đôn">TRANG CHỦ</a>
                </li>
                <li class="m2">
                    <a>PHỤ HUYNH</a>

                    <ul class="sub_menu">
                        <li><a href="{{Route('getDangKyGiaSu')}}" title="Đăng ký tìm gia sư">Đăng ký tìm gia sư</a></li>
                        <li><a href="{{Route('hocphithamkhao')}}" title="Học phí tham khảo">Học phí tham khảo</a></li>
                        <li><a href="{{Route('phuhuynhcanbiet')}}" title="Phụ huynh cần biết">Phụ huynh cần biết</a></li>
                        <li><a href="{{Route('getGiaSuTieuBieu')}}" title="Danh sách gia sư">Danh sách gia sư</a></li>
                    </ul>
                </li>
                <li class="m3">
                    <a>GIA SƯ</a>
                    <ul class="sub_menu">
                        <li><a href="{{Route('DkGiaSu')}}" title="Đăng ký làm gia sư">Đăng ký làm gia sư</a></li>
                        <li><a href="{{Route('getLopDangMo')}}" title="Danh sách lớp">Danh sách lớp</a></li>
                        <li><a href="{{Route('nganhang')}}" title="Tài khoản ngân hàng">Tài khoản ngân hàng</a></li>
                    </ul>
                </li>

                <li class="m4">
                    <a href="{{Route('getLopDangMo')}}" title="Danh sách lớp">Lớp mới</a>
                </li>

                <li class="m5">
                    <a href="{{Route('tuyendung')}}" title="Tuyển dụng">Tuyển dụng</a>
                </li>

                <li class="m6"><a href="{{Route('lienhe')}}" title="Liên hệ">Liên hệ</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="slideOut">
        <div id="slider" class="swipe">
            <div class="swipe-wrap">
                <div>
                    <img src="{{URL::asset('images/trung-tam-gia-su.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
                </div>
                <div>
                    <img src="{{URL::asset('images/trung-tam-gia-su-tphcm.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
                </div>
                <div>
                    <img src="{{URL::asset('images/trung-tam-gia-su-1.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
                </div>
                <div>
                    <img src="{{URL::asset('images/trung-tam-gia-su-2.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
                </div>

            </div>
        </div>
        <p class="slide_top">
            <img src="{{URL::asset('images/bg_slide.png?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn"/>
        </p>

        <div class="slide_left">
            <p style="font-size: 12pt; text-align: center;">Phụ huynh - học sinh<span style="text-align: center; font-size: 12pt;">Tìm gia sư</span></p>
            <p style="font-size: 14pt; margin-top: 5pt; color:red;">0975.034.745 (Cô Tâm)</p>
        </div>
        <div class="slide_right">
            <p style="font-size: 12pt; text-align: center;">Giáo viên - sinh viên<span style="text-align: center; font-size: 12pt;">Tìm lớp dạy</span></p>
        </div>
        <p class="r_phone" style="font-size: 14pt;">0903.651.882 (Cô Hà)</p>
    </div>

    <div class="pageBody clearfix">
        <div class="pageBodyInner clearfix">
            <div class="col_l">
                <div class="box">
                    <h3 class="bh_b">PHỤ HUYNH</h3>
                    <ul class="l_list1">
                        <li><a href="{{Route('getDangKyGiaSu')}}" title="Đăng ký tìm gia sư">Đăng ký tìm gia sư</a></li>
                        <li><a href="{{Route('hocphithamkhao')}}" title="Học phí tham khảo">Học phí tham khảo</a></li>
                        <li><a href="{{Route('phuhuynhcanbiet')}}" title="Phụ huynh cần biết">Phụ huynh cần biết</a></li>
                        <li><a href="{{Route('getGiaSuTieuBieu')}}" title="Danh sách gia sư">Danh sách gia sư</a></li>
                    </ul>
                </div>

                <div class="box">
                    <h3 class="bh_b">GIA SƯ</h3>
                    <ul class="l_list1">
                        <li><a href="{{Route('DkGiaSu')}}" title="Đăng ký làm gia sư">Đăng ký làm gia sư</a></li>
                        <li><a href="{{Route('getLopDangMo')}}" title="Danh sách lớp">Danh sách lớp</a></li>
                        <li><a href="{{Route('nganhang')}}" title="Tài khoản ngân hàng">Tài khoản ngân hàng</a></li>
                    </ul>
                </div>

                <div class="box">
                    <h3 class="bh_b">HỖ TRỢ </h3>
                    <ul class="l_list3">
                        <li>Phụ Huynh - Học Sinh</li>
                        <li>
                            0975.034.745<br><span>(Cô Tâm)</span>
                        </li>
                        <li>Giáo Viên - Sinh Viên</li>
                        <li>
                            0903.651.882<br><span>(Cô Hà)</span>
                        </li>
                    </ul>
                </div>


                <div class="box clearfix">
                    <h3 class="bh_b">BẢN ĐỒ</h3>
                    <div class="map">
                        <a data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!" target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("189/1/10C Dương Đình Hội, Phước Long B, Quận 9")}}','width=600,height=600'); return false;">
                            <img width="140px" src="{{URL::asset('images/bando.jpg?v=4.0.1')}}" alt="Gia sư Lê Quý Đôn">
                        </a>
                    </div>
                </div>

                <div class="box clearfix">
                    <h3 class="bh_b">FANPAGE</h3>
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgiasulequydon.net%2F&tabs&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1835120729871081" width="150" height="230" style="border:none;overflow:hidden" scrolling="true" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                </div>
            </div>


            <div class="col_c col_c2">
                <div class="newClass">
                    <h1 class="class_ttl" style="font-size: 14pt; font-weight: bold;">DANH SÁCH GIA SƯ</h1><br>

                    <h2 class="gstt_bh" style="background: #00BE67; color: white; font-weight: bold; padding-top: 5px;">Quý phụ huynh liên hệ
                        <strong style="color:green;">0975.034.745</strong> để được tư vấn gia sư miễn phí
                    </h2>

                    <div class="search clearfix" style="padding-left: 20px;">
                        <form method="post" action="{{Route('searchGiaSu')}}">
                            {{csrf_field()}}
                            <input type="text" name="tukhoa" value="" placeholder="Nhập mã số hoặc tên gia sư" class="in_search" style="margin-right:10px; color: orange; width: 30%; float: left;">
                            <select style="margin-bottom: 10px; margin-right:4px; color:orange;" name="lopday">
                                <option value="" disabled selected>Chọn lớp dạy</option>
                                @foreach($getLopHoc as $row)
                                    <option value="{{$row->MaLop}}">{{$row->TenLop}}</option>
                                @endforeach
                            </select>

                            <select style="margin-bottom: 10px; margin-right: 4px; color:orange;" name="monday">
                                <option value="" disabled selected>Chọn môn dạy</option>
                                @foreach($getMonHoc as $row)
                                    <option value="{{$row->MaMon}}">{{$row->TenMon}}</option>
                                @endforeach
                            </select>

                            <select style="margin-bottom: 10px;  margin-right: 4px; color:orange;" name="gioitinh">
                                <option value="" disabled selected>Giới tính</option>
                                <option value="Nam">Nam</option>
                                <option value="Nữ">Nữ</option>
                            </select>

                            <select style="margin-bottom: 10px;  margin-right: 4px; color:orange;" name="tinhthanh" id="tinhthanh">
                                <option value="1">Hồ Chí Minh</option>
                            </select>

                            <select style="margin-bottom: 10px;  margin-right: 4px; color:orange;" name="quanhuyen" id="quanhuyen">
                                <option value="" disabled selected>Quận / huyện</option>
                                @foreach($getKhuVuc as $row)
                                    <option value="{{$row->MaKV}}">{{$row->TenKV}}</option>
                                @endforeach
                            </select>

                            <select style="margin-bottom: 10px;  margin-right: 4px; color:orange;" name="trinhdo">
                                <option value="" disabled selected>Trình độ</option>
                                <?php
                                    $trinhdo = array("Giáo viên", "Sinh Viên", "Cử Nhân", "Kỹ Sư", "Thạc Sỹ", "Tiến Sỹ",
                                        "Giảng Viên", "Bằng Cấp Khác", "Sinh Viên Sư Phạm", "Cử Nhân Sư Phạm");
                                    foreach($trinhdo as $row)
                                        echo "<option value='$row'>$row</option>";
                                ?>
                            </select>

                            <input type="submit" name="timkiem" value="TÌM KIẾM" class="btn_s3" style="background:orange; float: none; width: 33% !important; border: orange 1px solid;">

                        </form>

                    </div>

                    <div class="giasu_d">
                        <div class="gs_item_l clearfix">
                            @foreach($getGiaSu as $row)
                                <div class="gs_item" style="min-height: 350px;">
                                    <div>
                                        @if($row->AnhThe != null)
                                            <img src='{{$row->AnhThe}}?v=4.0.1' title='{{$row->HoTen}} - {{$row->MaGiaSu}}' alt='{{$row->HoTen}}'/>
                                        @else
                                            <img src='https://i.imgur.com/zP22ia8.png?v=4.0.1' title='{{$row->HoTen}} - {{$row->MaGiaSu}}' alt='{{$row->HoTen}}'/>
                                        @endif
                                        Mã số: <strong class="gs3" style="font-size: medium;">{{$row->MaGiaSu}}</strong><span style="font-weight:bold; border: #00BE67 solid 1px; margin-left: 10px; padding: 2px; color: green;">{{$row->TrinhDo}}</span> <br>
                                        Gia sư: <strong class="gs3">{{$row->HoTen}}</strong><br>
                                        Năm sinh: <span class="gs4">{{substr($row->NgaySinh, 6, 4)}}</span><br>
                                        Trường: <span class="gs4">{{$row->TruongDaoTao}}</span><br>
                                        Chuyên ngành: <span class="gs4">{{$row->NganhHoc}}</span><br>
                                        Ra trường: <span class="gs4">{{$row->NamTN}}</span><br>
                                        <?php
                                            $lopday_array = explode(",", $row->LopDay);
                                            $monday_array = explode(",", $row->MonDay);
                                            $khuvuc_array = explode(",", $row->KhuVuc);
                                        ?>

                                        Lớp dạy:
                                        <span class="gs4">
                                            @foreach($getLopHoc as $lop)
                                                @foreach($lopday_array as $item)
                                                    @if($item == $lop->MaLop)
                                                        {{$lop->TenLop}},
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </span><br>

                                        Môn dạy:
                                        <span class="gs4">
                                            @foreach($getMonHoc as $mon)
                                                @foreach($monday_array as $item)
                                                    @if($item == $mon->MaMon)
                                                        {{$mon->TenMon}},
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </span><br>

                                        Khu vực:
                                        <span class="gs4">
                                            <?php
                                            echo $row->TinhThanh . "(";
                                            foreach($getKhuVuc as $khuvuc)
                                                foreach($khuvuc_array as $item)
                                                    if($item == $khuvuc->MaKV)
                                                        echo $khuvuc->TenKV . ", ";
                                            echo ")";
                                            ?>
                                            </span><br>

                                        Ưu điểm: <span class="gs4">{{$row->UuDiem}}</span>
                                    </div>
                                    <!--<p class="gs_buton"><a class="btn2">Chọn gia sư</a></p>-->
                                </div>
                            @endforeach
                        </div>
                        <div class="lgs_page clearfix" style="float: right;">{{$getGiaSu->links()}}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="f_inner clearfix">
                <div class="f_inner_l">
                    <ul class="f_list">
                        <li>
                            <p style="font-weight:bold; color: green;">Văn phòng 1</p>
                            <button style="font-weight:bold; border: #00BE67 solid 1px; padding: 2px; color: green;" data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!">
                                <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("497 Đường 10, Gò Vấp")}}','width=600,height=600'); return false;">
                                    497/8 Đường 10, Phường 8, Gò Vấp
                                </a>
                            </button>
                        </li>
                    </ul>
                </div>

                <div class="f_inner_r">
                    <ul class="f_list">
                        <li>
                            <p style="font-weight:bold; color: green;">Văn phòng 2</p>
                            <button style="font-weight:bold; border: #00BE67 solid 1px; padding: 2px; color: green;" data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!">
                                <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("189/1/10C Dương Đình Hội, Phước Long B, Quận 9")}}','width=600,height=600'); return false;">
                                    189/1/10C Dương Đình Hội, Phước Long B, Quận 9
                                </a>
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copyright">
            <a href="/" title="Gia Sư Lê Quý Đôn" style="text-decoration: none;">
                <p style="font-size: 12pt; font-weight: bolder; text-align: center; padding-top: 120px; margin-bottom: -30px;">GIA SƯ LÊ QUÝ ĐÔN</p>
            </a>
        </div>
    </div>
</div>

<a href="javascript:void(0);" id="scroll" title="Lên đầu trang" style="display: none; margin-bottom: 80px; margin-right: 25px;"><span></span></a>
<script type="text/javascript" src="{{URL::asset('js/swipe.js?v=4.0.1')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jcarousellite.js?v=4.0.1')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.validate.min.js?v=4.0.1')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/footer.js?v=4.0.1')}}"></script>
</body>
</html>
