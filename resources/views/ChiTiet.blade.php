@extends('master')
@section('title', "Gia Sư $getChiTiet->MonDay $getChiTiet->LopDay $getChiTiet->DiaChi - ")
@section('canonical', Route('getChiTietLop', ['slug' =>$getChiTiet->slug]))
@section('content')
<div class="col_c">
    <div class="newClass">
        <h1 class="class_ttl" style="font-size: 14pt; font-weight: bold;">{{mb_strtoupper($getChiTiet->MonDay)}} {{mb_strtoupper($getChiTiet->LopDay)}}</h1>
        <div class="ldcgs_info clearfix">
            <div class="ldcgs_info_l">
                <div class="clearfix">
                    <p style="margin-bottom: 10px; margin-top: 10px;">
                        Mã lớp: <span style="color: red; font-weight: bold;">{{$getChiTiet->MaPhieuMo}}</span>
                        @if($getChiTiet->Status == 1)
                            <span style="color: #fff; font-weight: bold;" class="label label-success">ĐÃ GIAO</span>
                        @else
                            <span style="color: #fff; font-weight: bold;" class="label label-danger">CHƯA GIAO</span>
                        @endif
                    </p>

                    <p style="margin-bottom: 10px;">Môn: <span style="color: green; font-weight: bold;">{{$getChiTiet->MonDay}}</span></p>
                    <p style="margin-bottom: 10px;">Lớp: <span style="color: green; font-weight: bold;">{{$getChiTiet->LopDay}}</span></p>
                    <p style="margin-bottom: 10px;">Địa chỉ:
                        <button style="font-weight:bold; border: #00BE67 solid 1px; padding: 2px; color: green;" data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!">
                            <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("$getChiTiet->DiaChi")}}','width=600,height=600'); return false;">
                                {{$getChiTiet->DiaChi}}
                            </a>
                        </button>
                    </p>
                    <p style="margin-bottom: 10px;">Dạy: <span style="color: green;">{{$getChiTiet->SoBuoiDay}} buổi/tuần</span></p>
                    <p style="margin-bottom: 10px;">Giờ học: <span style="color: green;">{{$getChiTiet->ThoiGian}}</span></p>
                    <p style="margin-bottom: 10px;">Lương: <span style="color: green;">{{$getChiTiet->Luong}} <sup>đ</sup>/tháng</span></p>
                    <p style="margin-bottom: 10px;">Thông tin: <span style="color: green;">{{$getChiTiet->ThongTin}}</span></p>
                    <p style="margin-bottom: 10px;">Yêu cầu: <span style="color: green;">{{$getChiTiet->YeuCau}}</span></p>
                    <p style="margin-bottom: 10px;">Liên hệ: <span><a href="tel:0903651882" style="text-decoration: none; color: green; font-weight: bold;">0903.651.882</a></span></p>
                </div>

            </div>

            <div class="ldcgs_info_r">
                <?php
                    $lephi = (float)$getChiTiet->LePhi;
                    $luong = (float)$getChiTiet->Luong;
                    $tongphi = (float)($luong * $lephi)/100;
                    $tongphi *= 1000000;
                    $tongphi = number_format($tongphi, 0, '', '.');
                ?>

            @if($getChiTiet->Status == 0)
                <div class="ldcgs_inner">
                    <style>
                        select:required:invalid {
                            color: gray;
                        }
                        option[value=""][disabled] {
                            display: none;
                        }
                    </style>
                    <h3 style="color: white;">ĐĂNG KÝ NHANH</h3>
                    <div class="ldcgs_f clearfix">
                        <form method="post" action="{{Route('postChiTietLop', ['slug' =>$getChiTiet->slug])}}">
                            {{csrf_field()}}
                            <input type="hidden" id="maphieu" name="maphieu" value="{{$getChiTiet->MaPhieuMo}}">
                            <input type="hidden" id="slug" name="slug" value="{{$getChiTiet->slug}}">
                            <input type="text" style="color: green" id="phone" name="phone" value="{{Session::has('sdt')?Session::get('sdt'):''}}" placeholder="Nhập số điện thoại của bạn" class="lgs_in_text" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                            <input type="hidden" id="lephi" name="lephi" value="{{$getChiTiet->LePhi}}">
                            <select id="hinhthuc" name="hinhthuc" style="width: 100%; color: green" required>
                                <option value="" disabled selected>Hình thức nhận lớp</option>
                                <option value="0">Chuyển khoản({{$getChiTiet->LePhi}}% = {{$tongphi}}<sup>đ</sup>)</option>
                                <option value="1">Tới trung tâm({{$getChiTiet->LePhi}}% = {{$tongphi}}<sup>đ</sup>)</option>
                            </select>
                            <p class="ldcgs_f_txt" style="padding-top: 5px;">Thời gian dạy:</p>
                            <input type="text" style="color: green" id="ngay" name="ngay" value="{{date('d-m-Y')}}" placeholder="Ngày dạy" class="lgs_in_text2" required/>
                            <select name="gio" id="gio" style="float:right; color: green" quired>
                                <option value='' disabled selected>Giờ dạy</option>
                            <?php
                                $gioday = array("8h", "8h30", "9h", "9h30", "10h","10h30", "14h", "14h30", "15h", "15h30", "16h","16h30", "17h", "17h30", "18h", "Lúc khác");
                                foreach($gioday as $row)
                                    echo "<option value='$row'>$row</option>";
                            ?>
                            </select>
                            <input type="text" style="color: green" id="noidung" name="noidung" placeholder="Ghi chú(nếu có)" class="lgs_in_text" maxlength="150"/>
                            <input type="submit" name="nhanlop" value="Nhận Lớp" class="btn_s2" style="background:green!important; border: 1px solid green; width: 100%!important;"/>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </div>

        <div class="dsgsdk"> <h4>DANH SÁCH GIA SƯ NHẬN DẠY</h4></div>
        <table class="dsgsdk_table">
            <tr>
                <th class="col_1" style="font-weight: bold;">STT</th>
                <th class="col_2" style="font-weight: bold;">Gia sư</th>
                <th class="col_3" style="font-weight: bold;">Ngày đăng ký</th>
                <th class="col_5" style="font-weight: bold;">Trạng thái</th>
            </tr>

            <?php
                $getPhieuNhan = \App\phieu_nhan_lop::where('MaPhieuMo', 'like', $getChiTiet->MaPhieuMo)->orderBy('created_at', 'desc')->get();
                $num = count($getPhieuNhan);
                $getGiaSu = \App\gia_su::all();
                $getPhieuMo = \App\phieu_mo_lop::all();
                $i = 1;

                foreach($getPhieuNhan as $row)
                {
                    while($num)
                    {
                        foreach($getGiaSu as $giasu)
                        {
                            if($row->MaGiaSu == $giasu->MaGiaSu)
                            {
                                foreach($getPhieuMo as $phieumo)
                                {
                                    if($row->MaPhieuMo == $phieumo->MaPhieuMo)
                                    {
                                        $status = $row->Status;
                                        $class = $label = '';
                                        switch ($status)
                                        {
                                            case  1:   $class='color3'; $label = 'Xem xét'; break;
                                            case  2:   $class='color2'; $label = 'Đủ điều kiện'; break;
                                            case  3:   $class='color1'; $label = 'Đã nhận'; break;
                                            case  4:   $class='color4'; $label = 'Không đạt'; break;
                                            default:   $class='color5'; $label = 'Chờ duyệt';
                                        }

                                        echo "<tr class='$class'>";
                                        echo "<td><span style='font-weight:bold; color: #679312; text-decoration: none; background:white; border:#679312 solid 1px; padding: 2px;'>$i</span></td>";
                                        echo "<td>$giasu->HoTen <span style='color: #679312; text-decoration: none; background:white; border:#679312 solid 1px; padding: 2px;'>$giasu->TrinhDo</span></td>";
                                        echo "<td>" .date_format(date_create($row->created_at), 'd-m-Y') . "</td>";
                                        echo "<td>$label</td>";
                                        echo "</tr>";
                                    }
                                }
                            }
                        }
                        $i++;
                        continue 2;
                    }
                }
            ?>
        </table>
        <p class="bg_txt">Cho phép tối đa 10 người đăng ký. Chỉ giao lớp cho 1 người đủ điều kiện nhận lớp và đóng lệ phí trước.</p>
    </div>
</div>
@endsection