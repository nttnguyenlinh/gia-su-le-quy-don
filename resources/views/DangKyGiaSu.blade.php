@extends('master')
@section('title', 'Đăng Ký Làm Gia Sư - ')
@section('canonical', Route('DkGiaSu'))
@section('content')
     <div class="col_c">
        <h1 class="class_ttl" style="font-size: 16pt; font-weight: bold;">ĐĂNG KÝ LÀM GIA SƯ</h1>
         @if(count($errors)>0)
             <div class="alert alert-danger">
                 @foreach($errors->all() as $err)
                     <li>{{$err}}</li>
                 @endforeach
             </div>
         @endif

         <p style="font-weight:bolder; text-align:center; color: blue; margin-top: 10px;">Anh chị vui lòng sử dụng số điện thoại chuẩn 10 số để đăng ký!</p>
        <div class="formOut clearfix">
            <form method="post" id="form_dk_gia_su" action="{{Route('postDkGiaSu')}}" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Tỉnh/Thành dạy(<span>*</span>)</p>
                    <div class="formOut_field w100">
                        <select name="tinhthanh" id="tinhthanh" required>
                            <option value="Hồ Chí Minh">Hồ Chí Minh</option>
                        </select>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Họ tên(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="hoten" name="hoten" value="" placeholder="" class="in_405" maxlength="30" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Ngày sinh(<span>*</span>)</p>
                    <div class="formOut_field clearfix">
                        <span class="w120">
                            <select name="ngay" id="ngay" required>
                                <option value="" disabled selected>Ngày</option>
                                <?php
                                    for($i = 1; $i <= 31; $i++)
                                        if($i < 10)
                                            echo "<option value='0$i'>0$i</option>";
                                        else
                                            echo "<option value='$i'>$i</option>";
                                ?>
                            </select>
                        </span>
                        <span class="w120">
                            <select name="thang" id="thang" required>
                                <option value="" disabled selected>Tháng</option>
                                <?php
                                for($i = 1; $i <= 12; $i++)
                                    if($i < 10)
                                        echo "<option value='0$i'>0$i</option>";
                                    else
                                        echo "<option value='$i'>$i</option>";
                                ?>
                            </select>
                        </span>
                        <span class="w120">
                            <select name="nam" id="nam" required>
                                <option value="" disabled selected>Năm</option>
                                <?php
                                for($i = (date('Y') - 18); $i >= 1960; $i--)
                                    echo "<option value='$i'>$i</option>";
                                ?>
                            </select>
                        </span>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Nguyên quán(<span>*</span>)</p>
                    <div class="formOut_field w100">
                        <select name="nguyenquan" id="nguyenquan" required>
                            <option value="" disabled selected>Tỉnh/thành trên CMND</option>
                            <?php
                                $tinhthanh = array("Hồ Chí Minh", "Hà Nội", "An Giang", "Bà Rịa - Vũng Tàu", "Bắc Cạn",
                                    "Bắc Giang", "Bạc Liêu", "Bắc Ninh", "Bến Tre", "Bình Định", "Bình Dương", "Bình Phước",
                                    "Bình Thuận", "Cà Mau", "Cần Thơ", "Cao Bằng", "Đà Nẵng", "Đắc Lắk", "Đắk Nông", "Điện Biên",
                                    "Đồng Nai", "Đồng Tháp", "Gia Lai", "Hà Giang", "Hà Nam", "Hà Tĩnh", "Hải Dương", "Hải Phòng",
                                    "Hậu Giang", "Hòa Bình", "Hưng Yên", "Khánh Hòa", "Kiên Giang", "Kon Tum", "Lai Châu", "Lâm Đồng",
                                    "Lạng Sơn", "Lào Cai", "Long An", "Nam Định", "Nghệ An", "Ninh Bình", "Ninh Thuận", "Phú Thọ", "Phú Yên",
                                    "Quảng Bình", "Quảng Nam", "Quảng Ngãi", "Quảng Ninh", "Quảng Trị", "Sóc Trăng", "Sơn La", "Tây Ninh", "Thái Bình",
                                    "Thái Nguyên", "Thanh Hóa", "Thừa Thiên - Huế", "Tiền Giang", "Trà Vinh", "Tuyên Quang", "Vĩnh Long", "Vĩnh Phúc",
                                    "Yên Bái");
                                foreach($tinhthanh as $row)
                                    echo "<option value='$row'>$row</option>";
                            ?>
                        </select>
                        <span class="w120">
                            <select name="giongnoi" id="giongnoi" required>
                                <option value="" disabled selected>Chọn giọng nói</option>
                                <?php
                                    $giongnoi = array("Miền Bắc", "Miền Trung", "Miền Nam");
                                    foreach($giongnoi as $row)
                                        echo"<option value='$row'>$row</option>";
                                ?>
                            </select>
                        </span><br>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Địa chỉ hiện tại(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="diachi" name="diachi" value="" class="in_405" maxlength="100" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">CMND(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="cmnd" name="cmnd" value="" class="in_405" maxlength="12" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Email(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="email" id="email" name="email" value="" class="in_405" maxlength="50" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Mật khẩu(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="password" id="password" name="password" value="" class="in_405" maxlength="30" required/>
                    </div>
                </div>
                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Xác nhận mật khẩu(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="password" id="password_confirm" name="password_confirm" value="" class="in_405" maxlength="30" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Điện thoại(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="phone" name="phone" value="" placeholder="0984513523" class="in_405" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Ảnh của bạn(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="file" name="anhthe" id="anhthe" accept="image/*" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Trường đào tạo(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="truonghoc" name="truonghoc" value="" placeholder="ví dụ: ĐH sư phạm" class="in_405" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Ngành học(<span>*</span>)</p>
                    <div class="formOut_field">
                        <input type="text" id="chuyennganh" name="chuyennganh" value="" placeholder="ví dụ: sư phạm vật lý" class="in_405" required/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Đơn vị công tác</p>
                    <div class="formOut_field">
                        <input type="text" id="congtac" name="congtac" value="" placeholder="Dành cho giáo viên, ví dụ: trường tiểu học Đinh Tiên Hoàng" class="in_405" maxlength="100"/>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Năm tốt nghiệp(<span>*</span>)</p>
                    <div class="formOut_field clearfix">
                        <span class="w120">
                            <select name="nam_totnghiep" id="nam_totnghiep" required>
                                <option value="" disabled selected>Năm tốt nghiệp</option>
                                <?php
                                    for($i = (date('Y') + 10); $i >= 1960; $i--)
                                        echo "<option value='$i'>$i</option>";
                                ?>
                            </select>
                        </span><br><br>
                        <p><i style="color: red; background: yellow;">
                                Ví dụ: Niên khóa ĐH/CĐ của bạn là 2015 - 2019. Bạn chọn năm tốt nghiệp là 2019.
                            </i>
                        </p>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Hiện là(<span>*</span>)</p>
                    <div class="formOut_field clearfix">
                        <span class="w160">
                            <select name="trinhdo" id="trinhdo" required>
                                <option value="" disabled selected>Trình độ</option>
                                <?php
                                    $trinhdo = array("Giáo viên", "Sinh Viên", "Cử Nhân", "Kỹ Sư", "Thạc Sỹ", "Tiến Sỹ",
                                    "Giảng Viên", "Bằng Cấp Khác", "Sinh Viên Sư Phạm", "Cử Nhân Sư Phạm");
                                    foreach($trinhdo as $row)
                                        echo "<option value='$row'>$row</option>";
                                ?>
                            </select>
                        </span>
                        <span class="w160">
                            <select name="gioitinh" id="gioitinh" required>
                                <option value="" disabled selected>Giới tính</option>
                                <?php
                                    $gioitinh = array("Nam", "Nữ");
                                    foreach($gioitinh as $row)
                                        echo "<option value='$row'>$row</option>";
                                ?>
                        </select>
                        </span>
                        <span class="w160">
                            <select name="loai_gv" id="loai_gv">
                                <option value="" disabled selected>Chọn loại giáo viên</option>
                                <?php
                                    $loai_gv = array("GV dạy tự do", "GV dạy tại trường", "GV dạy trung tâm");
                                    foreach($loai_gv as $row)
                                        echo "<option value='$row'>$row</option>";
                                ?>
                            </select>
                        </span><br/><br/>
                        <p>
                            <i style="color: red; background: yellow;">
                                Lưu ý: <b>Chọn loại giáo viên</b> chỉ dành riêng cho giáo viên nếu bạn không phải thì không cần chọn phần này.
                            </i>
                        </p>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Ưu điểm</p>
                    <div class="formOut_field">
                        <textarea id="uudiem" name="uudiem" placeholder="Nơi để bạn ghi thêm thông tin chi tiết về bản thân. Kinh nghiệm và thành tích bạn đạt được trong quá trình học tập và dạy kèm." class="area_100" maxlength="250"></textarea>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Môn dạy(<span>*</span>)</p>
                    <div class="formOut_field">
                        <ul class="check_l1 clearfix">
                            <?php
                                foreach($getMonHoc as $row)
                                    echo "<li><input type='checkbox' name='monday[]' value='$row->MaMon'/><label>$row->TenMon</label></li>";
                            ?>
                        </ul>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Lớp dạy(<span>*</span>)</p>
                    <div class="formOut_field">
                        <ul class="check_l2 clearfix">
                            <?php
                                foreach($getLopHoc as $row)
                                    echo "<li><input type='checkbox' name='lopday[]' value='$row->MaLop'/><label>$row->TenLop</label></li>";
                            ?>
                        </ul>
                    </div>
                </div>

                <div class="formOut_line clearfix">
                    <p class="formOut_lab">Khu vực dạy(<span>*</span>)</p>
                    <div class="formOut_field">
                        <ul class="check_l3 clearfix">
                            <?php
                                foreach($getKhuVuc as $row)
                                    echo "<li><input type='checkbox' name='khuvuc[]' value='$row->MaKV'/><label>$row->TenKV</label></li>";
                            ?>
                        </ul>
                    </div>
                </div>
                <p class="dangky_btn clearfix">
                    <input type="submit" id="submit" name="submit" value="ĐĂNG KÝ" class="btn_s3"/>
                </p>

                <p>
                    <i style="color: red; background: yellow;">
                        Lưu ý: Nhập đầy đủ thông tin đánh dấu (*) để được xét duyệt. Xin cảm ơn!
                    </i>
                </p>
            </form>
        </div>
    </div>
@endsection