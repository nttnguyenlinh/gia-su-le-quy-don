@extends('master')
@section('title', 'Liên Hệ - ')
@section('canonical', Route('lienhe'))
@section('content')
    <div class="col_c">
        <div class="newClass">
            <div class="content_choose_class">
                <h1 class="class_ttl" style="font-size: 16pt; font-weight: bold;">LIÊN HỆ</h1>
                <div class="box_contact box_tc">
                    <div class="content-box_contact">
                        <div class="f_inner clearfix">
                            <div class="f_inner">
                                <h2>GIA SƯ LÊ QUÝ ĐÔN</h2>

                                <ul class="gsgsdk_info2">
                                    <li  style="margin:5px;">
                                        <p style="font-size: 13pt;">Phụ huynh / học viên cần tìm gia sư xin liên hệ:</p>
                                        <p style="font-size: 13pt; margin: 20px;">
                                            <span style="color: #ff0000;">0975.034.745</span>
                                            <span style="color: #0000ff;">(Cô Tâm)</span>
                                        </p>
                                    </li>
                                    <li style="margin:5px;">
                                        <p style="font-size: 13pt;">Gia Sư cần tìm lớp dạy xin liên hệ:</p>
                                        <p style="font-size: 13pt; margin: 20px;">
                                            <span style="color: #ff0000;">0903.651.882</span>
                                            <span style="color: #0000ff;">(Cô Hà)</span>
                                        </p>
                                    </li>
                                </ul>

                                <hr style="border: 2px solid #00BE67;">

                                <div class="f_inner">
                                    <ul class="f_list">

                                        <li>
                                            <p style="font-weight:bold; color: green;">Văn phòng 1</p>
                                            <button style="font-weight:bold; border: #00BE67 solid 1px; padding: 2px; color: green;" data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!">
                                                <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("497 Đường 10, Gò Vấp")}}','width=600,height=600'); return false;">
                                                    497/8 Đường 10, Phường 8, Gò Vấp
                                                </a>
                                            </button>
                                        </li>

                                        <li>
                                            <p style="font-weight:bold; color: green;">Văn phòng 2</p>
                                            <button style="font-weight:bold; border: #00BE67 solid 1px; padding: 2px; color: green;" data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!">
                                                <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("189/1/10C Dương Đình Hội, Phước Long B, Quận 9")}}','width=600,height=600'); return false;">
                                                    189/1/10C Dương Đình Hội, Phước Long B, Quận 9
                                                </a>
                                            </button>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection