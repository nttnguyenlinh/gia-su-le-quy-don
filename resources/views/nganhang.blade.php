@extends('master')
@section('title', 'Tài Khoản Ngân Hàng - ')
@section('canonical', Route('nganhang'))
@section('content')
    <div class="col_c">
        <div class="newClass">
            <div class="content_choose_class">
                <h1 class="class_ttl" style="font-size: 14pt; font-weight: bold;">TÀI KHOẢN NGÂN HÀNG</h1><br>
                <div class="content-detail" style="margin-top: 10px;">
                    <p style="font-size: medium;">
                        Quý phụ huynh, gia sư nếu ở xa không có điều kiện đến trung tâm thì có thể chuyển khoản cho trung tâm. Sau khi trung tâm nhận được tiền sẽ liên hệ lại với quý phụ huynh, gia sư. Riêng gia sư sẽ nhận được:
                    </p>
                    <br>

                    <ul class="tgs_l" style="font-size: 12pt;">
                        <li style="margin-left: 20px;">Địa chỉ của phụ huynh</li>
                        <li style="margin-left: 20px;">Thông tin liên hệ với phụ huynh</li>
                        <li style="margin-left: 20px;">Trung tâm sẽ liên hệ với phụ huynh để xếp lịch giúp cho bạn</li>
                    </ul>

                    <br>
                    <p style="font-size: medium;">
                        Sau khi chuyển khoản bạn vui lòng liên hệ cho trung tâm biết.
                        Trung tâm sẽ gửi thông tin cho các bạn, ngay khi ngân hàng thông báo.
                    </p>

                    <br>
                    <p style="text-align: center; color:red; font-size: large; font-weight: bold;">
                        THÔNG TIN TÀI KHOẢN
                    </p>

                    <br><br>

                    <table width="100%">
                        <tbody>
                        <tr>
                           <th style="padding: 5px; text-align: center;">
                                <p><span style="color: #0000ff; font-weight: bold">AgriBank</span></p>
                            </th>

                            <th style="padding: 5px; text-align: center;">
                                <p><span style="color: #0000ff; font-weight: bold">SacomBank</span></p>
                            </th>
                        </tr>
                        <tr data-toggle="tooltip" title="Bấm vào dòng số tài khoản để sao chép!">
                            <td style="padding: 5px; text-align: center;">
                                <p><span onclick="copyToClipboard('#AgriBank')"  style="color: #ff0000; font-weight: bold;" id="AgriBank">6280215014582</span></p>
                                <p><span style="color: #000000; font-weight: bold;">Đỗ Đức Hồng Hà</span></p>
                                <p><span style="color: #000000;">Đông Sài Gòn</span></p>
                            </td>

                            <td style="padding: 5px; text-align: center;">
                                <p><span onclick="copyToClipboard('#SacomBank')"  style="color: #ff0000; font-weight: bold;" id="SacomBank">32413816413</span></p>
                                <p><span style="color: #000000; font-weight: bold;">Đỗ Đức Hồng Hà</span></p>
                                <p><span style="color: #000000;">Quận 9</span></p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });

        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
            /* Alert the copied text */
            alert("Số tài khoản đã được sao chép");
        };
    </script>
@endsection