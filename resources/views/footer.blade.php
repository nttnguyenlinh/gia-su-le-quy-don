
<div class="box">
    <h3 class="bh_g">THỜI GIAN LÀM VIỆC</h3>
    <ul class="l_list3">
        <li style="font-size: 14pt;">Buổi Sáng  <span style="color:red; font-size: 12pt;">8h - 11h30</span></li>
        <li></li>
        <li style="font-size: 14pt;">Buổi Chiều <span style="color:red; font-size: 12pt;">13h - 17h30</span></li>
        <li></li>
        <li style="font-size: 14pt;">Buổi Tối <span style="color:red; font-size: 12pt;">19h - 21h30</span></li>
    </ul>
</div>

<div class="box">
    <h3 class="bh_g">TK NGÂN HÀNG</h3>
    <ul class="blank_l">
        <li><a href="{{Route('nganhang')}}"><img src="{{URL::asset('images/bank1.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn" title="AgriBank"/></a></li>
        <li><a href="{{Route('nganhang')}}"><img src="{{URL::asset('images/bank7.jpg?v=4.0.1')}}" alt="Gia Sư Lê Quý Đôn" title="Sacombank"/></a></li>
    </ul>
</div>

<div class="box">
    <h3 class="bh_g">GIA SƯ TIÊU BIỂU</h3>
    <div class="giasutieubieu">
        <div class="jcarousel">
            <ul>
                <?php
                    $getGiaSu = App\gia_su::orderBy('MaGiaSu', 'desc')->where('Status', 1)->take(5)->get();
                    foreach($getGiaSu as $row)
                    {
                        echo "<li>";
                        if($row->AnhThe != null)
                            echo "<img src='$row->AnhThe?v=4.0.1' title='$row->HoTen - $row->MaGiaSu' alt='$row->HoTen'/>";
                        else
                            echo "<img src='https://i.imgur.com/zP22ia8.png?v=4.0.1' title='$row->HoTen - $row->MaGiaSu' alt='$row->HoTen'/>";
                        echo "</li>";
                    }
                ?>
            </ul>
        </div>
        <p class="more_g">
            <a href="{{Route('getGiaSuTieuBieu')}}" title="Xem thêm gia sư"><span style="padding-left: 10px; font-weight: bold;">Xem thêm</span></a>
        </p>
    </div>
</div>


<div class="box">
    <h3 class="bh_g">BỘ ĐẾM TRUY CẬP</h3>
    <img src="https://counter5.wheredoyoucomefrom.ovh/private/freecounterstat.php?c=j1pqq6kry2sxddfh4qycukypqp8azgdd" border="0" title="Bộ đếm truy cập" alt="Bộ đếm truy cập" style="margin: 0 5px;">
</div>

</div>
</div>

<div class="footer">
    <div class="f_inner clearfix">
        <div class="f_inner_l">
            <ul class="f_list">
                <li>
                    <p style="font-weight:bold; color: green;">Văn phòng 1</p>
                    <button style="font-weight:bold; border: #00BE67 solid 1px; padding: 2px; color: green;" data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!">
                        <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("497 Đường 10, Gò Vấp")}}','width=600,height=600'); return false;">
                            497/8 Đường 10, Phường 8, Gò Vấp
                        </a>
                    </button>
                </li>
            </ul>
        </div>

        <div class="f_inner_r">
            <ul class="f_list">
                <li>
                    <p style="font-weight:bold; color: green;">Văn phòng 2</p>
                    <button style="font-weight:bold; border: #00BE67 solid 1px; padding: 2px; color: green;" data-toggle="tooltip" title="Bấm vào đây để xem bản đồ!">
                        <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("189/1/10C Dương Đình Hội, Phước Long B, Quận 9")}}','width=600,height=600'); return false;">
                            189/1/10C Dương Đình Hội, Phước Long B, Quận 9
                        </a>
                    </button>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="copyright">
    <a href="/" title="Gia Sư Lê Quý Đôn" style="text-decoration: none;">
        <p style="font-size: 12pt; font-weight: bolder; text-align: center; padding-top: 120px; margin-bottom: -30px;">GIA SƯ LÊ QUÝ ĐÔN</p>
    </a>
</div>
</div>
</div>
<a href="javascript:void(0);" id="scroll" title="Lên đầu trang" style="display: none; margin-bottom: 80px; margin-right: 25px;"><span></span></a>
<script type="text/javascript" src="{{URL::asset('js/swipe.js?v=4.0.1')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jcarousellite.js?v=4.0.1')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/jquery.validate.min.js?v=4.0.1')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/footer.js?v=4.0.1')}}"></script>
</body>
</html>
