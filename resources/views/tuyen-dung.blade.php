@extends('master')
@section('title', 'Tuyển Dụng - ')
@section('canonical', Route('tuyendung'))
@section('content')
    <div class="col_c">
        <div class="newClass">
            <div class="content_choose_class">
                <h1 class="class_ttl" style="font-size: 14pt; font-weight: bold;">TUYỂN DỤNG</h1><br>
                <div class="content-detail" style="margin-top: 10px;">
                    @foreach($tuyendung as $row)
                        <p style="text-align: center; color: #ff0000; font-size: 14pt; font-weight: bold;">{{$row->TenPhieuTD}}</p>

                        <p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><span style="color:#0000FF;">Nơi làm:</span> {{$row->NoiLam}}</p>
                        <p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><span style="color:#0000FF;">Cần tuyển:</span> {{$row->SoLuong}} người</p>
                        <p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><span style="color:#0000FF;">Hồ sơ bao gồm:</span> {{$row->HoSo}}</p>
                        <p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><span style="color:#0000FF;">Yêu cầu:</span> {{$row->YeuCau}}</p>
                        <p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><span style="color:#0000FF;">Thời gian:</span> {{$row->ThoiGian}}</p>
                        <p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><span style="color:#0000FF;">Lương:</span> {{$row->Luong}}</p>
                        <p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><span style="color:#0000FF;">công việc:</span> {{$row->MotaCV}}</p>
                        <p style="margin: 6px 0px; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 33, 41); font-size: 14px;"><span style="color:#0000FF;">Quyền lợi:</span> {{$row->QuyenLoi}}</p>
                        <p style="text-align: justify;">&nbsp;</p>
                        <p style="text-align: justify;"><span style="font-size: medium;">Mọi chi tiết xin liên hệ <span style="color: #008000;">0903.651.882 (Cô Hà)</span></span></p>
                        <hr style="border: 2px solid #00BE67; margin-bottom: 10px;">
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
