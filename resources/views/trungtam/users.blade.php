@extends('trungtam.master')

@section('title', 'Dashboard')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="nav-menu-icon fal fa-home"></i>
                <a href="/">Home</a>
            </li>

            <li><a href="{{Route('trungtam')}}">Dashboard</a></li>
            <li class="active"><a href="{{Route('trungtam.users')}}">Người dùng</a></li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </div>
            @endif

            @if (Session::has('flag'))
                <div class="alert alert-{{Session::get('flag')}}">{{Session::get('message')}}</div>
            @endif

            <button class="btn btn-white btn-info btn-bold" data-toggle="modal" data-target="#mod" data-backdrop="false">
                <i class="ace-icon fa fa-plus-circle"></i>Thêm người dùng
            </button>

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>

            <table id="table_users" class="table table-bordered table-hover table-responsive">
                <tr style="background: #307ECC; color: white;">
                    <th style="text-align: center; width: 80px;">ID</th>
                    <th style="text-align: center;">Họ tên</th>
                    <th style="text-align: center;">Tài khoản</th>
                    <th style="text-align: center;">Email</th>
                    <th style="text-align: center; width: 80px;"><i class="far fa-lock"></i></th>
                    <th style="text-align: center;">#</th>
                </tr>

                <tbody style="text-align: center;">
                @foreach($admin as $id => $row)
                    <tr>
                        <td>
                            <button class="btn-id show-details-btn" title="Xem chi tiết">{{$row->id}}</button>
                        </td>

                        <td>
                            {{$row->name}}
                        </td>

                        <td>
                            {{$row->username}} <br>
                            @if($row->Status)
                                <span style="color: #679312; text-decoration: none; background:transparent; border:#679312 solid 1px; padding: 2px;">Hoạt động</span>
                            @else
                                <span style="color: #ff6069; text-decoration: none; background:transparent; border:#ff6069 solid 1px; padding: 2px;">Đang khoá</span>
                            @endif
                        </td>

                        <td>
                            <a href="mailto:{{$row->email}}" title="Gửi mail" style="text-decoration: none; font-weight: bold;">{{$row->email}}</a>
                        </td>

                        <td>
                            @if($row->Status)
                                <input type="checkbox" class="simpleCheck switch users-status" id="{{$row->id}}" data-status="enable" title="Đang hoạt động"/>
                            @else
                                <input type="checkbox" class="simpleCheck switch simpleCheck-checked users-status" id="{{$row->id}}" data-status="disable" title="Đang khoá"/>
                            @endif
                        </td>

                        <td>
                            <div class="hidden-sm hidden-xs btn-group">
                                <a href="javascript:void(0);" id="{{$row->id}}" class="purple users_password" title="Khôi phục mật khẩu">
                                    <i class="fal fa-sync-alt bigger-150"></i>
                                </a>

                                <span style="margin: 0px 1px;">&nbsp;</span>
                                <a href="javascript:void(0);" id="{{$row->id}}" class="red users_delete" title="Xoá người dùng">
                                    <i class="fal fa-trash-alt bigger-150"></i>
                                </a>
                            </div>

                            <div class="hidden-md hidden-lg">
                                <div class="inline pos-rel">
                                    <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                        <i class="fal fa-plus bigger-120"></i>
                                    </button>

                                    <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                        <li>
                                            <a href="javascript:void(0);" id="{{$row->id}}" class="purple users_password" title="Reset Mk">
                                                <i class="fal fa-sync-alt bigger-120"></i>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="javascript:void(0);" id="{{$row->id}}" class="red users_delete" title="Xoá">
                                                <i class="fal fa-trash-alt bigger-120"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="detail-row">
                        <td colspan="8">
                            <div class="table-detail">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="text-center">
                                            @if(!empty($row->avatar))
                                                <img src="{{$row->avatar}}?v=4.0.1" class="thumbnail inline margin-bottom" alt="{{$row->name}}" style="width: 170px; height: 150px; padding: 0px; margin-bottom: 0px;"/>
                                            @else
                                                <img src="https://i.imgur.com/3Ox71xn.jpg?v=4.0.1" class="thumbnail inline margin-bottom" alt="{{$row->name}}" style="width: 170px; height: 150px; padding: 0px; margin-bottom: 0px;"/>
                                            @endif

                                            @if($row->Status)
                                                <div class="width-65 label label-info label-xlg arrowed-in arrowed-in-right">
                                                    <i class="ace-icon fa fa-circle light-green"></i>
                                                    <span class="white">Hoạt động</span>
                                                </div>
                                            @else
                                                <div class="width-65 label label-danger label-xlg arrowed-in arrowed-in-right">
                                                    <i class="ace-icon fa fa-circle light-red"></i>
                                                    <span class="white">Bị khoá</span>
                                                </div>
                                            @endif


                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-8" style="margin-top: 10px;">
                                        <div class="space visible-xs"></div>
                                        <div class="profile-user-info profile-user-info-striped">

                                            <div class="profile-info-row">
                                                <div class="profile-info-name">Họ tên</div>
                                                <div class="profile-info-value">
                                                    <span>{{$row->name}}</span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name">User ID</div>
                                                <div class="profile-info-value">
                                                    <span>{{$row->id}}</span>
                                                </div>
                                            </div>


                                            <div class="profile-info-row">
                                                <div class="profile-info-name">Tài khoản</div>
                                                <div class="profile-info-value">
                                                    <span>{{$row->username}}</span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name">Email</div>
                                                <div class="profile-info-value">
                                                    <span><a href="mailto:{{$row->email}}" title="Gửi mail" style="text-decoration: none; font-weight: bold;">{{$row->email}}</a></span>
                                                </div>
                                            </div>

                                            <div class="profile-info-row">
                                                <div class="profile-info-name">Ngày Đk</div>
                                                <div class="profile-info-value">
                                                    <span>{{date_format(date_create($row->created_at), "d-m-Y")}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!--Thêm người dùng-->
    <div class="modal" id="mod" role="dialog">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title">Thêm người dùng</h4>
                    <span class="close" style="font-size: 12pt; padding-top: 10px; padding-right: 10px; color: #ff4871;" data-dismiss="modal">&times; Đóng</span>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form id="form_users_add" method="post" action="{{route('trungtam.users_add')}}">
                            {{csrf_field()}}
                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Họ tên" required maxlength="30" autofocus/>
                                    <i class="ace-icon fal fa-user"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="username" id="username" placeholder="Tài khoản" required maxlength="30"/>
                                    <i class="ace-icon fal fa-key"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" required/>
                                    <i class="ace-icon fal fa-envelope"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Mật khẩu" required/>
                                    <i class="ace-icon fal fa-lock"></i>
                                </span>
                            </label>

                            <div class="space-6"></div>

                            <div class="clearfix">
                                <button type="reset" class="width-30 pull-left btn btn-sm">
                                    <i class="ace-icon fal fa-refresh"></i>
                                    <span class="bigger-110">Clear</span>
                                </button>

                                <button type="submit" class="width-30 pull-right btn btn-sm btn-success" id="action">
                                    <span class="bigger-110">Thêm mới</span>
                                    <i class="ace-icon fal fa-arrow-right icon-on-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection