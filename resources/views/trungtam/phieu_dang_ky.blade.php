@extends('trungtam.master')
@section('title', 'Dashboard')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="nav-menu-icon fal fa-home"></i>
                <a href="/">Home</a>
            </li>

            <li><a href="{{Route('trungtam')}}">Dashboard</a></li>
            <li class="active"><a href="{{Route('trungtam.phieudangky')}}">Danh sách đăng ký</a></li>
        </ul>
    </div>
@endsection

@section('page-content')
     <div class="row">
        <div class="col-xs-12">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </div>
            @endif

            @if (Session::has('flag'))
                <div class="alert alert-{{Session::get('flag')}}">
                    {{Session::get('message')}}
                </div>
            @endif

            <button class="btn btn-white btn-info btn-bold phieudangky_add" id="phieudangky_add"  data-backdrop="false">
                <i class="ace-icon fa fa-plus-circle"></i>Đăng ký mới
            </button>

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div>
                <table id="table_phieudangky" class="table table-bordered table-responsive table-hover">
                    <thead>
                        <tr>
                            <th style="text-align: center;">ID</th>
                            <th style="text-align: center;">Họ tên</th>
                            <th style="text-align: center;">Liên hệ</th>
                            <th style="text-align: center; width: 80px;">Duyệt</th>
                            <th style="text-align: center;">#</th>
                        </tr>
                    </thead>
                    <tbody style="text-align: center;">
                        @foreach($phieudk as $id => $row)
                            <tr>
                                <td>
                                    <button class="btn-id" data-toggle="modal" data-target="#model-{{$id}}" data-backdrop="false" title="Xem chi tiết">{{$row->MaPhieuDK}}</button>
                                </td>
                                <td>
                                    <p>{{$row->HoTen}}</p>

                                    @if($row->Status)
                                        <p class="label label-success">Đã duyệt</p>
                                    @else
                                        <p class="label label-danger">Chưa duyệt</p>
                                    @endif
                                </td>

                                <td>
                                    <p><a href="tel:{{$row->SoDT}}">{{$row->SoDT}}</a></p>
                                    <p><a href="mailto:{{$row->Email}}">{{$row->Email}}</a></p>
                                    <p>
                                        <a target="popup" onclick="window.open('https://www.google.com/maps/place/{{urlencode("$row->DiaChi")}}','width=600,height=600'); return false;">
                                            {{$row->DiaChi}}
                                        </a>
                                    </p>
                                </td>
                                <td>
                                    <div id="switch" style="padding-top: 10px;">
                                        @if($row->Status)
                                            <input type="checkbox" class="simpleCheck switch simpleCheck-checked" title="Đã duyệt"/>
                                        @else
                                            <input type="checkbox" class="simpleCheck switch phieudangky_apply" id="{{$row->MaPhieuDK}}" data-backdrop="false" title="Chưa duyệt"/>
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    <a href="javascript:void(0);" id="{{$row->MaPhieuDK}}" class="red phieudangky_delete" title="Xoá phiếu đăng ký">
                                        <i class="fal fa-trash-alt bigger-150" style="padding-top: 10px;"></i>
                                    </a>
                                </td>
                            </tr>
                            <!--Xem chi tiết-->
                            <div class="modal fade" id="model-{{$id}}">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="widget-box">
                                        <div class="table-header text-uppercase">
                                            <button type="button" class="close" data-dismiss="modal" title="Đóng">
                                                <i class="fal fa-times white" style="padding-top: 5px;"></i>
                                            </button>
                                            Phiếu đăng ký của {{$row->HoTen}} - Ngày đăng ký: {{date_format(date_create($row->created_at), "d-m-Y")}}
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="space visible-xs"></div>
                                                        <div class="profile-user-info profile-user-info-striped">
                                                            <div class="Table" role="grid">

                                                                <div class="Heading" role="columnheader">
                                                                    <div class="Cell">
                                                                        <p>Lớp/Môn</p>
                                                                    </div>
                                                                    <div class="Cell" role="columnheader">
                                                                        <p>Học viên</p>
                                                                    </div>
                                                                    <div class="Cell" role="columnheader">
                                                                        <p>Buổi học</p>
                                                                    </div>
                                                                    <div class="Cell" role="columnheader">
                                                                        <p>Yêu cầu</p>
                                                                    </div>
                                                                </div>

                                                                <div class="Row" role="row">
                                                                    <div class="Cell" role="gridcell">
                                                                        <p><b>Lớp:</b>{{$row->Lop}}</p>
                                                                        <p><b>Môn:</b>{{$row->MonHoc}}</p>
                                                                    </div>
                                                                    <div class="Cell" role="gridcell">
                                                                        <p><b>Số lượng:</b>{{$row->SoHocSinh}} </p>
                                                                        <p><b>Học lực:</b>{{$row->HocLuc}}</p>
                                                                    </div>
                                                                    <div class="Cell" role="gridcell">
                                                                        <p><b>Số buổi:</b>{{$row->SoBuoiDay}} buổi/tuần</p>
                                                                        <p><b>Giờ học:</b>{{$row->ThoiGianHoc}}</p>
                                                                    </div>
                                                                    <div class="Cell" role="gridcell">
                                                                        <p><b>Yêu cầu:</b>{{$row->YeuCau}}</p>
                                                                        <p><b>Ghi chú:</b>{{$row->YeuCauKhac}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-sm btn-danger fa-pull-right" data-dismiss="modal">
                                                <i class="ace-icon fa fa-times"></i> Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal" id="phieudangky_dialog_add">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title" id="widget-title-dangky">Đăng ký & mở lớp</h4>
                    <span class="close close-dangky" style="font-size: 12pt; padding-top: 10px; padding-right: 10px; color: #ff4871;">Đóng</span>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form method="post" id="form_phieudangky_add" action="{{route('trungtam.phieudangky_add')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="maphieudk" id="maphieudk"/>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input style="color:#057bbe;" type="text" class="form-control" name="hoten"  id="hoten" maxlength="50" title="Họ tên phụ huynh" placeholder="Họ tên" required autofocus/>
                                    <i class="ace-icon fal fa-user" id="iconhoten"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input style="color:#057bbe;" type="text" name="diachi" id="diachi" title="Địa chỉ"  maxlength="50" placeholder="Địa chỉ phụ huynh" class="form-control" required/>
                                    <i class="ace-icon fal fa-map-marked" id="icondiachi"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input style="color:#057bbe;" type="text" name="diachitt" id="diachitt" title="Địa chỉ mở lớp" maxlength="50" placeholder="Địa chỉ mở lớp" class="form-control" required autofocus/>
                                    <i class="ace-icon fal fa-map-marked" id="icondiachitt"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input style="color:#057bbe;  width:43%;" type="text" name="dienthoai"  id="dienthoai" maxlength="10" title="Số điện thoại" style="width: 50%;" placeholder="Số điện thoại phụ huynh" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                                    <input style="color:#057bbe; width:56%;" type="email" name="email" id="email" maxlength="50" title="Email" style="width: 49%;"placeholder="Email phụ huynh"/>
                                    <i class="ace-icon fal fa-envelope" id="iconemail"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <select name="lop" id="lop" required style="width: 40%;">
                                        <option value="" disabled selected>Chọn lớp</option>
                                        @foreach($getLopHoc as $item)
                                            <option value="{{$item->TenLop}}">{{$item->TenLop}}</option>
                                        @endforeach
                                    </select>
                                    <input style="color:#057bbe; width:59%;" type="text" name="monhoc" id="monhoc" title="Môn học" style="width: 59%;" placeholder="Môn học | Ví dụ: Toán, lý, hóa,..." maxlength="100" required/>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input style="color:#057bbe;  width:43%;" type="text" name="sohs" id="sohs" title="Số lượng học sinh" maxlength="20" style="width: 40%;" placeholder="Số lượng học sinh"/>
                                    <input style="color:#057bbe; width:56%;" type="text" name="hocluc" id="hocluc" maxlength="20" title="Học lực của học sinh" style="width: 59%;" placeholder="Học lực của học sinh"/>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input style="color:#057bbe;" type="text" name="thongtinhs" id="thongtinhs" title="Thông tin học sinh để mở lớp" maxlength="50" class="form-control" placeholder="Thông tin học sinh để mở lớp"/>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <?php $buoi_array = array('2', '3', '4', '5'); ?>
                                    <select name="sobuoi" id="sobuoi" style="width: 40%;" required>
                                        <option value="" disabled selected>Số buổi học /tuần</option>
                                        @foreach($buoi_array as $id => $value)
                                            <option value="{{$value}}">{{$value}} buổi/tuần</option>
                                        @endforeach
                                    </select>
                                    <input style="color:#057bbe; width:59%;" type="text" name="thoigian" id="thoigian" maxlength="50" title="Thời gian học" style="width: 59%;" placeholder="Ví dụ: Thứ 2 - thứ 4; 17h - 19h" required/>
                                    <i class="ace-icon fal fa-calendar-star"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                     <select name="yeucau" id="yeucau" style="width: 40%;" required>
                                        <option value="" selected disabled>Chon người dạy</option>
                                        <option value="Giáo viên">Giáo viên</option>
                                        <option value="Sinh Viên">Sinh Viên</option>
                                        <option value="Cử Nhân">Cử Nhân</option>
                                        <option value="Kỹ Sư">Kỹ Sư</option>
                                        <option value="Thạc Sỹ">Thạc Sỹ</option>
                                        <option value="Tiến Sỹ">Tiến Sỹ</option>
                                        <option value="Giảng Viên">Giảng Viên</option>
                                        <option value="Bằng Cấp Khác">Bằng Cấp Khác</option>
                                        <option value="Sinh Viên Sư Phạm">Sinh Viên Sư Phạm</option>
                                        <option value="Cử Nhân Sư Phạm">Cử Nhân Sư Phạm</option>
                                    </select>
                                    <input style="color:#057bbe; width:59%;" type="text" name="yeucaukhac" id="yeucaukhac" title="Ghi chú (nếu có)" maxlength="100" style="width: 59%;" placeholder="Ghi chú (nếu có)"/>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input style="color:#057bbe;" type="text" name="luong" id="luong" title="Lương gia sư (chưa tính phí %)" maxlength="10" placeholder=" Lương gia sư, VD: 3.600.000" class="form-control" required/>
                                    <i class="ace-icon fal fa-money-bill" id="iconluong"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input style="color:#057bbe;" type="text" name="mucphi" id="mucphi" title="Mức phí %" placeholder="Mức phí, VD: 20" class="form-control" maxlength="2" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                                    <i class="ace-icon fal fa-exchange" id="iconmucphi"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input style="color:#057bbe;" type="text" name="yeucautt" id="yeucautt" title="Tóm lại yêu cầu để mở lớp" maxlength="100" placeholder="Tóm lại yêu cầu để mở lớp" class="form-control" required/>
                                </span>
                            </label>

                            <div class="clearfix">
                                <input type="submit" name="themdk" id="themdk" value="Add" class="width-30 pull-right btn btn-sm btn-success"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
