<style>
    .nav-menu-icon{
        font-size: 14pt;
        margin-right: 5px;
        margin-left: 5px;
    }
</style>

<ul class="nav nav-list">
    <li title="Dashboard">
        <a href="{{Route('trungtam')}}">
            <i class="nav-menu-icon fal fa-tachometer-alt"></i>
            Dashboard
        </a>
        <b class="arrow"></b>
    </li>

    <li title="Người dùng">
        <a href="{{Route('trungtam.users')}}">
            <i class="nav-menu-icon fal fa-user"></i>
            Người dùng
        </a>
        <b class="arrow"></b>
    </li>

    <li title=" Gia sư">
        <a href="{{Route('trungtam.giasu')}}">
            <i class="nav-menu-icon fal fa-chalkboard-teacher"></i>
            Gia sư
            @if(\App\gia_su::where('Status', 0)->count() != 0)
                <span class="badge badge-primary">{{\App\gia_su::where('Status', 0)->count()}}</span>
            @endif
        </a>
        <b class="arrow"></b>
    </li>

    <li title="D.sách đăng ký">
        <a href="{{Route('trungtam.phieudangky')}}">
            <i class="nav-menu-icon fal fa-layer-plus"></i>
            D.sách đăng ký
            @if(\App\phieu_dang_ky::where('Status', 0)->count() != 0)
                <span class="badge badge-primary">{{\App\phieu_dang_ky::where('Status', 0)->count()}}</span>
            @endif
        </a>
        <b class="arrow"></b>
    </li>

    <li title="Danh sách Lớp">
        <a href="{{Route('trungtam.phieumolop')}}">
            <i class="nav-menu-icon fal fa-list-alt"></i>
            Danh sách Lớp
            @if(\App\phieu_mo_lop::where('Status', 0)->count() != 0)
                <span class="badge badge-primary">{{\App\phieu_mo_lop::where('Status', 0)->count()}}</span>
            @endif
        </a>
        <b class="arrow"></b>
    </li>

    <li title="D.sách nhận dạy">
        <a href="{{Route('trungtam.nhanlop')}}">
            <i class="nav-menu-icon fal fa-list-ol"></i>
            D.sách nhận dạy
            @if(\App\phieu_nhan_lop::where('Status', 0)->count() != 0)
                <span class="badge badge-primary">{{\App\phieu_nhan_lop::where('Status', 0)->count()}}</span>
            @endif
        </a>
        <b class="arrow"></b>
    </li>

    <li title="Học phí">
        <a href="{{Route('trungtam.hocphi')}}">
            <i class="nav-menu-icon fal fa-credit-card"></i>
            Học phí
        </a>
        <b class="arrow"></b>
    </li>

    <li title="Tuyển dụng">
        <a href="{{Route('trungtam.tuyendung')}}">
            <i class="nav-menu-icon fal fa-atom-alt"></i>
            Tuyển dụng
        </a>
        <b class="arrow"></b>
    </li>

    <li title="Công cụ">
        <a class="dropdown-toggle">
            <i class="nav-menu-icon fal fa-cogs"></i>
            <span class="menu-text">Công cụ</span>
            <b class="arrow fal fa-level-down-alt"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu nav-show" style="display: block;">
            <li >
                <a href="{{Route('trungtam.giasu_sdt_password')}}">
                    <i class="nav-menu-icon fal fa-caret-right"></i>
                    Thay đổi SĐT
                </a>
                <b class="arrow"></b>
            </li>

            <li >
                <a href="{{Route('trungtam.giasu_sdt_password')}}">
                    <i class="nav-menu-icon fal fa-caret-right"></i>
                    Reset mật khẩu
                </a>
                <b class="arrow"></b>
            </li>
        </ul>
    </li>
</ul>