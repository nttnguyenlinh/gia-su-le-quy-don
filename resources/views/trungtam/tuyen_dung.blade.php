@extends('trungtam.master')
@section('title', 'Dashboard')
@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="/">Home</a>
            </li>

            <li><a href="{{Route('trungtam')}}">Dashboard</a></li>
            <li class="active"><a href="{{Route('trungtam.tuyendung')}}">Tuyển dụng</a></li>
        </ul>
    </div>
@endsection


@section('page-content')
    <div class="row">
        <div class="col-xs-12">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </div>
            @endif

            @if (Session::has('flag'))
                <div class="alert alert-{{Session::get('flag')}}">
                    {{Session::get('message')}}
                </div>
            @endif

            <button class="btn btn-white btn-info btn-bold tuyendung_add" id="tuyendung_add" data-backdrop="false">
                <i class="ace-icon fa fa-plus-circle"></i>Đăng tuyển dụng
            </button>

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>

            <div>
                <table id="table-tuyendung" class="table table-bordered table-responsive table-hover">
                    <thead>
                    <tr>
                        <th style="text-align: center;">ID</th>
                        <th style="text-align: center;">Tiêu đề</th>
                        <th style="text-align: center;">Công việc</th>
                        <th style="text-align: center;">Quyền lợi</th>
                        <th style="text-align: center;">#</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($tuyendung as $id => $row)
                            <tr>
                                <td>
                                    <button class="btn-id" data-toggle="modal" data-target="#model-{{$id}}" data-backdrop="false" title="Xem chi tiết">{{$row->MaPhieuTD}}</button>
                                </td>

                                <td id="{{$row->MaPhieuTD}}" class="tuyendung_edit" data-backdrop="false" title="Chỉnh sửa">
                                    <p style="font-weight: bold;">{{$row->TenPhieuTD}}</p>
                                    <p class="label label-success"><b>Số lượng:</b> {{$row->SoLuong}} người</p>
                                </td>

                                <td>
                                    <p><b>Công việc:</b> {{$row->MotaCV}}</p>
                                    <p><b>Nơi làm:</b> {{$row->NoiLam}}</p>
                                </td>

                                <td>
                                    <p><b>Lương CB:</b> {{$row->Luong}}</p>
                                    <p><b>Quyền lợi:</b> {{$row->QuyenLoi}}</p>
                                </td>

                                <td>
                                    <a href="javascript:void(0);" id="{{$row->MaPhieuTD}}" class="red tuyendung_delete" title="Xoá">
                                        <i class="fal fa-trash-alt bigger-120" style="padding-top: 10px;"></i>
                                    </a>
                                </td>
                            </tr>

                            <!--Xem chi tiết-->
                            <div class="modal fade" id="model-{{$id}}">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="widget-box">
                                        <div class="table-header text-uppercase">
                                            <button type="button" class="close" data-dismiss="modal" title="Đóng">
                                                <i class="fal fa-times white" style="padding-top: 5px;"></i>
                                            </button>
                                            Chi tiết tuyển dụng
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="space visible-xs"></div>
                                                        <div class="profile-user-info profile-user-info-striped">
                                                            <div class="Table" role="grid">

                                                                <div class="Heading" role="columnheader">
                                                                    <div class="Cell">
                                                                        <p>Thời gian</p>
                                                                    </div>
                                                                    <div class="Cell" role="columnheader">
                                                                        <p>Yêu cầu</p>
                                                                    </div>
                                                                    <div class="Cell" role="columnheader">
                                                                        <p>Hồ sơ</p>
                                                                    </div>
                                                                </div>

                                                                <div class="Row" role="row">
                                                                    <div class="Cell" role="gridcell">
                                                                        <p>{{$row->ThoiGian}}</p>
                                                                    </div>
                                                                    <div class="Cell" role="gridcell">
                                                                        <p>{{$row->YeuCau}}</p>
                                                                    </div>
                                                                    <div class="Cell" role="gridcell">
                                                                        <p> {{$row->HoSo}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-sm btn-danger fa-pull-right" data-dismiss="modal">
                                                <i class="ace-icon fa fa-times"></i> Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!--Thêm & Chỉnh sửa-->

    <div class="modal" id="tuyendung_dialog_add">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="widget-box">
                <div class="widget-header">
                    <h4 class="widget-title" id="widget-title-tuyendung">Đăng tuyển dụng</h4>
                    <span class="close close_add_tuyendung" style="font-size: 12pt; padding-top: 10px; padding-right: 10px; color: #ff4871;" data-dismiss="modal">Đóng</span>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <form method="post" id="form_tuyendung_add">
                            {{csrf_field()}}
                            <input type="hidden" name="maphieu" id="maphieu"/>
                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="tenphieu" id="tenphieu" placeholder="Tiêu đề" maxlength="150" required autofocus/>
                                    <i class="ace-icon fal fa-pencil-square-o"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="noilam" id="noilam" placeholder="Nơi làm việc" maxlength="100" required/>
                                    <i class="ace-icon fal fa-map-marked-alt"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="soluong" id="soluong" placeholder="Số lượng tuyển" maxlength="2" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                                    <i class="ace-icon fal fa-users"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="hoso" id="hoso" placeholder="Hồ sơ bao gồm" maxlength="150" required/>
                                    <i class="ace-icon fal fa-id-card"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <textarea id="yeucau" name="yeucau" class="autosize-transition form-control" placeholder="Yêu cầu thêm khi tuyển" required></textarea>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <textarea id="mota" name="mota" class="autosize-transition form-control" placeholder="Mô tả công việc" required></textarea>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="thoigian" id="thoigian" placeholder="Thời gian làm việc" maxlength="150" required/>
                                    <i class="ace-icon fal fa-calendar-star"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <input type="text" class="form-control" name="luong" id="luong" placeholder="Lương" maxlength="150" required/>
                                    <i class="ace-icon fal fa-money-bill"></i>
                                </span>
                            </label>

                            <label class="block clearfix">
                                <span class="block input-icon input-icon-right">
                                    <textarea id="quyenloi" name="quyenloi" class="autosize-transition form-control" placeholder="Quyền lợi khi làm việc" required></textarea>
                                </span>
                            </label>

                            <div class="clearfix">
                                <input type="submit" class="width-30 pull-right btn btn-sm btn-success" name="submit" id="action" value="Add"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection