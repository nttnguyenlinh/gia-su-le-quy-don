<?php
//    if(isset($_SERVER['HTTPS']))
//        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
//    else
//        $protocol = 'http';
//
//    $geturl = $protocol . "://" . $_SERVER['HTTP_HOST'];
?>

<!DOCTYPE html>
<html lang="vi">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-language" content="vi" />
    <title>@yield('title') - Gia Sư Lê Quý Đôn </title>
    <meta name="abstract" content="Gia Sư Lê Quý Đôn">
    <meta http-equiv="content-language" content="vi" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon image_src" href="/favicon.ico">

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="{{URL::asset('tt/css/bootstrap.min.css?v=4.0.1')}}"/>
    <link rel="stylesheet" href="{{URL::asset('tt/font-awesome/css/all.min.css?v=4.0.1')}}"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="{{URL::asset('tt/css/ace.min.css?v=4.0.1')}}" />

    <!--[if lte IE 9]-->
    <link rel="stylesheet" href="{{URL::asset('tt/css/ace-part2.min.css?v=4.0.1')}}"/>
    <![endif]-->

    <!--[if lte IE 9]-->
    <link rel="stylesheet" href="{{URL::asset('tt/css/ace-ie.min.css?v=4.0.1')}}"/>
    <![endif]-->

    <link rel="stylesheet" href="{{URL::asset('tt/chosen_1.8.7/chosen.min.css?v=4.0.1')}}" />

    <link rel="stylesheet" href="{{URL::asset('tt/css/notification.css?v=4.0.1')}}"/>

    <link rel="stylesheet" href="{{URL::asset('tt/toastr_2.1.3/toastr.min.css?v=4.0.1')}}"/>

    <link rel="stylesheet" href="{{URL::asset('tt/jquery-simpleCheck/css/simpleCheck.min.css?v=4.0.1')}}">
    <script src="{{URL::asset('tt/js/jquery-3.3.1.min.js?v=4.0.1')}}" crossorigin="anonymous"></script>

    <script src="{{URL::asset('tt/jquery-simpleCheck/js/simpleCheck.min.js?v=4.0.1')}}"></script>

</head>

<body class="no-skin" style="font-family: Arial, Helvetica, sans-serif;">
<div id="navbar" class="navbar navbar-default ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
            <a href="/" class="navbar-brand" style="font-size: 14pt; padding-top: 15px;">GIA SƯ LÊ QUÝ ĐÔN</a>
        </div>
        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">
                @if(Auth::check())
                    <li class="light-blue dropdown-modal">
                        <a data-toggle="dropdown" class="dropdown-toggle">
                            @if(!empty(Auth::user()->avatar))
                                <img src="{{Auth::user()->avatar}}?v=4.0.1" class="nav-user-photo" alt="{{Auth::user()->name}}"/>
                            @else
                                <img src="https://i.imgur.com/3Ox71xn.jpg?v=4.0.1" class="nav-user-photo" alt="{{Auth::user()->name}}"/>
                            @endif

                            <span class="user-info">
                                <small>Xin chào,</small>
                                {{Auth::user()->name}}
                            </span>

                            <i class="nav-menu-icon fal fa-caret-down"></i>
                        </a>

                        <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                            <li>
                                <a href="{{Route('trungtam.profile')}}">
                                    <i class="nav-menu-icon fal fa-user"></i>
                                    Cá nhân
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="{{Route('trungtam.logout')}}">
                                    <i class="nav-menu-icon fal fa-power-off"></i>
                                    Đăng xuất
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</div>

<div class="main-container ace-save-state" id="main-container">
    <div id="sidebar" class="sidebar responsive ace-save-state">
        @include('trungtam.nav-list')
        @show
        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fal fa-angle-double-left ace-save-state"
               data-icon1="ace-icon fal fa-angle-double-left" data-icon2="ace-icon fal fa-angle-double-right"></i>
        </div>
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            @section('nav-content')
            @show
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        @section('page-content')
                        @show
                    </div>
                </div>
            </div>
        </div>
    </div>


    <a href="javascript:void(0);" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="nav-menu-icon fal fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div>

<!--[if !IE]> -->
<script src="{{URL::asset('tt/js/jquery-2.1.4.min.js?v=4.0.1')}}"></script>
<!-- <![endif]-->

<!--[if IE]-->
<script src="{{URL::asset('tt/js/jquery-1.11.3.min.js?v=4.0.1')}}" crossorigin="anonymous"></script>
<![endif]-->

<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='{{URL::asset("tt/js/jquery.mobile.custom.min.js?v=4.0.1")}}'>" + "<" + "/script>");
</script>
<script src="{{URL::asset('tt/js/bootstrap.min.js?v=4.0.1')}}"></script>
<script src="{{URL::asset('tt/js/jquery.dataTables.min.js?v=4.0.1')}}"></script>
<script src="{{URL::asset('tt/js/jquery.dataTables.bootstrap.min.js?v=4.0.1')}}"></script>
<script src="{{URL::asset('tt/js/dataTables.select.min.js?v=4.0.1')}}"></script>
<script src="{{URL::asset('tt/js/jquery-ui.1.12.1.js?v=4.0.1')}}"></script>
<script src="{{URL::asset('tt/js/jquery.mask.min.js?v=4.0.1')}}"></script>
<script src="{{URL::asset('tt/js/jquery.validate.min.js?v=4.0.1')}}"></script>

<script src="{{URL::asset('tt/chosen_1.8.7/chosen.jquery.min.js?v=4.0.1')}}"></script>
<script src="{{URL::asset('tt/toastr_2.1.3/toastr.min.js?v=4.0.1')}}"></script>

@section('script-footer')
@show


<script type="text/javascript" src="{{URL::asset('tt/js/footer.js?v=4.0.1')}}" crossorigin="anonymous"></script>
<!-- ace scripts -->
<script src="{{URL::asset('tt/js/ace-elements.min.js?v=4.0.1')}}"></script>
<script src="{{URL::asset('tt/js/ace.min.js?v=4.0.1')}}"></script>



</body>
</html>
