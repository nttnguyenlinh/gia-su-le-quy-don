@extends('trungtam.master')
@section('title', 'Dashboard')

@section('nav-content')

    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="nav-menu-icon fal fa-home"></i>
                <a href="/">Home</a>
            </li>

            <li><a href="{{Route('trungtam')}}">Dashboard</a></li>
            <li class="active"><a href="{{Route('trungtam.giasu')}}">Gia sư</a></li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </div>
            @endif

            @if (Session::has('flag'))
                <div class="alert alert-{{Session::get('flag')}}">
                    {{Session::get('message')}}
                </div>
            @endif

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div>
                <table id="table_giasu" class="table table-bordered table-responsive table-hover">
                    <thead>
                        <tr>
                            <th style="text-align: center;">ID</th>
                            <th style="text-align: center;">Họ tên</th>
                            <th style="text-align: center;">Liên hệ</th>
                            <th style="text-align: center; width: 80px;"><i class="far fa-lock"></i></th>
                            <th style="text-align: center;">#</th>
                        </tr>

                    </thead>

                    <tbody style="text-align: center;">
                    @foreach($giasu as $id => $row)
                        <tr>
                            <td>
                                <button class="btn-id" data-toggle="modal" data-target="#model-{{$id}}" data-backdrop="false" title="Xem chi tiết">{{$row->MaGiaSu}}</button>
                            </td>

                            <td>
                                <p>{{$row->HoTen}}</p>

                                @if($row->Status)
                                    <p class="label label-success">Hoạt động</p>
                                @else
                                    <p class="label label-danger">Đang khoá</p>
                                @endif
                            </td>

                            <td>
                                <p><a href="tel:{{$row->SoDT}}">{{$row->SoDT}}</a></p>
                                <p><a href="mailto:{{$row->Email}}">{{$row->Email}}</a></p>
                            </td>

                            <td>
                                <div id="switch" style="padding-top: 10px;">
                                    @if($row->Status)
                                        <input type="checkbox" name="status" class="simpleCheck switch giasu_status" id="{{$row->MaGiaSu}}"  data-status="enable" title="Đang hoạt động"/>
                                    @else
                                        <input type="checkbox" name="status" class="simpleCheck switch simpleCheck-checked giasu_status" id="{{$row->MaGiaSu}}"  data-status="disable" title="Đang khoá"/>
                                    @endif
                                </div>
                            </td>

                            <td>
                                <a href="javascript:void(0);" id="{{$row->MaGiaSu}}" class="red giasu_delete" title="Xoá gia sư">
                                    <i class="fal fa-trash-alt bigger-150" style="padding-top: 10px;"></i>
                                </a>
                            </td>
                        </tr>

                        <div id="model-{{$id}}" class="modal fade" tabindex="-1">
                            <div class="col-sm-6 col-sm-offset-3">
                                <div class="widget-box">
                                    <div class="table-header text-uppercase">
                                        <button type="button" class="close" data-dismiss="modal" title="Đóng">
                                           <i class="fal fa-times white" style="padding-top: 5px;"></i>
                                        </button>
                                       Gia sư {{$row->HoTen}} ({{$row->NgaySinh}})
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-3">
                                                    <div class="text-center">
                                                        @if(!empty($row->AnhThe))
                                                            <img src="{{$row->AnhThe}}?v=4.0.1" class="thumbnail inline" alt="{{$row->HoTen}}" style="width: 170px; height: 200px; padding: 0px; margin-bottom: 0px;"/>
                                                        @else
                                                            <img src="https://i.imgur.com/3Ox71xn.jpg?v=4.0.1" class="thumbnail inline" alt="{{$row->HoTen}}" style="width: 170px; height: 2000px; padding: 0px; margin-bottom: 0px;"/>
                                                        @endif

                                                        <div class="width-65 label label-primary label-xlg arrowed-in arrowed-in-right">
                                                            <i class="far fa-calendar-check bolder"></i>
                                                            <span class="white">{{date_format(date_create($row->created_at), "d-m-Y")}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-9">
                                                    <div class="space visible-xs"></div>
                                                    <div class="profile-user-info profile-user-info-striped">
                                                        <div class="Table" role="grid">

                                                            <div class="Heading" role="columnheader">
                                                                <div class="Cell">
                                                                    <p>Quê quán / Địa chỉ</p>
                                                                </div>
                                                                <div class="Cell" role="columnheader">
                                                                    <p>Trình độ / Học vấn</p>
                                                                </div>
                                                                <div class="Cell" role="columnheader">
                                                                    <p>Nhận dạy</p>
                                                                </div>
                                                            </div>

                                                            <div class="Row" role="row">
                                                                <div class="Cell" role="gridcell">
                                                                    <p><b>Quê Quán:</b> {{$row->NguyenQuan}}</p>
                                                                    <p><b>Giọng nói:</b> {{$row->GiongNoi}}</p>
                                                                    <p><b>CMND:</b> {{$row->CMND}}</p>
                                                                    <p><b>Nơi ở:</b> {{$row->DiaChi}}</p>
                                                                </div>
                                                                <div class="Cell" role="gridcell">
                                                                    <p><b>Trường ĐT:</b> {{$row->TruongDaoTao}}</p>
                                                                    <p><b>Ngành học:</b> {{$row->NganhHoc}} </p>
                                                                    <p><b>Niên khoá:</b> {{$row->NamTN - 4}} - {{$row->NamTN}}
                                                                    <p><b>Trình độ:</b> {{$row->TrinhDo}}</p>
                                                                    <p><b>Giáo viên:</b> {{$row->LoaiGiaoVien}}</p>
                                                                </div>
                                                                <div class="Cell" role="gridcell">
                                                                    <p>
                                                                        <b>Môn dạy: </b>
                                                                        @foreach($getMonHoc as $mon)
                                                                            @foreach(explode(",",$row->MonDay) as $item)
                                                                                @if($item == $mon->MaMon)
                                                                                    {{$mon->TenMon}},
                                                                                @endif
                                                                            @endforeach
                                                                        @endforeach
                                                                    </p>

                                                                    <p>
                                                                        <b>Lớp dạy: </b>
                                                                        @foreach($getLopHoc as $lop)
                                                                            @foreach(explode(",",$row->LopDay) as $item)
                                                                                @if($item == $lop->MaLop)
                                                                                    {{$lop->TenLop}},
                                                                                @endif
                                                                            @endforeach
                                                                        @endforeach
                                                                    </p>

                                                                    <p>
                                                                        <b>Nơi dạy:</b>
                                                                        @foreach($getKhuVuc as $khuvuc)
                                                                            @foreach(explode(",",$row->KhuVuc) as $item)
                                                                                @if($item == $khuvuc->MaKV)
                                                                                    {{$khuvuc->TenKV}},
                                                                                @endif
                                                                            @endforeach
                                                                        @endforeach
                                                                    </p>

                                                                    <p><b>Ưu điểm:</b> {{$row->UuDiem}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-sm btn-danger fa-pull-right" data-dismiss="modal">
                                            <i class="ace-icon fa fa-times"></i> Đóng
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection