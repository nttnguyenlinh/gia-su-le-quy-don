@extends('trungtam.master')
@section('title', 'Dashboard')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="/">Home</a>
            </li>

            <li><a href="{{Route('trungtam')}}">Dashboard</a></li>
            <li class="active"><a href="{{Route('trungtam.hocphi')}}">Học phí tham khảo</a></li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </div>
            @endif

            @if (Session::has('flag'))
                <div class="alert alert-{{Session::get('flag')}}">
                    {{Session::get('message')}}
                </div>
            @endif

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>

            <form method="post" action="{{route('trungtam.hocphi_edit')}}">
                {{csrf_field()}}
                <div class="col-lg-6 col-md-6">
                    <div class="widget-box widget-color-green">
                        <div class="widget-header">
                            <h4 class="widget-title">2 BUỔI / TUẦN</h4>
                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fal fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <table class="table1 table-bordered table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col1" rowspan="2" style="text-align: center;">KHỐI LỚP</th>
                                            <th>Sinh viên</th>
                                            <th>Giáo viên</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($hocphi as $row)
                                            @if($row['SoBuoi'] == 2)
                                                <tr>
                                                    <td class="col1">{{$row->KhoiLop}}</td>
                                                    <td>
                                                        <input type="hidden" name="hp" value="{{$row->MaHP}}"/>
                                                        <input type="text" maxlength="15" name="sv{{$row->MaHP}}" value="{{$row->SinhVien}}" required/>
                                                    </td>
                                                    <td>
                                                        <input type="text" maxlength="15" name="gv{{$row->MaHP}}" value="{{$row->GiaoVien}}" required/>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="widget-box widget-color-red">
                        <div class="widget-header">
                            <h4 class="widget-title">3 BUỔI / TUẦN</h4>
                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fal fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <table class="table2 table-bordered table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col1" rowspan="2" style="text-align: center;">KHỐI LỚP</th>
                                            <th>Sinh viên</th>
                                            <th>Giáo viên</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($hocphi as $row)
                                            @if($row['SoBuoi'] == 3)
                                                <tr>
                                                    <td class="col1">{{$row->KhoiLop}}</td>
                                                    <td>
                                                        <input type="hidden" name="hp" value="{{$row->MaHP}}"/>
                                                        <input type="text" maxlength="15" name="sv{{$row->MaHP}}" value="{{$row->SinhVien}}" required/>
                                                    </td>
                                                    <td>
                                                        <input type="text" maxlength="15" name="gv{{$row->MaHP}}" value="{{$row->GiaoVien}}" required/>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="widget-box widget-color-green">
                        <div class="widget-header">
                            <h4 class="widget-title">4 BUỔI / TUẦN</h4>
                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fal fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <table class="table1 table-bordered table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col1" rowspan="2" style="text-align: center;">KHỐI LỚP</th>
                                            <th>Sinh viên</th>
                                            <th>Giáo viên</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($hocphi as $row)
                                            @if($row['SoBuoi'] == 4)
                                                <tr>
                                                    <td class="col1">{{$row->KhoiLop}}</td>
                                                    <td>
                                                        <input type="hidden" name="hp" value="{{$row->MaHP}}"/>
                                                        <input type="text" maxlength="15" name="sv{{$row->MaHP}}" value="{{$row->SinhVien}}" required/>
                                                    </td>
                                                    <td>
                                                        <input type="text" maxlength="15" name="gv{{$row->MaHP}}" value="{{$row->GiaoVien}}" required/>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="widget-box widget-color-red">
                        <div class="widget-header">
                            <h4 class="widget-title">5 BUỔI / TUẦN</h4>
                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fal fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <table class="table2 table-bordered table-responsive table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col1" rowspan="2" style="text-align: center;">KHỐI LỚP</th>
                                            <th>Sinh viên</th>
                                            <th>Giáo viên</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($hocphi as $row)
                                            @if($row['SoBuoi'] == 5)
                                                <tr>
                                                    <td class="col1">{{$row->KhoiLop}}</td>
                                                    <td>
                                                        <input type="hidden" name="hp" value="{{$row->MaHP}}"/>
                                                        <input type="text" maxlength="15" name="sv{{$row->MaHP}}" value="{{$row->SinhVien}}" required/>
                                                    </td>
                                                    <td>
                                                        <input type="text" maxlength="15" name="gv{{$row->MaHP}}" value="{{$row->GiaoVien}}" required/>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

               <br><br>
                <div style="width: 170px; margin: auto;">
                    <button type="submit" class="btn btn-success" style="font-weight: bold;">Cập nhật học phí</button>
                </div>
            </form>
        </div>
    </div>
@endsection
