@extends('trungtam.master')
@section('title', 'Dashboard')
@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="/">Home</a>
            </li>

            <li><a href="{{Route('trungtam')}}">Dashboard</a></li>
            <li class="active"><a href="{{Route('trungtam.giasu_sdt_password')}}">Thay đổi SĐT - Khôi phục mật khẩu</a></li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </div>
            @endif

            @if (Session::has('flag'))
                <div class="alert alert-{{Session::get('flag')}}">
                    {{Session::get('message')}}
                </div>
            @endif

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="widget-box widget-color-blue">
                    <div class="widget-header">
                        <h4 class="widget-title">Thay Đổi SĐT</h4>
                        <div class="widget-toolbar">
                            <a href="javascript:void(0);" data-action="collapse">
                                <i class="ace-icon fal fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <form method="post" action="{{route('trungtam.giasu_sdt_password_change')}}" id="form_giasu_sdt_password">
                                {{csrf_field()}}
                                <div>
                                    <p class="bolder">Thay đổi cho</p>
                                    <select class="chosen-select" name="id" data-placeholder="Chọn gia sư cần đổi" required>
                                        <option value=""></option>
                                        @foreach($giasu as $row)
                                            <option value="{{$row->MaGiaSu}}">{{$row->MaGiaSu}} - {{$row->HoTen}}</option>
                                        @endforeach
                                    </select>

                                    <p class="bolder" style="padding-top: 20px;">Nhập số điện thoại</p>
                                    <div class="input-group">
                                        <input class="form-control" style="border: #057bbe 1px solid; color: #057bbe;" type="text" id="phone" name="phone" placeholder="0984513523" class="in_405" maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                                        <span class="input-group-addon" style="border: #057bbe 1px solid; color: #057bbe;"><i class="ace-icon fal fa-phone"></i></span>
                                    </div>
                                </div>
                                <br>
                                <div class="center">
                                    <input type="submit" name="thaydoi" class="btn btn-sm btn-primary" value="Thay đổi"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="widget-box widget-color-blue">
                    <div class="widget-header">
                        <h4 class="widget-title">Khôi phục mật khẩu</h4>
                        <div class="widget-toolbar">
                            <a href="javascript:void(0);" data-action="collapse">
                                <i class="ace-icon fal fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main">
                            <form method="post" action="{{route('trungtam.giasu_sdt_password_change')}}" id="form_giasu_sdt_password">
                                {{csrf_field()}}
                                <div>
                                    <p class="bolder">Khôi phục cho</p>
                                    <select class="chosen-select" name="id" data-placeholder="Chọn gia sư cần khôi phục" required>
                                        <option value=""></option>
                                        @foreach($giasu as $row)
                                            <option value="{{$row->MaGiaSu}}">{{$row->MaGiaSu}} - {{$row->HoTen}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <br>
                                <div class="center">
                                    <input type="submit" name="pass" class="btn btn-sm btn-primary" value="Reset"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </div>
@endsection