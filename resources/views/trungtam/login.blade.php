<!DOCTYPE html>
<html lang="vi">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-language" content="vi" />
    <title>Đăng nhập</title>
    <meta name="abstract" content="Gia Sư Lê Quý Đôn">
    <meta http-equiv="content-language" content="vi" />
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon image_src" href="/favicon.ico">

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="{{URL::asset('tt/css/bootstrap.min.css?v=4.0.1')}}"/>

    <!-- ace styles -->
    <link rel="stylesheet" href="{{URL::asset('tt/css/ace.min.css?v=4.0.1')}}"/>

    <!--[if lte IE 9]-->
    <link rel="stylesheet" href="{{URL::asset('tt/css/ace-part2.min.css?v=4.0.1')}}"/>
    <![endif]-->

    <!--[if lte IE 9]-->
    <link rel="stylesheet" href="{{URL::asset('tt/css/ace-ie.min.css?v=4.0.1')}}"/>
    <![endif]-->

    <link rel="stylesheet" href="{{URL::asset('tt/css/notification.css?v=4.0.1')}}"/>
</head>

<body class="login-layout light-login">
    <div class="main-container">
        <div class="main-content">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1" type="margin-top: 150px;">
                    <div class="login-container">
                        <div class="center">
                            <h1>
                                <img src="{{URL::asset('images/logo_2.png?v=4.0.1')}}" width="64px" height="64px"/>
                                <span class="red">Gia Sư</span>
                                <span class="white" id="id-text2 blue">Lê Quý Đôn</span>
                            </h1>
                        </div>

                        <div class="space-6"></div>

                        <div class="position-relative">
                            <div id="login-box" class="login-box visible widget-box no-border">
                                <div class="widget-body">
                                    <div class="widget-main">
                                        <h4 class="header blue lighter bigger">
                                            <i class="ace-icon fa fa-key"></i>
                                            <span style="font-size:18pt;font-family:Calibri, Bookman, Tahoma, Verdana;">
                                                Đăng nhập hệ thống
                                            </span>
                                        </h4>

                                        <div class="space-6"></div>

                                        @if(Session::has('flag'))
                                            <div class="alert alert-{{Session::get('flag')}}">{{Session::get('message')}}</div>
                                        @endif

                                        <form id="form-login" action="{{Route('trungtam.postlogin')}}" method="post">
                                            {{csrf_field()}}
                                            <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="text" class="form-control" name="username" id="username" placeholder="Tài khoản" required autofocus>
                                                        <i class="ace-icon fa fa-user"></i>
                                                    </span>
                                            </label>

                                            <label class="block clearfix">
                                                    <span class="block input-icon input-icon-right">
                                                        <input type="password" class="form-control" name="password" id="password" placeholder="Mật khẩu" required/>
                                                        <i class="ace-icon fa fa-lock"></i>
                                                    </span>
                                            </label>

                                            <div class="space"></div>

                                            <div class="clearfix">
                                                <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                    <i class="ace-icon fa fa-key"></i>
                                                    <span>Đăng nhập</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--[if !IE]> -->
    <script src="{{URL::asset('tt/js/jquery-2.1.4.min.js?v=4.0.1')}}"></script>
    <!-- <![endif]-->

    <!--[if IE]-->
    <script src="{{URL::asset('tt/js/jquery-1.11.3.min.js?v=4.0.1')}}"></script>
    <![endif]-->

    <script type="text/javascript">
        if ('ontouchstart' in document.documentElement) document.write("<script src='{{URL::asset("tt/js/jquery.mobile.custom.min.js?v=4.0.1")}}'>" + "<" + "/script>");
    </script>

    <script src="{{URL::asset('tt/js/bootstrap.min.js?v=4.0.1')}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js?v=4.0.1"></script>--}}

    <script src="{{URL::asset('tt/js/jquery.validate.min.js?v=4.0.1')}}"></script>

    <script src="{{URL::asset('tt/js/footer.js?v=4.0.1')}}"></script>
</body>
</html>
