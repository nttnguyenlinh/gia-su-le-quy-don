@extends('trungtam.master')
@section('title', 'Dashboard')

@section('nav-content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="nav-menu-icon fal fa-home"></i>
            <a href="/">Home</a>
        </li>

        <li><a href="{{Route('trungtam')}}">Dashboard</a></li>
        <li class="active"><a href="{{Route('trungtam.profile')}}">Cá nhân</a></li>
    </ul>
</div>
@endsection

@section('page-content')

    @if(count($errors)>0)
        <div class="alert alert-danger" style="font-family: sans-serif, arial, georgia, verdana; font-size: 10pt;">
            @foreach($errors->all() as $err)
                <li>{{$err}}</li>
            @endforeach
        </div>
    @endif

    @if (Session::has('flag'))
        <div class="alert alert-{{Session::get('flag')}}">
            {{Session::get('message')}}
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div id="user-profile-1" class="user-profile row">
                <div class="col-xs-12 col-sm-3 center" style="margin-top: 30px;">
                    <span class="profile-picture">
                        @if(!empty($info->avatar))
                            <img class="editable img-responsive" alt="" src="{{$info->avatar}}?v=4.0.1" style="width:200px;height:200px;"/>
                        @else
                            <img class="editable img-responsive" alt="" src="https://i.imgur.com/3Ox71xn.jpg?v=4.0.1" style="width:200px;height:200px;"/>
                        @endif
                    </span>

                    <div class="space-4"></div>

                    @if($info->Status)
                        <div class="width-50 label label-info label-xlg arrowed-in arrowed-in-right">
                            <i class="ace-icon fa fa-circle light-green"></i>
                            <span class="white">Hoạt động</span>
                        </div>
                    @else
                        <div class="width-50 label label-danger label-xlg arrowed-in arrowed-in-right">
                            <i class="ace-icon fa fa-circle light-red"></i>
                            <span class="white">Bị khoá</span>
                        </div>
                    @endif

                    <div class="space-16"></div>
                </div>

                <div class="col-xs-12 col-sm-9">
                    <div class="widget-box transparent">
                        <div class="widget-header widget-header-flat">
                            <h4 class="widget-title blue smaller">
                                <i class="ace-icon fal fa-user blue"></i>
                                Thông tin cá nhân
                            </h4>

                            <div class="widget-toolbar">
                                <a href="javascript:void(0);" data-action="collapse">
                                    <i class="ace-icon fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <div class="space-10"></div>
                                <form method="post" id="form_profile_update" action="{{Route('trungtam.profile_edit')}}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" class="form-control input-sm" value="{{$info->id}}">
                                    <div class="profile-user-info profile-user-info-striped">
                                        <div class="profile-info-row">
                                            <div class="profile-info-name">Họ tên</div>
                                            <div class="profile-info-value">
                                                <input type="text" name="name" class="form-control input-sm" value="{{$info->name}}"  required maxlength="30">
                                            </div>
                                        </div>

                                        <div class="profile-info-row">
                                            <div class="profile-info-name">Email</div>
                                            <div class="profile-info-value">
                                                <input type="email" name="email" class="form-control input-sm" value="{{$info->email}}" required>
                                            </div>
                                        </div>

                                        <div class="profile-info-row">
                                            <div class="profile-info-name">Hình đại diện</div>
                                            <div class="profile-info-value">
                                                <input type="file" name="avatar" id="avatar" class="form-control input-sm" accept="image/*">
                                            </div>
                                        </div>

                                        <div class="profile-info-row">
                                            <div class="profile-info-name">Tài khoản</div>
                                            <div class="profile-info-value">
                                                <input type="text" name="username" readonly class="form-control input-sm" value="{{$info->username}}">
                                            </div>
                                        </div>

                                        <div class="profile-info-row">
                                            <div class="profile-info-name">Ngày tham gia</div>
                                            <div class="profile-info-value">
                                                <span name="created_at">{{date_format(date_create($info->created_at), "d-m-Y")}}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="space-4"></div>

                                    <div class="col-md-offset-3 col-md-6" style="text-align: center;">
                                        <button class="btn btn-info" type="submit">
                                            <i class="ace-icon fa fa-check bigger-110"></i>Cập nhật
                                        </button>
                                        <button class="btn" type="reset">
                                            <i class="ace-icon fa fa-undo bigger-110"></i>Clear
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-9" style="float:right; margin-top: 20px;">
                <div class="widget-box transparent">
                    <div class="widget-header widget-header-flat">
                        <h4 class="widget-title blue smaller">
                            <i class="ace-icon fal fa-key blue"></i>
                            Thay đổi mật khẩu
                        </h4>

                        <div class="widget-toolbar">
                            <a href="javascript:void(0);" data-action="collapse">
                                <i class="ace-icon fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="widget-main no-padding">
                            <div class="space-10"></div>
                            <form method="post" id="form_profile_change" action="{{route('trungtam.profile_password')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="id" class="form-control input-sm" value="{{$info->id}}">
                                <div class="profile-user-info profile-user-info-striped">
                                    <div class="profile-info-row">
                                        <div class="profile-info-name">M.khẩu hiện tại</div>
                                        <div class="profile-info-value">
                                            <input type="password" name="current_password" id="current_password" class="form-control input-sm" required>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Mật khẩu mới</div>
                                        <div class="profile-info-value">
                                            <input type="password" name="new_password"  id="new_password" class="form-control input-sm" required>
                                        </div>
                                    </div>

                                    <div class="profile-info-row">
                                        <div class="profile-info-name">Xác nhận M.k</div>
                                        <div class="profile-info-value">
                                            <input type="password" name="confirm_password" id="confirm_password" class="form-control input-sm" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="space-4"></div>

                                <div class="col-md-offset-3 col-md-6 text-center">
                                    <button class="btn btn-info" type="submit" name="change">
                                        <i class="ace-icon fa fa-check bigger-110"></i>Thay đổi
                                    </button>
                                    <button class="btn" type="reset">
                                        <i class="ace-icon fa fa-undo bigger-110"></i>Clear
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('script-footer')
    <script>
        $("#form_profile_update").ready(function () {
            //Chọn ảnh đại diện
            $('#avatar').ace_file_input({
                no_file: 'Chưa chọn ảnh',
                btn_choose: 'Chọn',
                btn_change: 'Đổi',
                onchange: true,
                thumbnail: true,
                whitelist: 'gif|png|jpg|jpeg',
                blacklist: 'exe|php|js'
            });

            document.getElementById("avatar").onchange = function () {
                var reader = new FileReader();
                if (this.files[0].size > 2097152) {
                    alert("Vui lòng chọn hình nhỏ hơn 2MB!");
                    $("#avatar").attr("src", "blank");
                    $('#avatar').wrap('<form>').closest('form').get(0).reset();
                    $('#avatar').unwrap();
                    return false;
                }
                if (this.files[0].type.indexOf("image") == -1) {
                    alert("Vui lòng chọn tệp hình ảnh!");
                    $("#avatar").attr("src", "blank");
                    $('#avatar').wrap('<form>').closest('form').get(0).reset();
                    $('#avatar').unwrap();
                    return false;
                }
                reader.onload = function (e) {
                    // get loaded data and render thumbnail.
                    document.getElementById("avatar").src = e.target.result;
                    $("#avatar").show();
                };

                // read the image file as a data URL.
                reader.readAsDataURL(this.files[0]);
            };
        });
    </script>
@endsection
