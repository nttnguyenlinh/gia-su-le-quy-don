@extends('trungtam.master')
@section('title', 'Dashboard')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="nav-menu-icon fal fa-home"></i>
                <a href="/">Home</a>
            </li>

            <li><a href="{{Route('trungtam')}}">Dashboard</a></li>

            <li><a href="{{Route('trungtam.nhanlop')}}">Danh sách nhận dạy</a></li>
        </ul>
    </div>
@endsection

@section('page-content')
   <div class="row">
        <div class="col-xs-12">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </div>
            @endif

            @if (Session::has('flag'))
                <div class="alert alert-{{Session::get('flag')}}">
                    {{Session::get('message')}}
                </div>
            @endif

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div>
                <table id="table_nhanday" class="table table-bordered table-responsive table-hover">
                    <thead>
                        <tr>
                            <th style="text-align: center;">ID</th>
                            <th style="text-align: center;">Gia Sư</th>
                            <th style="text-align: center;">Thông Tin</th>
                            <th style="text-align: center;">Thời gian</th>
                            <th style="text-align: center;">Thanh Toán</th>
                            <th style="text-align: center;">Trạng Thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($nhanlop as $id => $row)
                            <tr>
                                <td>
                                    <button class="btn-id" style="margin-top: 40px;">{{$row->MaPhieuNhan}}</button>
                                </td>

                                <td style="padding-top: 20px;">
                                    <p><b>Mã số:</b> {{$row->MaGiaSu}}</p>
                                    <p><b>Họ tên:</b> {{$row->HoTen}}</p>
                                    <p><b>Hiện là:</b> <span style="color: #679312; text-decoration: none; background:transparent; border:#679312 solid 1px; padding: 2px;">{{$row->TrinhDo}}</span></p>
                                </td>

                                <td style="padding-top: 20px;">
                                    <p><b>Mã lớp:</b> {{$row->MaPhieuMo}}</p>
                                    <p><b>Lớp:</b> {{$row->LopDay}}</p>
                                    <p><b>Môn:</b> {{$row->MonDay}}</p>
                                </td>

                                <td>
                                    <p><b>Ngày đk:</b> {{date_format(date_create($row->created_at), "d-m-Y")}}</p>
                                    <p><b>Ngày dạy:</b> {{$row->NgayDay}}</p>
                                    <p><b>Giờ dạy:</b> {{$row->GioDay}}</p>
                                    <p><b>Ghi chú:</b> {{$row->NoiDungThem}}</p>
                                </td>

                                <td>
                                    @if($row->HinhThucNhan == 0)
                                        <p><b>Hình thức:</b> Chuyển khoản</p>
                                    @else
                                        <p><b>Hình thức:</b> Tới trung tâm</p>
                                    @endif

                                    <p><b>Mức phí:</b> {{$row->LePhi}}<sup>%</sup></p>
                                    <p><b>Gia sư nhận:</b> <span style="color: #057bbe;">{{$row->Luong}} <sup>đ</sup>/tháng</span></p>
                                        <p><b>Hoa hồng:</b> <span style="color: #057bbe;">{{number_format(((float)(((float)$row->Luong * (float)$row->LePhi)/100) * 1000000), 0, '', '.')}}<sup>đ</sup></span></p>

                                </td>

                                <td style="padding-top: 30px;">
                                    @if($row->Status == 0)
                                        <span class="label label-sm block status_0">
                                            CHỜ DUYỆT
                                        </span>
                                    @elseif($row->Status == 1)
                                        <span class="label label-sm block status_1">
                                            XEM XÉT
                                        </span>
                                    @elseif($row->Status == 2)
                                        <span class="label label-sm block status_2">
                                            ĐỦ ĐK
                                        </span>
                                    @elseif($row->Status == 3)
                                        <span class="label label-sm block status_3">
                                            ĐÃ NHẬN
                                        </span>
                                    @elseif($row->Status == 4)
                                        <span class="label label-sm block status_4">
                                            KHÔNG ĐẠT
                                        </span>
                                    @endif

                                    <button class="btn btn-xs btn-primary btn-block" style="margin-top: 5px" data-toggle="modal" data-target="#model-{{$id}}" title="Bấm vào đây để thay đổi trạng thái">
                                        <i class="ace-icon fa fa-refresh"></i>Thay đổi
                                    </button>
                                </td>
                            </tr>

                            <div class="modal" id="model-{{$id}}" role="dialog">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4 class="widget-title">Thay đổi tình trạng</h4>
                                            <span class="close" style="font-size: 12pt; padding-top: 10px; padding-right: 10px; color: #ff4871;" data-dismiss="modal">&times; Đóng</span>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <form method="post" action="{{route('trungtam.nhanlop_status')}}">
                                                    {{csrf_field()}}
                                                    <div>
                                                        <input type="hidden" name="maphieunhan" value="{{$row->MaPhieuNhan}}"/>
                                                        <input type="hidden" name="maphieumo" value="{{$row->MaPhieuMo}}"/>
                                                        <label for="form-field-select-1">Lựa chọn tình trạng?</label>
                                                        <?php
                                                            $status_array =  array(0=>'Chờ duyệt', 1=>'Xem xét', 2=>'Đủ điều kiện',
                                                                3=>'Đã nhận', 4=>'Không đạt');
                                                        ?>
                                                        <select name="status" class="form-control" required>
                                                            @foreach($status_array as $key => $value)
                                                                @if($row->Status == $key)
                                                                    <option value="{{$key}}" selected>{{$value}}</option>
                                                                @else
                                                                    <option value="{{$key}}">{{$value}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <br>
                                                    <div class="center">
                                                        <input type="submit" class="btn btn-sm btn-primary" value="Thay đổi"/>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection