@extends('trungtam.master')
@section('title', 'Dashboard')

@section('nav-content')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="nav-menu-icon fal fa-home"></i>
                <a href="/">Home</a>
            </li>

            <li><a href="{{Route('trungtam')}}">Dashboard</a></li>
            <li class="active"><a href="{{Route('trungtam.phieumolop')}}">Danh sách lớp</a></li>
        </ul>
    </div>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-xs-12">
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err)
                        <li>{{$err}}</li>
                    @endforeach
                </div>
            @endif

            @if (Session::has('flag'))
                <div class="alert alert-{{Session::get('flag')}}">
                    {{Session::get('message')}}
                </div>
            @endif

            <div class="clearfix">
                <div class="pull-right tableTools-container"></div>
            </div>
            <div>
                <table id="table-phieumolop" class="table table-bordered table-responsive table-hover">
                    <thead>
                        <tr>
                            <th style="text-align: center;">ID</th>
                            <th style="text-align: center;">ĐK</th>
                            <th style="text-align: center;">Thông Tin</th>
                            <th style="text-align: center;">Thanh Toán</th>
                            <th style="text-align: center;">Ẩn</th>
                            <th style="text-align: center;">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dslop as $id => $row)
                            <tr>
                                <td style="text-align: center; padding-top: 20px;">
                                    <button class="btn-id" data-toggle="modal" data-target="#model-{{$id}}" data-backdrop="false" title="Xem chi tiết mở lớp">{{$row->MaPhieuMo}}</button>
                                </td>

                                <td style="text-align: center; padding-top: 20px;">
                                    <button class="btn-id phieumolop-dangky-detail" id="{{$row->MaPhieuDK}}" title="Xem chi tiết đăng ký">{{$row->MaPhieuDK}}</button>
                                </td>

                                <td>
                                    <p><b>Lớp:</b> {{$row->LopDay}}</p>
                                    <p><b>Môn:</b> {{$row->MonDay}}</p>
                                    <p><b>Ngày mở:</b> {{date_format(date_create($row->created_at), "d-m-Y")}}</p>
                                </td>

                                <td>
                                    <p><b>Lương:</b> {{$row->Luong}}<sup>đ</sup>/tháng</p>
                                    <p><b>Phí:</b> {{$row->LePhi}}%</p>
                                    <p><b>Hoa hồng:</b> {{number_format(((float)(((float)$row->Luong * (float)$row->LePhi)/100) * 1000000), 0, '', '.')}}<sup>đ</sup></p>
                                </td>

                                <td style="text-align: center; padding-top: 20px;">
                                    <div id="switch">
                                        @if($row->Status)
                                            <input type="checkbox" class="simpleCheck switch simpleCheck-checked phieumolop_status" id="{{$row->MaPhieuMo}}"  data-status="enable" title="Lớp đã & đang giao thì sẽ được ẩn"/>
                                        @else
                                            <input type="checkbox" class="simpleCheck switch phieumolop_status" id="{{$row->MaPhieuMo}}"  data-status="disable" title="Lớp chưa giao thì sẽ được hiện"/>
                                        @endif
                                    </div>
                                </td>

                                <td style="text-align: center; padding-top: 20px;">
                                    <div class="hidden-sm hidden-xs btn-group">
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#md2-{{$id}}" data-backdrop="false" class="orange2 phieumolop_edit" title="Chỉnh sửa">
                                            <i class="fal fa-pen-alt bigger-150"></i>
                                        </a>

                                        <span style="margin: 0px 2px;">&nbsp;</span>
                                        <a href="javascript:void(0);" id="{{$row->MaPhieuMo}}" class="red phieumolop_delete" title="Xoá">
                                            <i class="fal fa-trash-alt bigger-150"></i>
                                        </a>
                                    </div>

                                    <div class="hidden-md hidden-lg">
                                        <div class="inline pos-rel">
                                            <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                                <i class="fal fa-plus bigger-120"></i>
                                            </button>

                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                                <li>
                                                    <a href="javascript:void(0);" i data-toggle="modal" data-target="#md2-{{$id}}" data-backdrop="false" class="orange2 phieumolop_edit" title="Chỉnh sửa">
                                                        <i class="fal fa-pen-alt bigger-120"></i>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="javascript:void(0);" id="{{$row->MaPhieuMo}}" class="red phieumolop_delete" title="Xoá">
                                                        <i class="fal fa-trash-alt bigger-120"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <!--Xem chi tiết-->
                            <div class="modal fade" id="model-{{$id}}">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="widget-box">
                                        <div class="table-header text-uppercase">
                                            <button type="button" class="close" data-dismiss="modal" title="Đóng">
                                                <i class="fal fa-times white" style="padding-top: 5px;"></i>
                                            </button>
                                            Chi tiết lớp <button style="margin-top: 5px; width: 30px; height: 30px; padding-left: 5px; padding-bottom: 30px; background-color: transparent; color:white; border: 2px white solid; font-weight: bold;">{{$row->MaPhieuMo}}</button>
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12">
                                                        <div class="space visible-xs"></div>
                                                        <div class="profile-user-info profile-user-info-striped">
                                                            <div class="Table" role="grid">

                                                                <div class="Heading" role="columnheader">
                                                                    <div class="Cell">
                                                                        <p>Lớp/Môn</p>
                                                                    </div>
                                                                    <div class="Cell" role="columnheader">
                                                                        <p>Dạy Cho</p>
                                                                    </div>
                                                                    <div class="Cell" role="columnheader">
                                                                        <p>Giờ Học</p>
                                                                    </div>
                                                                    <div class="Cell" role="columnheader">
                                                                        <p>Yêu cầu</p>
                                                                    </div>
                                                                </div>

                                                                <div class="Row" role="row">
                                                                    <div class="Cell" role="gridcell">
                                                                        <p><b>Lớp:</b> {{$row->LopDay}}</p>
                                                                        <p><b>Môn:</b> {{$row->MonDay}}</p>
                                                                    </div>
                                                                    <div class="Cell" role="gridcell">
                                                                        <p>{{$row->ThongTin}}</p>
                                                                    </div>
                                                                    <div class="Cell" role="gridcell">
                                                                        <p><b>Số buổi:</b> {{$row->SoBuoiDay}} buổi/tuần</p>
                                                                        <p><b>Thời gian dạy:</b> {{$row->ThoiGian}}</p>
                                                                    </div>
                                                                    <div class="Cell" role="gridcell">
                                                                        <p>{{$row->YeuCau}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-sm btn-danger fa-pull-right" data-dismiss="modal">
                                                <i class="ace-icon fa fa-times"></i> Đóng
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Chỉnh sửa lớp-->
                            <div class="modal" id="md2-{{$id}}" role="dialog">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <div class="widget-box">
                                        <div class="widget-header">
                                            <h4 class="widget-title">Chỉnh sửa lớp</h4>
                                            <span class="close" style="font-size: 12pt; padding-top: 10px; padding-right: 10px; color: #ff4871;" data-dismiss="modal">Đóng</span>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <form method="post" action="{{route('trungtam.phieumolop_edit')}}">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="id" value="{{$row->MaPhieuMo}}">
                                                    <select name="lop" class="form-control" required>
                                                        <option value="" disabled selected>Chọn lớp dạy</option>
                                                        @foreach($getLopHoc as $item)
                                                            @if($row->LopDay == $item->TenLop)
                                                                <option value="{{$item->TenLop}}" selected>{{$item->TenLop}}</option>
                                                            @else
                                                                <option value="{{$item->TenLop}}">{{$item->TenLop}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="mon" value="{{$row->MonDay}}" title="Môn dạy" placeholder="Môn dạy | cú pháp: Toán, lý, hóa,..." class="form-control" required/>
                                                            <i class="ace-icon fal fa-book"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="diachi" value="{{$row->DiaChi}}" title="Địa chỉ của trung tâm" placeholder="Địa chỉ của trung tâm" class="form-control" required/>
                                                            <i class="ace-icon fal fa-map-marked"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="luong" id="luong" value="{{$row->Luong}}" title="Lương gia sư (chưa tính phí %)" placeholder="Lương gia sư | vd: 3.600.000" maxlength="10" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                                                            <i class="ace-icon fal fa-money-bill"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="mucphi" value="{{$row->LePhi}}" title="Mức phí %" placeholder="Mức phí % | vd: 20" class="form-control" maxlength="2" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
                                                            <i class="ace-icon fal fa-exchange"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <?php $buoi_array = array('2', '3', '4', '5'); ?>

                                                            <select name="sobuoi" class="form-control" title="Số buổi học /tuần" required>
                                                                @foreach($buoi_array as $id => $value)
                                                                    @if($row->SoBuoiDay == $value)
                                                                        <option value="{{$value}}" selected>{{$value}} buổi/tuần</option>
                                                                    @else
                                                                        <option value="{{$value}}">{{$value}} buổi/tuần</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="thoigian" value="{{$row->ThoiGian}}" title="Thời gian học" placeholder="Thời gian học | cú pháp: Thứ 2 - thứ 4; 17h - 19h" class="form-control" required/>
                                                            <i class="ace-icon fal fa-calendar-star"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="thongtin" value="{{$row->ThongTin}}" title="Thông tin học viên" placeholder="Thông tin về số lượng học sinh, học lực học sinh" class="form-control"/>
                                                            <i class="ace-icon fal fa-users-class"></i>
                                                        </span>
                                                    </label>

                                                    <label class="block clearfix">
                                                        <span class="block input-icon input-icon-right">
                                                            <input type="text" name="yeucau" value="{{$row->YeuCau}}" title="Yêu cầu thêm về gia sư dạy" placeholder="Yêu cầu về gia sư dạy, trình độ kinh nghiệm của gia sư dạy..." class="form-control"/>
                                                            <i class="ace-icon fal fa-chalkboard-teacher"></i>
                                                        </span>
                                                    </label>


                                                    <div class="clearfix">
                                                        <button type="reset" class="width-30 pull-left btn btn-sm">
                                                            <i class="ace-icon fal fa-refresh"></i>
                                                            <span class="bigger-110">Reset</span>
                                                        </button>

                                                        <button type="submit" class="width-30 pull-right btn btn-sm btn-success" name="update" value="update">
                                                            <span class="bigger-110">Chỉnh sủa</span>
                                                            <i class="ace-icon fal fa-arrow-right icon-on-right"></i>
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!--Xem chi tiết phiếu đăng ký-->

    <div class="modal" id="dangky_dialog">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="widget-box">
                <div class="widget-header" style="color: white; background: #307ECC;">
                    <h4 class="widget-title">
                        Phiếu đăng ký <button id="maphieudk" style="width: 30px; height:35px; margin-top: 5px; padding:0px; padding-top: -10px; background-color: transparent; border: white 2px solid;"></button>
                    </h4>
                    <span class="close_dangky" style="font-size: 12pt; padding-top: 10px; padding-right: 10px; float: right;" data-dismiss="modal">Đóng</span>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="Table" role="grid">
                            <div class="Heading" role="columnheader">
                                <div class="Cell">
                                    <p>Họ Tên</p>
                                </div>
                                <div class="Cell" role="columnheader">
                                    <p>Số ĐT</p>
                                </div>
                                <div class="Cell" role="columnheader">
                                    <p>Email</p>
                                </div>
                                <div class="Cell" role="columnheader">
                                    <p>Địa Chỉ</p>
                                </div>
                            </div>

                            <div class="Row" role="row">
                                <div class="Cell" role="gridcell">
                                    <p id="hoten"></p>
                                </div>
                                <div class="Cell" role="gridcell">
                                    <p id="sodt"></p>
                                </div>
                                <div class="Cell" role="gridcell">
                                    <p id="email"></p>
                                </div>
                                <div class="Cell" role="gridcell">
                                    <p id="diachi"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection