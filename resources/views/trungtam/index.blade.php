@extends('trungtam.master')
@section('title', 'Dashboard')

@section('nav-content')
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="nav-menu-icon fal fa-home"></i>
            <a href="/">Home</a>
        </li>

        <li><a href="{{Route('trungtam')}}">Dashboard</a></li>
    </ul>
</div>
@endsection

@section('page-content')
    <div class="alert alert-block alert-success" style="background-color: #dff0d8;">
        <i class="far fa-handshake"></i>
        CHÀO MỪNG <strong class="green">{{Auth::user()->name}}</strong> QUAY LẠI DASHBOARD!
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 pricing-box">
            <div class="widget-box widget-color-blue">
                <div class="widget-header">
                    <h4 class="widget-title bigger lighter">GIA SƯ</h4>
                    <div class="widget-toolbar">
                        <a href="javascript:void(0);" data-action="collapse">
                            <i class="ace-icon fal fa-chevron-down"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="col-lg-4 col-md-4">
                            <div class="panel-primary">
                                <div class="panel-heading" style="margin-bottom: 10px; color: #337ab7; background-color:#f4fffc; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-chalkboard-teacher fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\gia_su::count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">TỔNG SỐ GIA SƯ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-primary">
                                <div class="panel-heading" style="margin-bottom: 10px; background-color:#a5e0ff; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-chalkboard-teacher fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\gia_su::where('Status', 1) ->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">ĐANG HOẠT ĐỘNG</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-danger">
                                <div class="panel-heading" style="margin-bottom: 10px; border: 0px solid #ff444e;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-chalkboard-teacher fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\gia_su::where('Status', 0) ->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">ĐANG BỊ KHOÁ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a href="{{Route('trungtam.giasu')}}" class="btn btn-block btn-primary">
                            <span>Xem chi tiết</span>
                            <i class="fal fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="space-16"></div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 pricing-box">
            <div class="widget-box widget-color-green">
                <div class="widget-header">
                    <h4 class="widget-title bigger lighter">PHỤ HUYNH ĐĂNG KÝ</h4>
                    <div class="widget-toolbar">
                        <a href="javascript:void(0);" data-action="collapse">
                            <i class="ace-icon fal fa-chevron-down"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="col-lg-4 col-md-4">
                            <div class="panel-success">
                                <div class="panel-heading" style="margin-bottom: 10px; background-color:#f4fffc; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-layer-plus fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_dang_ky::count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">TỔNG SỐ ĐĂNG KÝ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-success">
                                <div class="panel-heading" style="margin-bottom: 10px; color: #f4fffc;  background-color:#b5e2c5; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-layer-plus fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_dang_ky::where('Status', 1) ->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">ĐÃ MỞ LỚP</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-danger">
                                <div class="panel-heading" style="margin-bottom: 10px; border: 0px solid #ff444e;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-layer-plus fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_dang_ky::where('Status', 0) ->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">CHƯA ĐƯỢC DUYỆT</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a href="{{Route('trungtam.phieudangky')}}" class="btn btn-block btn-success">
                            <span>Xem chi tiết</span>
                            <i class="fal fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="space-16"></div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 pricing-box">
            <div class="widget-box widget-color-orange">
                <div class="widget-header">
                    <h4 class="widget-title bigger lighter">DANH SÁCH LỚP</h4>
                    <div class="widget-toolbar">
                        <a href="javascript:void(0);" data-action="collapse" style="color: white;">
                            <i class="ace-icon fal fa-chevron-down"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="col-lg-4 col-md-4">
                            <div class="panel-info">
                                <div class="panel-heading" style="margin-bottom: 10px; color: #ffbf61; background-color:#f4fffc; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-list-alt fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_mo_lop::count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">TỔNG SỐ LỚP</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-success">
                                <div class="panel-heading" style="margin-bottom: 10px; color:#f4fffc; background-color:#ffe67d; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-list-alt fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_mo_lop::where('Status', 1) ->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">ĐANG ĐƯỢC DẠY</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-danger">
                                <div class="panel-heading" style="margin-bottom: 10px; border: 0px solid #ff444e;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-list-alt fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_mo_lop::where('Status', 0) ->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">CHƯA ĐƯỢC DẠY</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a href="{{Route('trungtam.phieumolop')}}" class="btn btn-block btn-warning">
                            <span>Xem chi tiết</span>
                            <i class="fal fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="space-16"></div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 pricing-box">
            <div class="widget-box widget-color-purple">
                <div class="widget-header">
                    <h4 class="widget-title bigger lighter">GIA SƯ NHẬN DẠY</h4>
                    <div class="widget-toolbar">
                        <a href="javascript:void(0);" data-action="collapse">
                            <i class="ace-icon fal fa-chevron-down"></i>
                        </a>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div class="col-lg-4 col-md-4">
                            <div class="panel-info">
                                <div class="panel-heading" style="margin-bottom: 10px; color: #80367e; background-color:#f4fffc; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-users-class fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_nhan_lop::count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">TỔNG SỐ NHẬN DẠY</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-success">
                                <div class="panel-heading" style="margin-bottom: 10px; color:#f4fffc; background-color:#9797fd; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-users-class fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_nhan_lop::where('Status', 3)->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">ĐANG NHẬN</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-success">
                                <div class="panel-heading" style="margin-bottom: 10px; background: #befcd8; border: 0px solid #ff444e;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-users-class fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_nhan_lop::where('Status', 2)->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">ĐỦ ĐIỀU KIỆN (Đã đóng phí)</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-default">
                                <div class="panel-heading" style="margin-bottom: 10px; color: #ffbf61; background-color:#fbfaa7; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-users-class fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_mo_lop::where('Status', 1)->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">XEM XÉT (Chưa đóng phí)</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-success">
                                <div class="panel-heading" style="margin-bottom: 10px; color:#232927; background-color:#6A7177; border: 0px solid #337ab7;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-users-class fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_nhan_lop::where('Status', 4) ->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">THẤT BẠI</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="panel-danger">
                                <div class="panel-heading" style="margin-bottom: 10px; border: 0px solid #ff444e;">
                                    <div class="row">
                                        <div class="col-xs-3" style="margin-left: 10px; padding-top: 20px;">
                                            <i class="fal fa-users-class fa-5x"></i>
                                        </div>
                                        <div class="col-xs-7 text-right">
                                            <div style="font-size: 40pt; margin-right: -50px;">{{\App\phieu_nhan_lop::where('Status', 0) ->count()}}</div>
                                            <div style="font-size: 12pt; margin-right: -50px;">CHƯA XỬ LÝ</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <a href="{{Route('trungtam.nhanlop')}}" class="btn btn-block btn-purple">
                            <span>Xem chi tiết</span>
                            <i class="fal fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script-footer')

@endsection