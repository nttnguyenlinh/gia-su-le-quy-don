-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 05, 2019 at 03:57 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_giasu`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` int(1) DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `email`, `password`, `avatar`, `Status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Linh', 'nguyenlinh', 'ntt.nguyenlinh@gmail.com', '$2y$10$chfgcNF9F6AUIV4sw3oDyunGKek3HjldCePNnoBKXsupfbGuWVzb6', 'https://i.imgur.com/SfcFCCe.jpg', 1, 'tSHUxhAYl4BVLbREtOySgUR1qzi4ZVc6R95ZSl20zDwudJA5cLsb9NGPTWKC', '2018-08-04 09:51:54', '2018-12-20 07:17:59'),
(3, 'Đỗ Đức Hồng Hà', 'hongha', 'hadung2014@gmail.com', '$2y$10$hrY5lfZZ41Q809dwmT4sZ.T55xw1T3cQk7zYjxETMn5s18XrAcE7i', 'https://i.imgur.com/Q5dzf0z.jpg', 1, 'WlM3Viy5vBOWY62uTMaa2jbw1kLicTCnTKDqOwxHceeXPBxCGwg9Q0lJ1MQU', '2018-11-04 03:03:42', '2019-01-02 18:13:34');

-- --------------------------------------------------------

--
-- Table structure for table `gia_su`
--

CREATE TABLE `gia_su` (
  `MaGiaSu` int(11) NOT NULL,
  `HoTen` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NgaySinh` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GioiTinh` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Nam',
  `NguyenQuan` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Hồ Chí Minh',
  `GiongNoi` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Miền Nam',
  `DiaChi` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CMND` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SoDT` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `MatKhau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `AnhThe` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TruongDaoTao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NganhHoc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CongTac` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NamTN` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `TrinhDo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LoaiGiaoVien` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UuDiem` text COLLATE utf8_unicode_ci,
  `MonDay` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LopDay` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TinhThanh` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `KhuVuc` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gia_su`
--

INSERT INTO `gia_su` (`MaGiaSu`, `HoTen`, `NgaySinh`, `GioiTinh`, `NguyenQuan`, `GiongNoi`, `DiaChi`, `CMND`, `Email`, `SoDT`, `MatKhau`, `AnhThe`, `TruongDaoTao`, `NganhHoc`, `CongTac`, `NamTN`, `TrinhDo`, `LoaiGiaoVien`, `UuDiem`, `MonDay`, `LopDay`, `TinhThanh`, `KhuVuc`, `Status`, `created_at`, `updated_at`) VALUES
(1, 'Hoàng Ngọc Huyền', '26-09-1999', 'Nữ', 'Hồ Chí Minh', 'Miền Nam', 'Quận 3', '301525666', 'hoangngochuyen@gmail.com', '0973412721', '$2y$10$K18f5rr4n6AyjGWBow6AGulYOQfs2aJm9ml5wh68AEqFwEe8oXBtW', 'https://i.imgur.com/hVhgEyY.jpg', 'ĐH Kinh Tế HCM', 'Ngôn ngữ Anh', NULL, '2021', 'Sinh Viên', NULL, 'Có kinh nghiệm dạy kèm tại nhà, kiên nhân, nhiệt tình với các học viên, kĩ năng nền tảng tiếng anh khá tốt', '1,2,3,9,10,12', '2,3,4,5,6,7,8,9,10,11,12', 'Hồ Chí Minh', '1,3,4,7,22', 1, '2018-08-15 17:00:00', '2018-12-15 14:39:37'),
(2, 'Lê Thị Mỹ Anh', '05-08-1988', 'Nữ', 'Hồ Chí Minh', 'Miền Nam', 'Gò Vấp', '201122565', 'lemyanh@gmail.com', '0906754290', '$2y$10$Lh.w/ZKiO41bNshI558TWuG1ahqpwKxaBcYC6uS./JzpOfBzu/0cK', 'https://i.imgur.com/WIWrk4Z.jpg', 'ĐH Quốc tế Hồng Bàng HCM', 'Lịch sử, Tiếng trung', NULL, '2010', 'Thạc Sỹ', 'GV dạy tại trường', 'Có 8 năm kinh nghiệm dạy kèm', '1,2,3,4,5,6', '1,2,3', 'Hồ Chí Minh', '1,2,3', 1, '2018-08-18 16:33:32', '2018-12-15 14:30:30'),
(3, 'Huỳnh Thị Hồng Phương', '02-09-1998', 'Nữ', 'Hồ Chí Minh', 'Miền Nam', 'Quận 7', '01669788679', 'hongphuong@gmail.com', '0166978867', '$2y$10$ff8F7NFwRatL/.q7yU2aROYbYDDnEI4T0eX2kFqdkr0rELqiJP8Oy', 'https://i.imgur.com/qR7QlUP.jpg', 'ĐH Tôn Đức Thắng HCM', 'Kế Toán', NULL, '2020', 'Sinh Viên', NULL, 'Trong suốt quá trình học tại trường đạt 11 năm liền học sinh giỏi từ lớp 2 đến lớp 12. Có sở thích yêu trẻ em nên có thể hoà hợp và hiểu được các em.', '1,24', '3,4,5,6', 'Hồ Chí Minh', '7', 1, '2018-08-18 17:21:04', '2018-08-18 17:21:04'),
(4, 'Dương Vĩ Tấn', '09-07-1997', 'Nam', 'Bến Tre', 'Miền Nam', 'Bình Chánh', '0914162689', 'duongvitan@gmail.com', '0914162689', '$2y$10$BOTMlE1n/VQ2yOvPzE6/seWXWeXp0oF6nk2Zw/LVCdoIC7rHIhSdu', 'https://i.imgur.com/dPkwGrF.jpg', 'ĐH Kinh Tế HCM', 'Kinh doanh quốc tế', NULL, '2019', 'Sinh Viên', NULL, 'Đã làm gia sư dạy kèm từ năm 2015 đến nay. Học sinh giỏi 12 năm liền, đạt thành tích cao trong học tập.', '1,2,3,12,24', '5,6,7,8,9,10,11', 'Hồ Chí Minh', '1,3,4,9', 1, '2018-08-18 17:25:45', '2018-12-15 05:11:28'),
(5, 'Vương Minh Tuấn', '12-12-1999', 'Nam', 'An Giang', 'Miền Nam', 'Quận 3', '0978178764', 'vuongminhtuan@gmail.com', '0978178764', '$2y$10$RHuS87XrfHalbgiOZ2XVFuH/7wsAoe5YP5K6ACPimxqu7nuB7Rxcu', 'https://i.imgur.com/2Y4xHZG.jpg', 'ĐH Khoa Học Tự Nhiên HCM', 'Công nghệ kỹ thuật hóa', NULL, '2021', 'Sinh Viên', NULL, 'Chăm chỉ, trách nhiệm, Nắm vững nội dung chuyên môn, tận tâm trong công việc.', '1,2,3,5', '7,8,9,10', 'Hồ Chí Minh', '1,3,4,5,6', 1, '2018-08-18 17:50:53', '2018-08-21 12:22:32'),
(6, 'Nguyễn Phương Chi', '09-09-1963', 'Nữ', 'An Giang', 'Miền Nam', 'Quận 3', '0979809204', 'nguyenphuongchi@gmail.com', '0979809204', '$2y$10$3/QJuOpnrQkK4Ydgn14BS.GScTpcpJzkO/4PRaxfAloTgKRJTTPb.', 'https://i.imgur.com/ltTLSdp.jpg', 'Lê Quý Đôn Q3', 'Sư phạm Ngữ văn', 'Lê Quý Đôn Q3', '1986', 'Cử Nhân', 'GV dạy tại trường', '25 năm kinh nghiệm day Ngữ Văn. Chuyên dạy lớp cuối cấp và luyện thi tuyển sinh. Tận tâm và nghiêm túc.', '5', '8,10,12', 'Hồ Chí Minh', '1,3,7,17', 1, '2018-08-18 17:58:36', '2018-08-18 17:58:36'),
(7, 'Hoàng Thị Phương Phúc', '29-04-1996', 'Nữ', 'Cà Mau', 'Miền Nam', 'Quận 5', '0905682529', 'phuongtruc@gmail.com', '0905682529', '$2y$10$Ht4obRs/6H8ck3bjUPS2H..aabbq1I.urI8.oy/tGV3mBGrhpiXSq', 'https://i.imgur.com/LZxSPD8.jpg', 'Trường THPT Marie Curie', 'Sư phạm Sinh học', NULL, '2018', 'Giáo viên', 'GV dạy tại trường', NULL, '1,4,24', '2,3,4,5,6', 'Hồ Chí Minh', '1,3,5', 1, '2018-08-18 18:01:46', '2018-12-15 12:31:22'),
(8, 'Mai Hồng Ngọc', '07-02-1992', 'Nữ', 'Cần Thơ', 'Miền Nam', 'Quận 5', '0982725726', 'maihongngoc@gmail.com', '0982725726', '$2y$10$Q.7e6iIoMyMtExyJeSm3luiHr92KBzc1r/LK4QUWSbczX9MOp4SCO', 'https://i.imgur.com/8edk4A8.jpg', 'ĐH Sư Phạm HCM', 'Sư phạm Tin học', NULL, '2016', 'Giáo viên', 'GV dạy tại trường', 'Có kinh nghiệm dạy kèm lâu năm các lớp từ lớp 1 đến lớp 9, luyện thi vô lớp 10 các môn Toán, Lý, Hóa, Anh, Tin học.', '1,2,3,8,10,24', '1,2,3,4,5,6,10', 'Hồ Chí Minh', '1,3,5,10', 1, '2018-08-18 18:05:26', '2018-12-18 14:18:15'),
(9, 'Nguyễn Linh', '02-03-1996', 'Nam', 'Hồ Chí Minh', 'Miền Nam', 'Quận 7', '301544141', 'nguyenlinh2396@gmail.com', '0906754112', '$2y$10$S6t0cdWiOmd25ZN4Iftvo.ne57m5KZf4JS9zd6LuPji1.yIiRZoue', 'https://i.imgur.com/cv6G3n0.jpg', 'ĐH Nguyễn Tất Thành HCM', 'Công Nghệ Thông Tin', NULL, '2018', 'Cử Nhân', NULL, NULL, '1,8', '11,12,13', 'Hồ Chí Minh', '4,7', 1, '2018-09-07 10:00:31', '2018-12-15 12:40:41'),
(10, 'Phạm Thị Hằng', '06-05-1964', 'Nữ', 'Long An', 'Miền Nam', 'Quận 12', '301212121', 'phamthihang2014@gmail.com', '0977467524', '$2y$10$dLbOUiho7Rk.6u0eigrSMO3a7DTOZxTf.1kSKO8L3U2Wusu5fftzG', 'https://i.imgur.com/tMTD4Xh.jpg', 'ĐH Sư Phạm HCM', 'Sư phạm Lịch sử', NULL, '1979', 'Giáo viên', NULL, NULL, '1', '11', 'Hồ Chí Minh', '12', 1, '2018-11-04 04:38:43', '2018-12-18 16:30:46');

-- --------------------------------------------------------

--
-- Table structure for table `hoc_phi`
--

CREATE TABLE `hoc_phi` (
  `MaHP` int(11) NOT NULL,
  `KhoiLop` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SinhVien` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `GiaoVien` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `SoBuoi` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hoc_phi`
--

INSERT INTO `hoc_phi` (`MaHP`, `KhoiLop`, `SinhVien`, `GiaoVien`, `SoBuoi`) VALUES
(1, 'Lớp lá, 1, 2, 3, 4', '600 -> 700', '1000 -> 1200', 2),
(2, 'Lớp lá, 1, 2, 3, 4', '800 -> 1000', '1400 -> 1800', 3),
(3, 'Lớp lá, 1, 2, 3, 4', '1100 -> 1500', '1800 -> 2000', 4),
(4, 'Lớp lá, 1, 2, 3, 4', '1400 -> 1600', '2200 -> 2800', 5),
(5, 'Lớp 5, 6, 7, 8', '700 -> 800', '1200 -> 1400', 2),
(6, 'Lớp 5, 6, 7, 8', '900 -> 1000', '1600 -> 2000', 3),
(7, 'Lớp 5, 6, 7, 8', '1200 -> 1300', '2100 -> 2700', 4),
(8, 'Lớp 5, 6, 7, 8', '1400 -> 1600', '2500 -> 3000', 5),
(9, 'Lớp 9, 10, 11, 12', '800 -> 1000', '1400 -> 1600', 2),
(10, 'Lớp 9, 10, 11, 12', '1100 -> 1400', '1800 -> 2400', 3),
(11, 'Lớp 9, 10, 11, 12', '1300 -> 1700', '2200 -> 2900', 4),
(12, 'Lớp 9, 10, 11, 12', '1700 -> 2000', '3100 -> 3700', 5),
(13, 'Luyện thi, ngoại ngữ,...', '900 -> 1100', '1500 -> 1700', 2),
(14, 'Luyện thi, ngoại ngữ,...', '1300 -> 1500', '2200 -> 2500', 3),
(15, 'Luyện thi, ngoại ngữ,...', '1600 -> 1800', '2600 -> 3100', 4),
(16, 'Luyện thi, ngoại ngữ,...', '2000 -> 2300', '3200 -> 4000', 5);

-- --------------------------------------------------------

--
-- Table structure for table `khu_vuc`
--

CREATE TABLE `khu_vuc` (
  `MaKV` int(11) NOT NULL,
  `TenKV` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MaTinh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `khu_vuc`
--

INSERT INTO `khu_vuc` (`MaKV`, `TenKV`, `MaTinh`) VALUES
(1, 'Quận 1', 1),
(2, 'Quận 2', 1),
(3, 'Quận 3', 1),
(4, 'Quận 4', 1),
(5, 'Quận 5', 1),
(6, 'Quận 6', 1),
(7, 'Quận 7', 1),
(8, 'Quận 8', 1),
(9, 'Quận 9', 1),
(10, 'Quận 10', 1),
(11, 'Quận 11', 1),
(12, 'Quận 12', 1),
(13, 'Bình Tân', 1),
(14, 'Bình Thạnh', 1),
(15, 'Gò Vấp', 1),
(16, 'Phú Nhuận', 1),
(17, 'Tân Bình', 1),
(18, 'Tân Phú', 1),
(19, 'Thủ Đức', 1),
(20, 'Bình Chánh', 1),
(21, 'Cần Giờ', 1),
(22, 'Củ Chi', 1),
(23, 'Hóc Môn', 1),
(24, 'Nhà Bè', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lop_hoc`
--

CREATE TABLE `lop_hoc` (
  `MaLop` int(11) NOT NULL,
  `TenLop` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CapHoc` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lop_hoc`
--

INSERT INTO `lop_hoc` (`MaLop`, `TenLop`, `CapHoc`) VALUES
(1, 'Lớp lá', 1),
(2, 'Lớp 1', 1),
(3, 'Lớp 2', 1),
(4, 'Lớp 3', 1),
(5, 'Lớp 4', 1),
(6, 'Lớp 5', 1),
(7, 'Lớp 6', 2),
(8, 'Lớp 7', 2),
(9, 'Lớp 8', 2),
(10, 'Lớp 9', 2),
(11, 'Lớp 10', 3),
(12, 'Lớp 11', 3),
(13, 'Lớp 12', 3),
(14, 'Ôn trường chuyên', 3),
(15, 'Ôn đại học', 5),
(17, 'Lớp song ngữ', 4),
(18, 'Dạy bằng tiếng anh', 4),
(20, 'Lớp khác', 6);

-- --------------------------------------------------------

--
-- Table structure for table `mon_hoc`
--

CREATE TABLE `mon_hoc` (
  `MaMon` int(11) NOT NULL,
  `TenMon` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mon_hoc`
--

INSERT INTO `mon_hoc` (`MaMon`, `TenMon`) VALUES
(1, 'Toán'),
(2, 'Lí'),
(3, 'Hoá'),
(4, 'Sinh'),
(5, 'Văn'),
(6, 'Sử'),
(7, 'Địa'),
(8, 'Tin Học'),
(9, 'Vẽ'),
(10, 'Rèn Chữ'),
(11, 'Báo Bài'),
(12, 'Tiếng Anh'),
(13, 'Tiếng Pháp'),
(14, 'Tiếng Hoa'),
(15, 'Tiếng Hàn'),
(16, 'Tiếng Nhật'),
(17, 'Anh Văn Giao Tiếp'),
(18, 'TOEIC'),
(19, 'IELTS'),
(20, 'TOEFL'),
(21, 'Đàn Piano'),
(22, 'Đàn Organ'),
(23, 'Đàn Guitar'),
(24, 'Tiếng Việt'),
(25, 'Tiếng Việt Cho Người Nước Ngoài');

-- --------------------------------------------------------

--
-- Table structure for table `phieu_dang_ky`
--

CREATE TABLE `phieu_dang_ky` (
  `MaPhieuDK` int(11) NOT NULL,
  `HoTen` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DiaChi` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SoDT` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Lop` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MonHoc` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GiaSu` int(11) DEFAULT NULL,
  `SoHocSinh` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HocLuc` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'trung bình',
  `SoBuoiDay` int(1) NOT NULL DEFAULT '2',
  `ThoiGianHoc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YeuCau` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YeuCauKhac` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `phieu_dang_ky`
--

INSERT INTO `phieu_dang_ky` (`MaPhieuDK`, `HoTen`, `DiaChi`, `SoDT`, `Email`, `Lop`, `MonHoc`, `GiaSu`, `SoHocSinh`, `HocLuc`, `SoBuoiDay`, `ThoiGianHoc`, `YeuCau`, `YeuCauKhac`, `Status`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Minh Quân', 'Quận 7', '0900741474', NULL, 'Lớp 8', 'Toán', NULL, '1 hs nam', 'trung bình', 3, 'Thứ 2 - 4 - 6', 'Giáo viên', 'Giáo viên nữ', 1, '2018-08-13 18:14:38', '2018-08-21 11:40:56'),
(2, 'Nguyễn Minh Đăng', 'Quận 4', '01345687', NULL, NULL, 'Đàn piano', NULL, '1 nam', NULL, 2, 'Thứ 2 - 4', 'Giáo viên', 'Giáo viên giàu kinh nghiệm', 1, '2018-08-14 04:08:47', '2018-08-17 04:08:47'),
(3, 'Bùi Trung Dũng', 'Nguyễn sơn, phường Phú Thạnh, Tân phú', '0167358058', 'buitrungdung@gmail.com', 'Lớp 11', 'Hoá', NULL, '1 hs nam', NULL, 2, 'Tối T3, 5 (7h30-9h)', 'Giáo viên', 'Giáo viên Nam & Nữ', 1, '2018-08-14 07:40:03', '2018-08-17 07:40:03'),
(4, 'Nguyễn Minh Hùng', 'Quận 4', '0122244214', NULL, NULL, 'Vẽ', NULL, '1 hs', NULL, 3, '2 4 6', 'Giảng Viên', 'nữ', 1, '2018-08-16 07:58:49', '2018-08-18 07:48:00'),
(5, 'Đỗ Đặng Bích Ngân', 'Quận 9', '0122541452', 'ddbichngan@gmail.com', 'Lớp 6', 'Toán, Lý, Hoá', NULL, '1 học sinh', 'trung bình', 3, 'Thứ 2, thứ 5, thứ 6', 'Giảng Viên', 'Giảng viên có nhiều năm kinh nghiệm trong việc giảng dạy.', 1, '2018-08-21 11:32:15', '2018-08-24 04:38:38'),
(6, 'Bùi Trung Hải', 'Bình Tân', '0906754290', 'buitrunghai@gmail.com', 'Lớp 12', 'Toán, Lí', NULL, '1hs nữ', 'Khá', 3, 'Tối thứ 2 - 4 - 6', 'Giáo viên', 'Giáo viên có nhiều năm kinh nghiệm', 1, '2018-09-02 13:11:22', '2018-09-02 14:47:31'),
(7, 'Nguyễn Trung Kiên', 'Bình Chánh', '0126854005', 'nguyentrungkien@gmail.com', 'Ôn trường chuyên', 'Toán, Lí, Hoá', NULL, '1hs nữ', 'trung bình', 2, 'thứ 5 và thứ 7', 'Sinh Viên', 'SInh viên nam', 0, '2018-09-02 13:17:37', '2018-09-02 14:47:27'),
(8, 'Đoàn Minh Hùng', 'an ohú đông, quận 12', '0945521452', 'doanminhung@gmail.com', NULL, 'Toán, anh văn', NULL, '1 học sinh nam', NULL, 3, 'thứ 2, 4 ,6', 'Giáo viên', 'giáo viên có kinh nghiệm dạy kèm', 1, '2018-09-06 08:36:26', '2018-10-18 16:38:56'),
(9, 'Bùi Minh Quân', NULL, '0164924403', 'buiminhquan@gmail.com', NULL, 'Toán', NULL, NULL, NULL, 2, 'thứ 4, thứ 6, 5h chiều', NULL, NULL, 1, '2018-09-07 09:50:10', '2018-10-18 16:46:56'),
(27, 'Nguyễn Linh', NULL, '0906754290', 'ntt.nguyenlinh@gmail.com', 'Lớp 7', 'Toán', NULL, NULL, NULL, 2, NULL, 'Kỹ Sư', NULL, 1, '2018-11-03 17:25:27', '2018-12-15 15:38:25'),
(28, 'Nguyễn Linh', 'Quận 7', '0909009000', 'nguyenlinh2396@gmail.com', 'Lớp 9', NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, 0, '2018-11-04 03:07:55', '2018-11-04 03:12:16'),
(29, 'Nguyễn Văn B', 'Quận 12', '0999999999', 'nguyenvana@gmail.com', 'Lớp 12', 'Toán', NULL, NULL, NULL, 2, 'Thứ 4 thứ 6', NULL, NULL, 1, '2018-11-04 04:11:04', '2018-12-18 16:45:36'),
(31, 'nguyễn văn hai', 'an lão bình quới q11', '0966555454', NULL, 'Lớp 12', 'Toán, Lí, Hoá', NULL, NULL, NULL, 3, 'Tối 2 4 6', 'Giáo viên', 'Giáo viên nữ', 1, '2018-12-16 14:47:50', '2018-12-16 14:49:11'),
(32, 'Nguyễn Linh', 'Hoàng Diệu, quận 4', '0778888888', 'nguyenlinh2396@gmail.com', 'Lớp 10', 'Toán, Văn', NULL, '1 học sinh', 'Trung Bình', 3, 'Chiều 3 5 7', 'Giáo viên', 'Giáo viên nữ', 1, '2019-01-02 14:31:28', '2019-01-02 14:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `phieu_mo_lop`
--

CREATE TABLE `phieu_mo_lop` (
  `MaPhieuMo` int(11) NOT NULL,
  `MaPhieuDK` int(11) NOT NULL,
  `LopDay` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MonDay` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DiaChi` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Luong` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LePhi` varchar(2) COLLATE utf8_unicode_ci DEFAULT '30',
  `SoBuoiDay` int(1) DEFAULT '2',
  `ThoiGian` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ThongTin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YeuCau` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `phieu_mo_lop`
--

INSERT INTO `phieu_mo_lop` (`MaPhieuMo`, `MaPhieuDK`, `LopDay`, `MonDay`, `DiaChi`, `Luong`, `LePhi`, `SoBuoiDay`, `ThoiGian`, `ThongTin`, `YeuCau`, `slug`, `Status`, `created_at`, `updated_at`) VALUES
(2, 1, 'Lớp 8', 'Toán', 'Quận 7', '3.600.000', '35', 3, 'Thứ 2 - 4 - 6', '1 hs nam trung bình', 'Giáo viên nữ', 'toan-lop-8-814124319', 0, '2018-08-12 17:00:00', '2018-10-13 20:57:01'),
(3, 3, 'Lớp 11', 'Hoá', 'Tân Phú', '3.600.000', '40', 2, 'Tối T3, 5 (7h30-9h)', '1 hs nam', 'Giáo viên giỏi', 'hoa-lop-11-1612802531', 1, '2018-08-14 12:57:58', '2018-10-14 09:56:51'),
(8, 4, NULL, 'Vẽ', 'Quận 4', '4.000.000', '35', 3, '2 4 6', '1 hs', 'Giáo viên: nữ', 've-1278424021', 0, '2018-08-18 07:46:31', '2018-08-18 07:46:31'),
(9, 5, 'Lớp 6', 'Toán, Lý, Hoá', 'QUận 9', '3.900.000', '35', 3, 'Thứ 2, thứ 5, thứ 6', '1 học sinh - trung bình', 'Giảng viên có nhiều năm kinh nghiệm trong việc giảng dạy.', 'toan-ly-hoa-lop-6-406152386', 1, '2018-08-21 11:39:55', '2018-12-21 13:41:59'),
(10, 6, 'Ôn trường chuyên', 'Toán, Lí, Hoá', 'Bình Chánh', '4.500.000', '40', 2, 'thứ 5 và thứ 7', '1hs nữ - trung bình', 'Sinh viên nam', 'toan-li-hoa-on-truong-chuyen-871998272', 0, '2018-09-02 14:16:35', '2018-11-04 04:41:40'),
(12, 7, 'Ôn trường chuyên', 'Toán, Lý', 'Bình Chánh', '4.500.000', '40', 2, 'thứ 5 và thứ 7', '1 hs nữ', 'Giáo viên nữ', 'toan-ly-on-truong-chuyen-1504230327', 0, '2018-09-02 14:28:01', '2018-10-14 09:55:25'),
(13, 8, 'Lớp 10', 'Toán, anh văn', 'Quận 9', '3.600.000', '10', 3, 'thứ 2, 4 ,6', '1 học sinh nam', 'Giáo viên nữ có kinh nghiệm dạy kèm', 'toan-anh-van-lop-10-1514174686', 0, '2018-10-14 09:59:55', '2018-10-18 14:58:46'),
(14, 9, NULL, 'Toán', 'Quận 9', '4.000.000', '20', 2, 'Thứ 4 - 6, 5h chiều', NULL, NULL, 'toan-715285384', 0, '2018-10-18 16:46:56', '2018-10-18 16:46:56'),
(15, 28, 'Lớp 9', 'Toán, Văn', 'Quận 9', '2.000.000', '5', 2, 'Thứ 4, Thứ 6, 18h', NULL, NULL, 'toan-van-lop-9-27792168', 1, '2018-11-04 03:12:16', '2018-11-04 03:32:28'),
(16, 31, 'Lớp 12', 'Toán, Lí, Hoá', 'Quận 9', '4.000.000', '20', 3, 'Tối 2 4 6', NULL, 'Giáo viên nữ', 'toan-li-hoa-lop-12-1241799156', 0, '2018-12-16 14:49:11', '2018-12-20 17:38:19'),
(17, 29, 'Lớp 12', 'Toán', 'Quận 9', '5.000.000', '20', 2, 'Thứ 4 thứ 6', '1 học sinh', 'Giáo viên nữ', 'toan-lop-12-1552582074', 0, '2018-12-18 16:45:36', '2019-01-02 10:56:29'),
(18, 32, 'Lớp 10', 'Toán, Văn', 'Hoàng Diệu, Quận 4', '3.000.000', '15', 3, 'Chiều 3 5 7', '1 học sinh trung bình', 'Giáo viên nữ', 'toan-van-lop-10-1860870504', 0, '2019-01-02 14:35:34', '2019-01-02 14:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `phieu_nhan_lop`
--

CREATE TABLE `phieu_nhan_lop` (
  `MaPhieuNhan` int(11) NOT NULL,
  `MaPhieuMo` int(11) NOT NULL,
  `MaGiaSu` int(11) NOT NULL,
  `HinhThucNhan` int(1) NOT NULL,
  `NgayDay` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GioDay` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LePhi` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `NoiDungThem` text COLLATE utf8_unicode_ci,
  `Status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `phieu_nhan_lop`
--

INSERT INTO `phieu_nhan_lop` (`MaPhieuNhan`, `MaPhieuMo`, `MaGiaSu`, `HinhThucNhan`, `NgayDay`, `GioDay`, `LePhi`, `NoiDungThem`, `Status`, `created_at`, `updated_at`) VALUES
(1, 9, 1, 0, '25-08-2018', '18h', '35', NULL, 3, '2018-08-23 07:26:35', '2018-12-21 13:41:59'),
(2, 8, 1, 1, '23-08-2018', '10h', '35', NULL, 0, '2018-08-23 07:33:40', '2018-08-23 07:33:40'),
(3, 3, 1, 0, '23-08-2018', '15h', '40', NULL, 3, '2018-08-23 07:35:36', '2018-10-14 03:54:14'),
(4, 2, 1, 1, '23-08-2018', '9h', '35', NULL, 0, '2018-08-23 07:37:30', '2018-08-23 07:37:30'),
(5, 3, 2, 1, '25-08-2018', 'Lúc khác', '40', 'Dạy 19h30', 0, '2018-08-24 10:03:24', '2018-08-24 10:03:24'),
(6, 9, 4, 1, '27-08-2018', '18h', '35', NULL, 0, '2018-08-24 10:12:14', '2018-08-24 10:12:14'),
(7, 10, 1, 0, '02-09-2018', '17h', '40', NULL, 4, '2018-09-02 14:18:12', '2018-12-08 05:12:23'),
(9, 12, 1, 0, '05-09-2018', '18h', '40', NULL, 0, '2018-09-03 09:32:21', '2018-09-03 09:32:21'),
(10, 9, 8, 0, '07-09-2018', '10h30', '35', NULL, 0, '2018-09-07 08:40:51', '2018-09-07 08:40:51'),
(11, 10, 8, 0, '07-09-2018', '18h', '40', NULL, 3, '2018-09-07 09:22:39', '2018-11-04 03:35:23'),
(12, 8, 8, 1, '07-09-2018', '15h', '35', NULL, 1, '2018-09-07 09:30:25', '2018-10-14 03:54:03'),
(13, 12, 7, 1, '07-09-2018', '18h', '40', 'Đóng trước lệ phí 200.000đ', 1, '2018-09-07 10:18:55', '2018-10-14 03:53:53'),
(14, 10, 4, 0, '14-10-2018', '18h', '40', NULL, 0, '2018-10-14 04:43:41', '2018-10-16 11:25:16'),
(15, 13, 9, 1, '18-10-2018', '18h', '10', NULL, 1, '2018-10-18 04:43:55', '2019-01-02 14:26:54'),
(16, 12, 9, 1, '18-10-2018', '18h', '40', NULL, 0, '2018-10-18 04:46:35', '2018-10-18 04:46:35'),
(17, 10, 9, 1, '18-10-2018', '18h', '40', NULL, 2, '2018-10-18 07:30:44', '2018-11-04 04:41:40'),
(18, 15, 9, 1, '05-11-2018', '18h', '10', 'đóng vào ngày 05/11 buổi chiều', 3, '2018-11-04 03:26:02', '2018-11-04 03:28:34'),
(19, 14, 9, 0, '04-11-2018', '17h', '20', NULL, 1, '2018-11-04 04:47:51', '2019-01-02 14:29:24'),
(22, 18, 9, 0, '02-01-2019', '18h', '15', NULL, 0, '2019-01-02 16:56:53', '2019-01-02 16:56:53');

-- --------------------------------------------------------

--
-- Table structure for table `phieu_tuyen_dung`
--

CREATE TABLE `phieu_tuyen_dung` (
  `MaPhieuTD` int(11) NOT NULL,
  `TenPhieuTD` text COLLATE utf8_unicode_ci NOT NULL,
  `NoiLam` text COLLATE utf8_unicode_ci NOT NULL,
  `SoLuong` text COLLATE utf8_unicode_ci NOT NULL,
  `HoSo` text COLLATE utf8_unicode_ci NOT NULL,
  `YeuCau` text COLLATE utf8_unicode_ci NOT NULL,
  `ThoiGian` text COLLATE utf8_unicode_ci NOT NULL,
  `Luong` text COLLATE utf8_unicode_ci NOT NULL,
  `MotaCV` text COLLATE utf8_unicode_ci NOT NULL,
  `QuyenLoi` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tinh_thanh`
--

CREATE TABLE `tinh_thanh` (
  `MaTinh` int(11) NOT NULL,
  `TenTinh` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tinh_thanh`
--

INSERT INTO `tinh_thanh` (`MaTinh`, `TenTinh`) VALUES
(1, 'Hồ Chí Minh');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `gia_su`
--
ALTER TABLE `gia_su`
  ADD PRIMARY KEY (`MaGiaSu`);

--
-- Indexes for table `hoc_phi`
--
ALTER TABLE `hoc_phi`
  ADD PRIMARY KEY (`MaHP`);

--
-- Indexes for table `khu_vuc`
--
ALTER TABLE `khu_vuc`
  ADD PRIMARY KEY (`MaKV`),
  ADD KEY `INDEX` (`MaTinh`);

--
-- Indexes for table `lop_hoc`
--
ALTER TABLE `lop_hoc`
  ADD PRIMARY KEY (`MaLop`);

--
-- Indexes for table `mon_hoc`
--
ALTER TABLE `mon_hoc`
  ADD PRIMARY KEY (`MaMon`);

--
-- Indexes for table `phieu_dang_ky`
--
ALTER TABLE `phieu_dang_ky`
  ADD PRIMARY KEY (`MaPhieuDK`);

--
-- Indexes for table `phieu_mo_lop`
--
ALTER TABLE `phieu_mo_lop`
  ADD PRIMARY KEY (`MaPhieuMo`),
  ADD KEY `MaPhieuDK` (`MaPhieuDK`);

--
-- Indexes for table `phieu_nhan_lop`
--
ALTER TABLE `phieu_nhan_lop`
  ADD PRIMARY KEY (`MaPhieuNhan`),
  ADD KEY `MaPhieuMo` (`MaPhieuMo`),
  ADD KEY `MaGiaSu` (`MaGiaSu`);

--
-- Indexes for table `phieu_tuyen_dung`
--
ALTER TABLE `phieu_tuyen_dung`
  ADD PRIMARY KEY (`MaPhieuTD`);

--
-- Indexes for table `tinh_thanh`
--
ALTER TABLE `tinh_thanh`
  ADD PRIMARY KEY (`MaTinh`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gia_su`
--
ALTER TABLE `gia_su`
  MODIFY `MaGiaSu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `hoc_phi`
--
ALTER TABLE `hoc_phi`
  MODIFY `MaHP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `khu_vuc`
--
ALTER TABLE `khu_vuc`
  MODIFY `MaKV` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `lop_hoc`
--
ALTER TABLE `lop_hoc`
  MODIFY `MaLop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `mon_hoc`
--
ALTER TABLE `mon_hoc`
  MODIFY `MaMon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `phieu_dang_ky`
--
ALTER TABLE `phieu_dang_ky`
  MODIFY `MaPhieuDK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `phieu_mo_lop`
--
ALTER TABLE `phieu_mo_lop`
  MODIFY `MaPhieuMo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `phieu_nhan_lop`
--
ALTER TABLE `phieu_nhan_lop`
  MODIFY `MaPhieuNhan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `phieu_tuyen_dung`
--
ALTER TABLE `phieu_tuyen_dung`
  MODIFY `MaPhieuTD` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tinh_thanh`
--
ALTER TABLE `tinh_thanh`
  MODIFY `MaTinh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `khu_vuc`
--
ALTER TABLE `khu_vuc`
  ADD CONSTRAINT `fk_khuvuc_tinh` FOREIGN KEY (`MaTinh`) REFERENCES `tinh_thanh` (`MaTinh`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `phieu_mo_lop`
--
ALTER TABLE `phieu_mo_lop`
  ADD CONSTRAINT `fk_phieumo_phieudk` FOREIGN KEY (`MaPhieuDK`) REFERENCES `phieu_dang_ky` (`MaPhieuDK`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `phieu_nhan_lop`
--
ALTER TABLE `phieu_nhan_lop`
  ADD CONSTRAINT `fk_phieunhan_giasu` FOREIGN KEY (`MaGiaSu`) REFERENCES `gia_su` (`MaGiaSu`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_phieunhan_phieumo` FOREIGN KEY (`MaPhieuMo`) REFERENCES `phieu_mo_lop` (`MaPhieuMo`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
