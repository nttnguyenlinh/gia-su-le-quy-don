<?php

use Illuminate\Support\Facades\Artisan;


//Xoá cache
Route::get('clear/{cmd}', function ($cmd) {
    $cmd = trim(str_replace("-", ":", $cmd));
    $validCommands = ['cache:clear', 'route:clear', 'view:clear', 'config:cache', 'config:clear'];
    if (in_array($cmd, $validCommands)) {
        Artisan::call($cmd);
        return "<h1 style='color: #2ae9aa;'>Artisan command: {$cmd} - successfully!</h1><h2 style='color: orange;'>More: cache-clear || route-clear || view-clear || config-cache || config-clear</h2>";
    } else
        return "<h1 style='color: #ff7acb;'>Artisan command: {$cmd} - Not valid!</h1><h2 style='color: orange;'>More: cache-clear || route-clear || view-clear || config-cache || config-clear</h2>";
});

Route::get('clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');

    return "<h3 style='color: #2ae9aa;'>
            ============<br>
            ||Successfully!||<br>
            ============<br>
			</h3>
		";
});

Route::get('info', function () {
    return view('phpinfo');
})->name('info');


//Danh sách route phần Trang chủ
Route::get('/', 'HomeController@index')->name('home');
Route::get('hoc-phi-tham-khao.html', 'HomeController@getHocPhi')->name('hocphithamkhao');
Route::get('tuyen-dung.html', 'HomeController@getTuyenDung')->name('tuyendung');
Route::get('lien-he.html', function () {
    return view('lienhe');
})->name('lienhe');
Route::get('tai-khoan-ngan-hang.html', function () {
    return view('nganhang');
})->name('nganhang');
Route::get('phu-huynh-can-biet.html', function () {
    return view('canbiet');
})->name('phuhuynhcanbiet');

Route::get('dang-ky-tim-gia-su.html', 'HomeController@getDangKyGiaSu')->name('getDangKyGiaSu');
Route::post('dang-ky-tim-gia-su.html', 'HomeController@postDangKyGiaSu')->name('postDangKyGiaSu');

Route::get('danh-sach-gia-su.html', 'HomeController@getGiaSuTieuBieu')->name('getGiaSuTieuBieu');
Route::post('danh-sach-gia-su.html', 'HomeController@searchGiaSu')->name('searchGiaSu');

Route::get('nhan-lop/gia-su-{slug}.html', 'HomeController@getChiTietLop')->name('getChiTietLop');

Route::get('danh-sach-lop.html', 'HomeController@getLopDangMo')->name('getLopDangMo');
Route::post('danh-sach-lop.html', 'HomeController@searchLopDangMo')->name('searchLopDangMo');

Route::get('dang-ky-lam-gia-su.html', 'HomeController@DkGiaSu')->name('DkGiaSu');
Route::post('dang-ky-lam-gia-su.html', 'HomeController@postDkGiaSu')->name('postDkGiaSu');


//Dành cho Gia sư & cần Session để truy vấn

Route::group(['middleware' => 'web'], function () {
    Route::get('giasu', 'GiaSuController@login')->name('giasu.login');
    Route::post('giasu', 'GiaSuController@postlogin')->name('giasu.postlogin');
    Route::get('thoat.html', 'GiaSuController@thoat')->name('giasu.thoat');

    Route::get('tai-khoan.html', 'GiaSuController@index')->name('giasu.index');
    Route::post('tai-khoan.html', 'GiaSuController@post_update')->name('giasu.post_update');

    Route::get('doi-mat-khau.html', 'GiaSuController@doimk')->name('giasu.doimk');
    Route::post('doi-mat-khau.html', 'GiaSuController@postdoimk')->name('giasu.postdoimk');

    Route::post('nhan-lop/gia-su-{slug}.html', 'HomeController@postChiTietLop')->name('postChiTietLop');

    Route::get('lop/gia-su-{slug}.html', 'GiaSuController@getLop')->name('getLop');
});

//Danh sách route phần trung tâm
Route::get('trungtam', 'DashboardController@login')->name('trungtam.login');
Route::post('trungtam', 'DashboardController@postLogin')->name('trungtam.postlogin');
Route::get('dang-xuat.html', 'DashboardController@logout')->name('trungtam.logout');
Route::get('trungtam/index.html', 'DashboardController@index')->name('trungtam');


/************************NGƯỜI DÙNG************************/

Route::get('trungtam/nguoi-dung.html', 'DashboardController@users')->name('trungtam.users');
Route::post('trungtam/nguoi-dung.html', 'DashboardController@users_add')->name('trungtam.users_add');
Route::get('trungtam/xoa-nguoi-dung', 'DashboardController@users_delete')->name('trungtam.users_delete');
Route::get('trungtam/khoi-phuc-mk', 'DashboardController@users_password')->name('trungtam.users_password');
Route::get('trungtam/trang-thai-nguoi-dung', 'DashboardController@users_status')->name('trungtam.users_status');

/************************NGƯỜI DÙNG************************/


/************************CÁ NHÂN************************/

//Route::get('trungtam/ca-nhan.html', 'DashboardController@profile')->name('trungtam.profile')->middleware('check_access_token');
//Route::post('trungtam/ca-nhan.html', 'DashboardController@editprofile')->name('trungtam.editprofile')->middleware('check_access_token');

Route::get('trungtam/ca-nhan.html', 'DashboardController@profile')->name('trungtam.profile');
Route::post('trungtam/ca-nhan.html', 'DashboardController@profile_edit')->name('trungtam.profile_edit');
Route::post('trungtam/doi-mk.html', 'DashboardController@profile_password')->name('trungtam.profile_password');

/************************CÁ NHÂN************************/


/************************GIA SƯ************************/

Route::get('trungtam/gia-su.html', 'DashboardController@giasu')->name('trungtam.giasu');
Route::get('trungtam/xoa-gia-su', 'DashboardController@giasu_delete')->name('trungtam.giasu_delete');
Route::get('trungtam/trang-thai-gia-su', 'DashboardController@giasu_status')->name('trungtam.giasu_status');
Route::get('trungtam/thay-doi-so-dt-mk.html', 'DashboardController@giasu_sdt_password')->name('trungtam.giasu_sdt_password');
Route::post('trungtam/thay-doi-so-dt-mk.html', 'DashboardController@giasu_sdt_password_change')->name('trungtam.giasu_sdt_password_change');

/************************GIA SƯ************************/


/************************PHIẾU ĐĂNG KÝ************************/

Route::get('trungtam/danh-sach-dang-ky.html', 'DashboardController@phieudangky')->name('trungtam.phieudangky');
Route::post('trungtam/danh-sach-dang-ky.html', 'DashboardController@phieudangky_add')->name('trungtam.phieudangky_add');
Route::get('trungtam/duyet-dang-ky', 'DashboardController@phieudangky_status')->name('trungtam.phieudangky_status');
Route::get('trungtam/xoa-dang-ky', 'DashboardController@phieudangky_delete')->name('trungtam.phieudangky_delete');
Route::get('trungtam/lay-thong-tin-dang-ky', 'DashboardController@phieudangky_id')->name('trungtam.phieudangky_id');
/************************PHIẾU ĐĂNG KÝ************************/


/************************MỞ LỚP***********************/

Route::get('trungtam/danh-sach-lop.html', 'DashboardController@phieumolop')->name('trungtam.phieumolop');
Route::post('trungtam/danh-sach-lop.html', 'DashboardController@phieumolop_edit')->name('trungtam.phieumolop_edit');
Route::get('trungtam/xoa-lop', 'DashboardController@phieumolop_delete')->name('trungtam.phieumolop_delete');
Route::get('trungtam/trang-thai-lop', 'DashboardController@phieumolop_status')->name('trungtam.phieumolop_status');

/************************MỞ LỚP***********************/


/************************TUYỂN DỤNG***********************/

Route::get('trungtam/tuyen-dung.html', 'DashboardController@tuyendung')->name('trungtam.tuyendung');
Route::post('trungtam/them-tuyen-dung', 'DashboardController@tuyendung_add')->name('trungtam.tuyendung_add');
Route::post('trungtam/sua-tuyen-dung', 'DashboardController@tuyendung_edit')->name('trungtam.tuyendung_edit');
Route::get('trungtam/xoa-tuyen-dung', 'DashboardController@tuyendung_delete')->name('trungtam.tuyendung_delete');
Route::get('trungtam/lay-thong-tin-tuyen-dung', 'DashboardController@tuyendung_id')->name('trungtam.tuyendung_id');

/************************TUYỂN DỤNG***********************/


/************************HỌC PHÍ THAM KHẢO***********************/

Route::get('trungtam/hoc-phi-tham-khao.html', 'DashboardController@hocphi')->name('trungtam.hocphi');
Route::post('trungtam/hoc-phi-tham-khao.html', 'DashboardController@hocphi_edit')->name('trungtam.hocphi_edit');

/************************HỌC PHÍ THAM KHẢO***********************/


/************************NHẬN DẠY***********************/

Route::get('trungtam/danh-sach-nhan-day.html', 'DashboardController@nhanlop')->name('trungtam.nhanlop');
Route::post('trungtam/danh-sach-nhan-day.html', 'DashboardController@nhanlop_status')->name('trungtam.nhanlop_status');

/************************NHẬN DẠY***********************/

//Get access token
//Route::get('get_imgur_auth', 'DashboardController@get_imgur_auth')->middleware('admin')->name('get_imgur_auth');

//Save access token
//Route::post('save_imgur_auth','DashboardController@save_imgur_auth')->middleware('admin')->name('save_imgur_auth');