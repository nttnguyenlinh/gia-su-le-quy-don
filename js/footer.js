$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

    $("#ngay").datepicker({
        dateFormat : "dd-mm-yy",
        minDate : 0
    });

    $('.jcarousel').jCarouselLite({
        vertical: true,
        hoverPause: true,
        visible: 2,
        auto: 1500,
        speed: 1000
    });

    $('#slider').Swipe({
        auto: 3000,
        continuous: true
    }).data('Swipe');

    $('#form_tim_gia_su').validate({
        rules: {
            hoten: {minlength: 4, maxlength: 30, required: true},
            dienthoai: {required: true, minlength: 10, maxlength: 10},
            diachi: {maxlength: 30, required: true},
            email:{email:true},
            lop: {required: true},
            monhoc: {required: true},
            level: {required: true},
            sobuoi: {required: true},
            thoigian: {maxlength: 30, required: true},
            yeucau: {maxlength: 100}
        },

        messages: {
            hoten: {required: 'Nhập họ tên!', minlength: 'Nhập ít nhất 4 kí tự!', maxlength: 'Nhập tối đa 30 kí tự!'},
            dienthoai: {required: 'Nhập số điện thoại!', minlength: 'Số điện thoại gồm 10 số!', maxlength: 'Số điện thoại gồm 10 số!'},
            diachi: {required: 'Nhập địa chỉ!', maxlength: 'Nhập tối đa 30 kí tự!'},
            email: {email: 'Email không hợp lệ!'},
            lop: {required: 'Chọn lớp học!'},
            monhoc: {required: 'Nhập môn học!'},
            level: {required: 'Chọn người dạy!'},
            sobuoi: {required: 'Chọn số buổi học /tuần!'},
            thoigian: {required: 'Nhập thời gian học!', maxlength: 'Nhập tối đa 30 kí tự!'},
            yeucau: {maxlength: 'Nhập tối đa 100 kí tự!'}

        }
    });

    $("#form_dk_gia_su").validate({
        rules: {
            hoten: {required: true},
            ngay: {required: true},
            thang: {required: true},
            nam: {required: true},
            nguyenquan: {required: true},
            giongnoi: {required: true},
            diachi: {required: true},
            cmnd: {required: true},
            email: {required: true, email: true},
            password: {required: true, pass_regex: true},
            password_confirm: {required: true, equalTo: "#password"},

            phone: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            truonghoc: {required: true},
            chuyennganh: {required: true},
            nam_totnghiep: {required: true},
            trinhdo: {required: true},
            gioitinh: {required: true},
            anhthe:{required: true}
        },

        messages: {
            hoten: {required: 'Nhập họ tên!'},
            ngay: {required: 'Chọn ngày!'},
            thang: {required: 'Chọn tháng!'},
            nam: {required: 'Chọn năm!'},
            nguyenquan: {required: 'Chọn nguyên quán!'},
            giongnoi: {required: 'Chọn giọng nói!'},

            diachi: {required: 'Nhập địa chỉ!'},
            cmnd: {required: 'Nhập cmnd!'},
            email: {required: 'Nhập email!', email: 'Email không hợp lệ!'},

            password: {required: 'Nhập mật khẩu!', pass_regex: 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, tối thiểu 6 kí tự!'},
            password_confirm: {required: 'Nhập lại mật khẩu!', equalTo: 'Mật khẩu không khớp!'},

            phone: {
                required: 'Nhập số điện thoại!',
                minlength: 'Có ít nhất 10 số!',
                maxlength: 'Tối đa 10 số!',
            },
            truonghoc: {required: 'Nhập trường đào tạo!'},
            chuyennganh: {required: 'Nhập chuyên ngành!'},
            nam_totnghiep: {required: 'Chọn năm tốt nghiệp!'},
            trinhdo: {required: 'Chọn trình độ!'},
            gioitinh: {required: 'Chọn giới tính!'},
            anhthe: {required: 'Chọn hình ảnh rõ về bạn!'},
        }
    });

    $("#form_dk_gia_su").ready(function () {
        var availableTruongs = [];
        var availableNganhs = [];
        availableTruongs.push('ĐH Sư Phạm HCM');
        availableTruongs.push('ĐH Ngoại Thương HCM');
        availableTruongs.push('ĐH Ngân Hàng HCM');
        availableTruongs.push('ĐH Bách Khoa HCM');
        availableTruongs.push('ĐH Kinh Tế HCM');
        availableTruongs.push('ĐH Khoa Học Tự Nhiên HCM');
        availableTruongs.push('ĐH Y Dược HCM');
        availableTruongs.push('ĐH Y Khoa Phạm Ngọc Thạch HCM');
        availableTruongs.push('ĐH Công Nghiệp HCM');
        availableTruongs.push('ĐH Luật HCM');
        availableTruongs.push('ĐH Kinh Tế Luật HCM');
        availableTruongs.push('ĐH Mở HCM');
        availableTruongs.push('ĐH Quốc Gia HCM');
        availableTruongs.push('ĐH Công Nghệ Thông Tin HCM');
        availableTruongs.push('ĐH FPT');
        availableTruongs.push('ĐH Giao Thông Vận Tải HCM');
        availableTruongs.push('ĐH Giao Thông Vận Tải Cơ Sở 2');
        availableTruongs.push('HV Bưu Chính Viễn Thông HCM');
        availableTruongs.push('ĐH Nông Lâm HCM');
        availableTruongs.push('ĐH Sư Phạm Kỹ Thuật HCM');
        availableTruongs.push('ĐH Sài Gòn HCM');
        availableTruongs.push('ĐH RMIT');
        availableTruongs.push('ĐH Ngoại Ngữ Tin Học HCM');
        availableTruongs.push('ĐH Quốc Tế HCM');
        availableTruongs.push('ĐH Kiến Trúc HCM');
        availableTruongs.push('ĐH Khoa Học Xã Hội Và Nhân Văn HCM');
        availableTruongs.push('HV Hàng Không');
        availableTruongs.push('ĐH Tài Chính Marketing HCM');
        availableTruongs.push('ĐH Tôn Đức Thắng HCM');
        availableTruongs.push('ĐH Mỹ Thuật HCM');
        availableTruongs.push('Nhạc Viện HCM');
        availableTruongs.push('ĐH Tài Nguyên Và Môi Trường HCM');
        availableTruongs.push('ĐH Văn Hóa');
        availableTruongs.push('CĐ Công Thương HCM');
        availableTruongs.push('CĐ Kinh Tế Đối Ngoại HCM');
        availableTruongs.push('CĐ Tài Chính Hải Quan');
        availableTruongs.push('CĐ Sư Phạm Trung Ương HCM');
        availableTruongs.push('CĐ Điện Lực HCM');
        availableTruongs.push('CĐ Giao Thông Vận Tải 3 HCM');
        availableTruongs.push('CĐ Giao thông Vận tải HCM');
        availableTruongs.push('CĐ Kinh tế HCM');
        availableTruongs.push('CĐ Kỹ thuật Cao Thắng HCM');
        availableTruongs.push('CĐ Kỹ thuật Lý Tự Trọng HCM');
        availableTruongs.push('CĐ Phát thanh Truyền hình 2');
        availableTruongs.push('CĐ Văn hóa Nghệ thuật HCM');
        availableTruongs.push('CĐ Xây dựng số 2 HCM');
        availableTruongs.push('ĐH Công nghiệp Thực phẩm HCM');
        availableTruongs.push('ĐH Sân khấu Điện ảnh HCM');
        availableTruongs.push('ĐH Thể dục Thể thao HCM');
        availableTruongs.push('ĐH Quốc tế Hồng Bàng HCM');
        availableTruongs.push('ĐH Nguyễn Tất Thành HCM');
        availableTruongs.push('ĐH Văn Hiến HCM');
        availableTruongs.push('ĐH Văn Lang HCM');
        availableTruongs.push('ĐH Kinh tế - Tài chính HCM');
        availableTruongs.push('ĐH Hoa Sen HCM');
        availableTruongs.push('ĐH Thủy Lợi - Cơ sở 2 HCM');
        availableTruongs.push('ĐH Quy Nhơn');
        availableNganhs.push('Sư phạm Toán học');
        availableNganhs.push('Sư phạm Tin học');
        availableNganhs.push('Sư phạm Vật lý');
        availableNganhs.push('Sư phạm Hóa học');
        availableNganhs.push('Sư phạm Sinh học');
        availableNganhs.push('Sư phạm Ngữ văn');
        availableNganhs.push('Sư phạm Lịch sử');
        availableNganhs.push('Sư phạm Địa lý');
        availableNganhs.push('Sư phạm Tiếng Anh');
        availableNganhs.push('Giáo dục Mầm non');
        availableNganhs.push('Giáo dục Tiểu học');
        availableNganhs.push('Giáo dục Đặc biệt');
        availableNganhs.push('Sư phạm tiếng Nga');
        availableNganhs.push('Sư phạm Tiếng Pháp');
        availableNganhs.push('Sư phạm Tiếng Trung Quốc');
        availableNganhs.push('Kế Toán');
        availableNganhs.push('Quản Trị Kinh Doanh');
        availableNganhs.push('Toán - Tin');
        availableNganhs.push('Công Nghệ Thông Tin');
        availableNganhs.push('Ngôn ngữ Anh');
        availableNganhs.push('Ngôn ngữ Nga');
        availableNganhs.push('Ngôn ngữ Pháp');
        availableNganhs.push('Ngôn ngữ Trung Quốc');
        availableNganhs.push('Ngôn ngữ Nhật');
        availableNganhs.push('Ngôn ngữ Hàn Quốc');
        availableNganhs.push('ngoại ngữ');
        availableNganhs.push('Kế toán - Kiểm toán');
        availableNganhs.push('Kiểm toán');
        availableNganhs.push('Nhật bản học');

        $("#truonghoc").autocomplete({
            source: availableTruongs
        });

        $("#chuyennganh").autocomplete({
            source: availableNganhs
        });

        document.getElementById("anhthe").onchange = function () {
            var reader = new FileReader();
            if(this.files[0].size>2097152){
                alert("Vui lòng chọn hình nhỏ hơn 2MB!");
                $("#anhthe").attr("src","blank");
                $('#anhthe').wrap('<form>').closest('form').get(0).reset();
                $('#anhthe').unwrap();
                return false;
            }
            if(this.files[0].type.indexOf("image")==-1){
                alert("Vui lòng chọn tệp hình ảnh!");
                $("#anhthe").attr("src","blank");
                //$("#anhthe").hide();
                $('#anhthe').wrap('<form>').closest('form').get(0).reset();
                $('#anhthe').unwrap();
                return false;
            }
            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                document.getElementById("anhthe").src = e.target.result;
                $("#anhthe").show();
            };

            // read the image file as a data URL.
            reader.readAsDataURL(this.files[0]);
        };

        //Kiểm tra mật khẩu
        $.validator.addMethod("pass_regex", function (value) {
            return /(?!^[0-9]*$)(?!^[a-zA-Z]*$)(?!^[0-9]{1})^([a-zA-Z0-9]{6,})$/.test(value);
        });
    });

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });

    $(window).scroll(function(){
        if($(this).scrollTop() > 100){
            $('#scroll').fadeIn();
        }else{
            $('#scroll').fadeOut();
        }
    });

    $('#scroll').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    $("#form_update_profile").validate({
        rules: {
            hoten: {required: true},
            ngay: {required: true},
            thang: {required: true},
            nam: {required: true},
            nguyenquan: {required: true},
            giongnoi: {required: true},
            diachi: {required: true},
            cmnd: {required: true},
            email: {required: true, email: true},
            password: {required: true, pass_regex: true},
            truonghoc: {required: true},
            chuyennganh: {required: true},
            nam_totnghiep: {required: true},
            trinhdo: {required: true},
            gioitinh: {required: true},
        },

        messages: {
            hoten: {required: 'Nhập họ tên!'},
            ngay: {required: 'Chọn ngày!'},
            thang: {required: 'Chọn tháng!'},
            nam: {required: 'Chọn năm!'},
            nguyenquan: {required: 'Chọn nguyên quán!'},
            giongnoi: {required: 'Chọn giọng nói!'},
            diachi: {required: 'Nhập địa chỉ!'},
            cmnd: {required: 'Nhập cmnd!'},
            email: {required: 'Nhập email!', email: 'Email không hợp lệ!'},
            password: {required: 'Nhập mật khẩu!', pass_regex: 'Mật khẩu phải bắt đầu bằng chữ cái, có ít nhất 1 số, tối thiểu 6 kí tự!'},
            truonghoc: {required: 'Nhập trường đào tạo!'},
            chuyennganh: {required: 'Nhập chuyên ngành!'},
            nam_totnghiep: {required: 'Chọn năm tốt nghiệp!'},
            trinhdo: {required: 'Chọn trình độ!'},
            gioitinh: {required: 'Chọn giới tính!'},
        }
    });
});

<!-- Google Tag Manager & Facebook-->
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js?v=4.0.1'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5XZ75SP');

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js?v=4.0.1#xfbml=1&version=v2.12&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
<!-- End Google Tag Manager -->